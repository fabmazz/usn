from ndlibA.models.compartments.Compartment import Compartiment
import numpy as np

__author__ = 'Giulio Rossetti'
__license__ = "BSD-2-Clause"
__email__ = "giulio.rossetti@gmail.com"



## NEW ARRAY BASED COMPARTMENT
class NodeStochastic(Compartiment):

    def __init__(self, rate, triggering_status=None, **kwargs):
        super(self.__class__, self).__init__(kwargs)
        self.rate = rate
        self.trigger = triggering_status

    def execute(self, adjacency, edges, attributes, status_map, to_use, *args, **kwargs):
        '''
        adjacency is a non-symmetric adjacency matrix: if the edge i,j exists, in i,j there is the status of j, in j,i the status of i. otherwise, in i,j there is 0
        nb: statuses now start from 1
        nb: we assume that the graph is undirected
        '''
        
        if self.trigger is None:
            triggered = to_use
        else:
            triggered = (adjacency==status_map[self.trigger]).sum(axis=1)
            triggered = np.asarray(triggered, dtype=bool).reshape(to_use.shape)*to_use
        
        p = np.random.random(triggered.sum()) 
        test = np.zeros(triggered.shape, dtype=bool)
        try:
            test[triggered] = (p < self.rate)
        except:
            test[triggered] = (p < self.rate[triggered])
        if np.any(test):
            return np.logical_and(test, self.compose(adjacency, edges, attributes, status_map, to_use, kwargs))
        else:
            return test
