#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#   
#    This file is part of USN.
#   
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#   
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#   
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#   
#   

import sys
import argparse
import numpy as np
import pandas as pd
import random
import os
from lib.libfunc import read_from_config

RANDOM=None
EXPECTED_NUMBER_OF_ROWS=102
NUM_COLS = 4
from config.config import AGE_GROUPS, get_population_file


def main():
    parser=argparse.ArgumentParser(description="modify population ages in population_0.csv")
    parser.add_argument('-p', '--input_file', dest='input_file', type=str, required=True, help='ISTAT file with pop ages')
    parser.add_argument('--city', dest='city', type=str, required=True, help='specify the city')
    
    args=parser.parse_args()

    ISTAT_pop_ages_file = args.input_file
    city = args.city
    city = city.lower()
    popfile = get_population_file(city)
    print(f'reading population file {popfile}')
    if not os.path.isfile(popfile):
        print(f'ERROR: file {popfile} not found! Quit.')
        sys.exit(111)
    istatdf     = pd.read_csv(ISTAT_pop_ages_file) #
    output_file = popfile
    age_groups  = AGE_GROUPS
    print(f'age groups: {age_groups}')
    rng = np.random.default_rng(RANDOM)
    
    if len(istatdf.columns) == NUM_COLS:
        istatdf.columns = ['Età', 'Maschi', 'Femmine', 'Totale']
    else:
        print(f, "Error, the number of columns should be:  {NUM_COLS}")

    if istatdf.shape[0] != EXPECTED_NUMBER_OF_ROWS:
        print(f"ERROR: expecting {EXPECTED_NUMBER_OF_ROWS} rows! Abort.", file=sys.stderr)
        print(istatdf.shape)
        sys.exit(111)

    p = []
    for i in range(len(age_groups)):
        if i==0:
            p.append(istatdf['Totale'].iloc[0:age_groups[i]+1].sum())
        else:
            p.append(istatdf['Totale'].iloc[age_groups[i-1]+1:age_groups[i]+1].sum())
    print(f'people in age group: {p}') 
    tot=sum(p)

    p = np.array(p)/tot
    popdf = pd.read_csv(popfile)
    how_many_types_arr=rng.multinomial(len(popdf), p)
    col_arr=[n for n in range(len(p)) for i in range(how_many_types_arr[n])]
    rng.shuffle(col_arr)
    popdf['type']=col_arr
    popdf=popdf.round(1)
    popdf.to_csv(output_file,index=False)
    print(f'Population file {popfile} overwritten with ISTAT data.')

    return

if __name__ == "__main__":
    main()



