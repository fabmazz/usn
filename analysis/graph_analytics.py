#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#   
#    This file is part of USN.
#   
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#   
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#   
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#   
#   

#!/usr/bin/env python
# coding: utf-8

import sys
import os
import argparse
import configparser as cp
import matplotlib.pylab as plt
plt.rcParams["figure.figsize"] = (10,10)
plt.style.use('ggplot')
import pickle
import pandas as pd
import igraph
import numpy as np
from scipy.sparse import csr_matrix, coo_matrix
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import colors
import seaborn as sns
sns.set(rc={'figure.figsize':(12, 10)})
from config.config import get_territory_file
from lib.libfunc import read_from_config



def get_metrics(graphs):
        vcount    = graphs.vcount()
        ecount    = graphs.ecount()
        density   = round( (2*ecount)/(vcount*(vcount-1)), 4)
        avgdeg    = round(2*ecount/vcount, 2)
        globalC   = round(graphs.transitivity_undirected(mode="zero"), 4)
        avglocC   = round(graphs.transitivity_avglocal_undirected(mode="zero"), 4)
        degAssort = round(graphs.assortativity_degree(directed=False), 4)
        cc        = graphs.components(mode=igraph.WEAK)
        numCC     = len(cc)
        sizeGCC   = len(cc.giant().vs)
        columns = ['nodes', 'edges', 'density', 'avgdeg', 'globalC', 'avglocC', 'assortativity', 'numC', 'sizeGCC']
        metrics = [vcount, ecount, density, avgdeg, globalC, avglocC, degAssort, numCC, sizeGCC]
        df = pd.DataFrame()
        df['metrics']=columns
        df['values']=metrics
        return df



INPUT_DIR="SINGLE_RUN_OUTPUT/sabaudia/euclideanPower_2_real_lognormal_reader_uniform_10_real__0/"
GRAPH_FILE="sb_graph.pickle"
DISTANCE_INPUT_FILE="DISTANCE_AllGroups.pickle"

GLOBAL_METRICS_TABLE_CSV = "globalMetrics.csv"
GLOBAL_METRICS_TABLE_LATEX = "globalMetrics.tex"
IGRAPH_DEGREE_DISTRIB_FILE = 'igraph_degree_distrib.txt'
HEATMAP_FILE='heatmap.pdf'
DEG_VS_PEOPLE_FILE='deg_vs_people.pdf'
MEAN_TILE_DEG_VS_PEOPLE_FILE='mean_tile_deg_vs_people.pdf'
TILE_DEG_DIST_FILE='tile_deg_dist.pdf'
PEOPLE_PER_TILE_FILE='people_per_tile.pdf'
GRAPH_DEG_DISTRIB_FILE='deg_distrib.pdf'
EDGE_LENGHT_DISTRIB_FILE='edge_length_distrib.pdf'

def main():

    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-c', '--config', dest='config', type=str, required=True, help='configuration file in .ini format (needed only for territory info)')
    parser.add_argument('-i', '--input_dir', dest='input_dir', type=str, required=True, help='directory with the graph and distances pickle files')
    parser.add_argument('-r', '--remove_households', dest='remove_households', type=str,  help='remove househlods edges from input graph')
    args=parser.parse_args()

    config_file = args.config
    input_dir = args.input_dir

    if args.remove_households:
        remove_households = True
    else:
        remove_households = False
    
    city=read_from_config(config_file, 'CITY', 'city')
    territory_file_pj = get_territory_file(city)
    with open(territory_file_pj, 'rb') as fin:
        territory = pickle.load(fin)

    ntiles=territory['ntiles']

    dimx, dimy = ntiles[0], ntiles[1]
    dim=dimx*dimy

    fin = os.path.join(input_dir, GRAPH_FILE)
    print("reading graph: {}".format(fin))

    g1=igraph.Graph.Read_Pickle(fin)
    print(g1.summary())
    print("Graph edges layers: {}".format(np.unique(g1.es['layer'])))


    hh_edges = [e for e in g1.es if "household" in e['layer']]
    fs_edges = [e for e in g1.es if "friendship" in e['layer']]
    print("Num hh edges: {}, num fs edges; {}".format(len(hh_edges), len(fs_edges)))
    if remove_households:
        g1=g1.subgraph_edges(fs_edges, delete_vertices=True)

    
    # Compute global metrics and dump tex and csv
    dfmetrics=get_metrics(g1)
    dfmetrics = dfmetrics.round(5)
    
    fout_tex = os.path.join(input_dir, GLOBAL_METRICS_TABLE_LATEX)
    fout_csv = os.path.join(input_dir, GLOBAL_METRICS_TABLE_CSV)

    with open(fout_tex, 'w') as tf:
        tf.write(dfmetrics.to_latex())

    dfmetrics.to_csv(fout_csv)


    # Compute degree distribution with igraph and dump to text
    a=np.count_nonzero([i for i in g1.degree() if i > 100])
    print("Number of nodes with deg > 100: {}".format(a))
    dd=g1.degree_distribution()
    
    fout_igraph_dd = os.path.join(input_dir, IGRAPH_DEGREE_DISTRIB_FILE)
    with open(fout_igraph_dd, 'w') as tf:
        print(dd, file=tf)


    # DEGREE HEATMAT
    plt.figure()
    df=pd.DataFrame()
    df['degree']=g1.degree()
    df['tile']=g1.vs['tile_id']
    df=df.sort_values(by=['tile'])

    dfdeg=df.groupby('tile').sum()
    dfcount=df.groupby('tile').count()
    dfdeg=dfdeg.reindex(np.arange(0, dim)).fillna(0)
    dfcount=dfcount.reindex(np.arange(0, dim)).fillna(0)

    degree=dfdeg.to_numpy()
    count=dfcount.to_numpy()
    degree2=degree/count
    degree2=np.nan_to_num(degree2)
    d=degree2.reshape(dimy,dimx)

    sns.heatmap(d, xticklabels=True, yticklabels=True, annot = True)
    plt.xlim(0,dimx)
    plt.ylim(0,dimy)
    FOUT=os.path.join(input_dir, HEATMAP_FILE)
    plt.title('Heatmap of average degree per tile in {}'.format(city.capitalize()))
    plt.savefig(FOUT)
    plt.close()


    # # NODE DEGREE VS PEOPLE IN TILE
    plt.figure()
    df=pd.DataFrame()
    df['degree']=g1.degree()
    df['tile']=g1.vs['tile_id']
    df=df.sort_values(by=['tile'])
    pop=df.groupby('tile')['degree'].count()
    df['pop']=[pop[p] for p in df['tile']]

    plt.title('Node degree vs people in tile for {}'.format(city.capitalize()))
    sns.scatterplot(x=df['pop'],y=df['degree'])
    FOUT=os.path.join(input_dir, DEG_VS_PEOPLE_FILE)
    plt.savefig(FOUT)
    plt.close()


    # # Mean Degree per Tile vs People in Tile
    plt.figure()
    sns.scatterplot(x=count.flatten(), y=degree2.flatten())
    plt.xlabel('people per tile for')
    plt.ylabel('mean degree per tile')
    plt.title('Mean degree vs people (per tile) in {}'.format(city.capitalize()))
    FOUT=os.path.join(input_dir, MEAN_TILE_DEG_VS_PEOPLE_FILE)
    plt.savefig(FOUT)
    plt.close()

    # # TILE DEGREE DISTRIBUTION 
    d=degree2.flatten()
    d=d[d != 0]
    # the histogram of the data
    plt.figure()
    sns.distplot(d, bins=50, kde=False)
    plt.title('Distribution of average degree per tile in {}'.format(city.capitalize()))
    plt.xlabel('average degree')
    plt.ylabel('number of tiles')
    FOUT=os.path.join(input_dir, TILE_DEG_DIST_FILE)
    plt.savefig(FOUT)
    plt.close()


    # # PEOPLE IN TILE
    dfcount=dfcount.rename(columns={'degree':'people'})
    dfcount= dfcount[dfcount['people']>0]
    plt.figure()
    sns.distplot(dfcount,kde=False)
    plt.title('People in each tile (empty tiles removed) for {}'.format(city.capitalize()))
    plt.xlabel('number of persons')
    plt.ylabel('number of tiles')
    FOUT=os.path.join(input_dir, PEOPLE_PER_TILE_FILE)
    plt.savefig(FOUT)
    plt.close()


    # # GRAPH DEGREE DISTRIBUTION
    realdeg=g1.degree()
    plt.figure()
    sns.distplot(realdeg, hist=True, norm_hist=True, kde=False)
    plt.title('Graph degree distribution for {}'.format(city.capitalize()))
    plt.xlabel('degree')
    plt.ylabel('density')
    FOUT=os.path.join(input_dir, GRAPH_DEG_DISTRIB_FILE)
    plt.savefig(FOUT)
    plt.close()

    # # Edge Lenght Distribution
    fin = os.path.join(input_dir, DISTANCE_INPUT_FILE)
    df=pd.read_pickle(fin)
    l=np.array(df)
    plt.figure()
    ax=sns.distplot(l, kde=False)
    plt.title('Edge length distribution for {}'.format(city.capitalize()))
    plt.xlabel('edge length (m)')
    plt.ylabel('number of edges')
    FOUT=os.path.join(input_dir, EDGE_LENGHT_DISTRIB_FILE)
    plt.savefig(FOUT)
    plt.close()

    return

if __name__ == "__main__":
    main()



