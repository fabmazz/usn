#!/bin/bash
if [[ $# -lt 2 ]]; then
	echo "Usage: $0 dir precision"
	exit 1
fi
d=$1
p=$2
sd=$(dirname ${d})
for a in $(tail -n +2 ${d}/free_terms.csv | cut -d ',' -f 1,2,4,5 | sed -e 's/, /_/g'); do 
	i=(${a//_/ }); 
	n=$(wc -l ${d}/blocks_${i[0]}_${i[1]}.csv | cut -d ' ' -f 1); 
	let n=n-1;
	tail -n +2 ${d}/blocks_${i[0]}_${i[1]}.csv | sed -e 's/, / /' | ${sd}/solver ${n} ${i[2]} ${i[3]} ${p} | tail -1 | awk -v a=${i[0]} -v b=${i[1]} '{print a, b, $2}';
done
