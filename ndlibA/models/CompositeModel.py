from ndlibA.models.DiffusionModel import DiffusionModel
# import future.utils
import numpy as np
from scipy.sparse import csr_matrix


__author__ = 'Giulio Rossetti'
__license__ = "BSD-2-Clause"
__email__ = "giulio.rossetti@gmail.com"


## NOVEL ARRAY BASED COMPARTMENT

def get_igraph_adjacency(g):
    if g.is_directed():
        data = np.ones(g.ecount())
    else:
        data = np.ones(2*g.ecount())
    indptr = [0]
    indices = []
    for adjl in g.get_adjlist():
        indices.extend(adjl)
        indptr.append(len(indices))
    A = csr_matrix((data,indices,indptr), shape=(g.vcount(),g.vcount()))
    return A


class CompositeModel(DiffusionModel):

    def __init__(self, graph=None, seed=None, store_infections=False):
        """
             Model Constructor
             :param graph: An igraph graph object
         """
        #super(self.__class__, self).__init__(graph)
        super(self.__class__, self).__init__(graph, seed)
        self.available_statuses = {}
        self.compartment = {}
        self.compartment_progressive = 0
        self.status_progressive = 1
        self.attributes = {}
        if store_infections:
            self.infections = {}
        else:
            self.infections = None
        if graph is not None:
            self.add_graph(graph)
        

    def add_graph(self, graph, reset_status=True):
        self.graph = graph
        if reset_status:
            self.status = {n: 0 for n in range(self.graph.vcount())} # self.graph.nodes}
        self.adjacency = get_igraph_adjacency(graph) 
        self.edges = {}
        edge_attrs = graph.edge_attributes() # set().union(*[e[2].keys() for e in graph.edges(data=True)])
        temp_d = {}
        for attr in edge_attrs:
            self.edges[attr] = {}
            temp_d[attr] = {}
            for idx,v in zip(graph.get_edgelist(), graph.es[attr]): # nx.get_edge_attributes(graph, attr).items():
                temp_d[attr].setdefault(v,[]).append(idx)
            for v,idxs in temp_d[attr].items():
                rows, cols = list(zip(*idxs))
                self.edges[attr][v] = csr_matrix((np.ones(len(idxs)),(rows,cols)), shape=self.adjacency.shape)
                self.edges[attr][v] = self.edges[attr][v]+self.edges[attr][v].T

    def add_status(self, status_name):
        if status_name not in self.available_statuses:
            self.available_statuses[status_name] = self.status_progressive
            self.status_progressive += 1

    def add_rule(self, status_from, status_to, rule):
        self.compartment[self.compartment_progressive] = (status_from, status_to, rule)
        self.compartment_progressive += 1

    def iteration(self, node_status=True, only_rules=None):
        """
        Execute a single model iteration

        :return: Iteration_id, Incremental node status (dictionary node->status)
        """
        self.clean_initial_status(list(self.available_statuses.values()))
        actual_status = {node:nstatus for node,nstatus in self.status.items()}

        if self.actual_iteration == 0:
            self.actual_iteration += 1
            delta, node_count, status_delta = self.status_delta(actual_status)
            self.attributes['status'] = np.array([v for k,v in sorted(self.status.items())])
            # self.adjacency = self.adjacency.multiply(self.attributes['status']) 
            if node_status:
                return {"iteration": 0, "status": actual_status.copy(),
                        "node_count": node_count.copy(), "status_delta": status_delta.copy()}
            else:
                return {"iteration": 0, "status": {},
                        "node_count": node_count.copy(), "status_delta": status_delta.copy()}

        changed = np.zeros(self.graph.vcount(), dtype=bool)
        status_adjacency = self.adjacency.multiply(self.attributes['status'])
        
        if only_rules is not None:
            selected_rules = only_rules
        else:
            selected_rules = range(0, self.compartment_progressive)
        
        for i in selected_rules:
            rule = self.compartment[i][2]
            to_use = np.logical_and(self.attributes['status']==self.available_statuses[self.compartment[i][0]], ~changed)
            test = rule.execute(status_adjacency, self.edges, self.attributes, self.available_statuses, to_use, self.infections, params=self.params)
            to_change = np.logical_and(to_use, test)
            self.attributes['status'] = np.where(to_change, self.available_statuses[self.compartment[i][1]], self.attributes['status'])
            changed = np.logical_or(changed, to_change)
            if np.all(changed):
                break
        actual_status = {i:v for i,v in enumerate(list(self.attributes['status']))}

        delta, node_count, status_delta = self.status_delta(actual_status)
        self.status = actual_status
        self.actual_iteration += 1

        if node_status:
            return {"iteration": self.actual_iteration - 1, "status": delta.copy(),
                    "node_count": node_count.copy(), "status_delta": status_delta.copy()}
        else:
            return {"iteration": self.actual_iteration - 1, "status": {},
                    "node_count": node_count.copy(), "status_delta": status_delta.copy()}
