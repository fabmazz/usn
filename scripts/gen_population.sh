#!/bin/bash

# Default VARS
#geojson="city.geojson"
#territory_not_proj="territory_not_projected.pickle"
#population_dir="../data/population/"
#population_dataframe='population_dataframe.pickle'
#tiff_file='tiff_info.pickle'

# expects "$?" as input param
check_exit_status()
{
    local retval="$1"
    if [ $retval -ne 0 ]; then
        echo "last command returned the error code: $retval"
        exit $retval
    fi
}


if [ $# -ne 2 ]
then
    printf "Usage $0: <city_name> <worldpop_dir>\n"
	printf "\t <city_name> the name of the city\n"
	printf "\t <worldpop_dir> the directory containing the tiff file downloaded\n"
	printf "\t                from worldpop, using the script 'download_wordpop_tiff.sh'\n"
    exit 1
fi

city="$(tr '[:lower:]' '[:upper:]' <<< ${1:0:1})${1:1}"
#city="${1}"
wdir="${2}"

# DEFAULT CONFIG
CONFIG=config.py
echo "reading from $CONFIG"

# READ FILE NAME AND OTHER VARS FROM CONFIG
geojson=$(grep "^CITY_GEOJSON" ${CONFIG} | cut -f2 -d'=' | xargs)
check_exit_status "$?"
echo "geojson = ${geojson}"
territory_not_proj=$(grep "^TERRITORY_PICKLE_FILE_NOT_PROJECTED" ${CONFIG} | cut -f2 -d'=' | xargs)
check_exit_status "$?"
echo "territory_not_projected = ${territory_not_proj}"
pop_dir=$(grep "^POPULATION_PATH" ${CONFIG} | cut -f2 -d'=' | xargs)
check_exit_status "$?"
population_dir="../${pop_dir}"
echo "output population dir = ${population_dir}"
population_dataframe=$(grep "^POPULATION_DATAFRAME_FILE" ${CONFIG} | cut -f2 -d'=' | xargs)
check_exit_status "$?"
echo "population_dataframe = ${population_dataframe}"
tiff_file=$(grep "^TIFF_INFO_FILE" ${CONFIG} | cut -f2 -d'=' | xargs)
check_exit_status "$?"
echo "tiff info file = ${tiff_file}"
tx=$(grep "^TX" ${CONFIG} | cut -f2 -d'=' | xargs)
ty=$(grep "^TY" ${CONFIG} | cut -f2 -d'=' | xargs)

if [[ ${tx} -lt 150 ]]; then
    echo "Tile size too small: TX=${tx}. Must be > 150"
    exit 1
    fi
    
if [[ ${ty} -lt 150 ]]; then
    echo "Tile size too small: TY=${ty}. Must be > 150"
    exit 1
    fi

echo "Tile size: (${tx}, ${ty})"

# START THE WORK
echo "Create population for: $city"

printf "\npython3 get_city_geojson.py '${city}'\n"
python3 get_city_geojson.py "${city}"
check_exit_status "$?"

printf "\npython3 plot_geojson.py ${geojson}\n"
python3 plot_geojson.py ${geojson}
check_exit_status "$?"

printf "\npython3 compute_territory.py ${geojson} ${tx} ${ty}\n"
python3 compute_territory.py ${geojson} ${tx} ${ty}
check_exit_status "$?"

printf "\npython3 extract_population_from_worldpop_tif.py -f ${wdir} -t ${territory_not_proj} -g ${geojson} -d 1\n"
python3 extract_population_from_worldpop_tif.py -f ${wdir} -t ${territory_not_proj} -g ${geojson} -d 1
check_exit_status "$?"

python3 create_population_csv.py -p ${population_dataframe} -g ${geojson} -d ${tiff_file} --debug 2 --city "${city}"
check_exit_status "$?"

# city --> dir_name
# first, strip underscores
dir_name=${city//_/}
# next, replace spaces with underscores
dir_name=${dir_name// /_}
# now, clean out anything that's not alphanumeric or an underscore
dir_name=${dir_name//[^a-zA-Z0-9_]/}
# finally, lowercase with TR
dir_name=`echo -n ${dir_name} | tr A-Z a-z`


printf "\nMoving all output files to: ./${dir_name}\n"
mkdir -p ${dir_name}

output_files=$(ls -1 *.{pdf,csv,pickle,geojson,parquet,json})
for file in ${output_files}
    do
        mv "${file}" ${dir_name}
    done

## EXTRA SCRIPTS
printf "\npython3 plot_pop_heatmap.py -n ${dir_name} \n"
python3 plot_pop_heatmap.py -n ${dir_name}

printf "Moving the newly created directory ${dir_name} to ${population_dir}\n"
path="${population_dir}/${dir_name}"
if [ -d "${path}" ]; then
    echo "WARNING: directory ${path} already exists! You must move ./${dir_name} to ${population_dir} by hands !!!\n"
else
    mv ${dir_name} ${population_dir}
fi





