#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import pickle
import os
import igraph
import argparse
import errno
import configparser as cp
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import sys
import matplotlib.colors as mcolors
from matplotlib.lines import Line2D
from matplotlib.text import Text
import matplotlib.text as mtext
import matplotlib

FONT_SIZE = 12
FONT_FAMILY = matplotlib.rcParams['font.family']
FONT_STYLE = matplotlib.rcParams['font.style']
FONT_WEIGHT = matplotlib.rcParams['font.weight']

class LegendTitle(object):
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        #title = mtext.Text(x0, y0, str(orig_handle).title(), usetex=True, fontsize=FONT_SIZE+2, fontfamily=FONT_FAMILY, fontweight=FONT_WEIGHT)
        title = mtext.Text(x0, y0, str(orig_handle).title(), usetex=False, fontsize=FONT_SIZE+1)
        handlebox.add_artist(title)
        return title

###################################
# HEATMAP
###################################
def get_heatmap(iter_id, data, output_dir):
    epidemic_status = data['epidemic_status']
    estatus_list = data['estatus_list']
    nNodes = data['pop_size']
    dimx, dimy = data['tiles']
    dim = dimx * dimy
    
    df = pd.DataFrame()
    df['tile'] = data['tile_id']
    for s in range(len(estatus_list)): # for each Epidemic Status
        s_list = []
        s += 1
        for i in range(nNodes): # for each Node
            if epidemic_status[iter_id][i] == s:
                s_list.append(1)
            else:
                s_list.append(0)
        df[s] = s_list
    
    df=df.sort_values(by=['tile'])
    dfsum=df.groupby('tile').sum()      # How many people in status X in the tile
    dfcount=df.groupby('tile')[1].count()  # How many people in the tile
    
    dfsum=dfsum.reindex(np.arange(0, dim)).fillna(0)
    dfcount=dfcount.reindex(np.arange(0, dim)).fillna(0)
    
    count = dfcount.to_numpy()
    statusList = {}
    for s in range(len(estatus_list)):
        statusList[s+1] = dfsum[s+1].to_numpy()
    normData = {}
    for s in range(len(estatus_list)):
        normStatus = statusList[s+1]/count
        normStatus = np.nan_to_num(normStatus)
        normData[s+1] = normStatus.reshape(dimy,dimx)
    normPop = count/nNodes
    normPop = np.nan_to_num(normPop)
    normPop = normPop.reshape(dimy,dimx)

    #fig = plt.figure(figsize=(20, 13), dpi= 200)
    fig = plt.figure(figsize=(12, 7), dpi= 200)
    n_cols = 2
    rows = (len(estatus_list)+1+n_cols-1) // n_cols
    maskCount = count.reshape(dimy,dimx)
    for s in range(len(estatus_list)): # For each Epidemic Status - create a chart
        plt.subplot(rows, n_cols, s+1)
        sns.heatmap(normData[s+1], xticklabels=False, yticklabels=False, annot = True, fmt=".1%", vmin=0, vmax=1, mask=maskCount<1)
        plt.title(estatus_list[s].capitalize())
        plt.xlim(0,dimx)
        plt.ylim(0,dimy)
    plt.subplot(rows, n_cols, len(estatus_list)+1)
    sns.heatmap(normPop, xticklabels=False, yticklabels=False, annot = True, fmt=".2%", vmin=0, vmax=1, mask=maskCount<1)
    plt.title("Population Density")
    plt.xlim(0,dimx)
    plt.ylim(0,dimy)
    #plt.subplots_adjust(hspace=0.2)
    plt.tight_layout()
    plt.savefig(f"{output_dir}/epidemicStatusNorm_heatmapPlot_{iter_id:03}.png")
    plt.close(fig)


def get_density_heatmap(pop_size, dimx, dimy, tile_id, output_dir):
    nNodes = pop_size
    dim = dimx * dimy
    
    df = pd.DataFrame()
    df['tile'] = tile_id
    dfcount = df.groupby('tile')['tile'].count()
    dfcount=dfcount.reindex(np.arange(0, dim)).fillna(0)
    count = dfcount.to_numpy()
    normPop = count/nNodes
    normPop = np.nan_to_num(normPop)
    normPop = normPop.reshape(dimy,dimx)

    fig = plt.figure(figsize=(16, 5), dpi= 200)
    plt.subplot(1, 2, 1)
    maskCount = count.reshape(dimy,dimx)
    sns.heatmap(normPop, xticklabels=False, yticklabels=False, annot = True, fmt=".1%", mask=maskCount<1, cmap="YlGnBu", cbar=False)
    plt.title("Population")
    plt.xlim(0,dimx)
    plt.ylim(0,dimy)
    
    plt.subplot(1, 2, 2)
    df = pd.DataFrame()
    heatTileID = np.array(range(dim)).reshape(dimy,dimx)
    sns.heatmap(normPop, xticklabels=False, yticklabels=False, annot = heatTileID, fmt="d", cmap="YlGnBu")
    plt.title("Tiles")
    plt.xlim(0,dimx)
    plt.ylim(0,dimy)
    plt.tight_layout()
    plt.savefig(f"{output_dir}/densityTileID.png")
    plt.close(fig)


###################################
# BUILD DATA STRUCTURE
###################################
def build_data_structure(config_file, graph_file, epidemic_file, city, output_dir):
    config = cp.ConfigParser()
    config.read(config_file)
    groups_name = eval(config.get('MAIN', 'groups_name'))
    ntiles = eval(config.get( 'MAIN', 'ntiles'))
    epidemic_statuses = eval(config.get( 'MAIN', 'epidemic_statuses'))
    dimx, dimy = ntiles[city]
    dim = dimx*dimy
    read_config = None
    
    g = igraph.Graph.Read_Pickle(graph_file) 
    e = pickle.load( open( epidemic_file, "rb" ) )

    nNodes = g.vcount()
    noHH = 0
    setHH = set()
    for v in range(nNodes): 
        if g.vs[v]['household'] == -1:
            noHH += 1
        else:
            setHH.add(g.vs[v]['household'])
    nHH = len(setHH)
    setHH = None

    tile_id = g.vs['tile_id']
    hh_id = g.vs['household']
    type_id = g.vs['type']
    groups_size = {}
    for i in range(len(groups_name)):
        groups_size[i] = 0
    for v in range(nNodes):
        groups_size[g.vs[v]['type']] += 1
    g = None
    
    sim_steps = []  # sim_steps is a list of list [ [],[],[], ... ]
    nIterations = len(e)
    for i in range(nIterations): # for each iteration
        iteration = [0]*nNodes
        for v in range(nNodes): # for each node
            if v in e[i]['status']: # e[i]['status'] is a dictionary { NODE_ID: STATUS }
                iteration[v] = e[i]['status'][v]
            else:
                iteration[v] = sim_steps[i-1][v]
        sim_steps.append(iteration)
    e = None  
    # SAVE DATA STRUCTURE
    data = { 'tile_id': tile_id, 'hh_id':hh_id, 'type_id': type_id, 'sim_steps': sim_steps,
             'groups_size': groups_size, 'groups_name': groups_name, 'epidemic_statuses': epidemic_statuses, 
             'pop_size': nNodes, 'city': city, 'tiles': [dimx,dimy], 
             'nHouseholds': nHH, 'nIsolated': noHH }
             
    with open(f"{output_dir}/{city}_{FILE_NAME_PICKLE}", "wb") as fout:
        pickle.dump( data, fout, protocol=pickle.HIGHEST_PROTOCOL)
        
    return data
    
    
###################################
# DATA PLOTS
###################################    
def prepare_data_for_plotting(sim_steps, groups_name, epidemic_statuses, pop_size, nIterations, type_id):
    # A dictionary for each group { g1:{}, g2:{}, ... }
    # Each dictionary contains a list for each epidemic status { s1:[], s2:[], s3:[], ... }
    dataToPlot = {}   
    for g in range(len(groups_name)): # for each age group create a dictionary 
        dataToPlot[g] = {}  
        for s in range(len(epidemic_statuses)): # for each epidemic status create a list
            dataToPlot[g][s+1] = [0] * pop_size
    dataToPlot['total'] = {}
    for s in range(len(epidemic_statuses)):
        dataToPlot['total'][s+1] = [0] * pop_size
    for i in range(nIterations): # count 
        iteration = sim_steps[i]
        for v in range(pop_size):
            dataToPlot['total'][iteration[v]][i] += 1
            dataToPlot[type_id[v]][iteration[v]][i] += 1
            
    return dataToPlot

###################################
# PLOT ONLY ONE EPIDEMIC STATUS
###################################   
def line_plot_status(status, maxItr, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir):
    bbox = dict(boxstyle="round", fc="0.96", lw=0.2)
    for s in range(len(epidemic_statuses)): # For each Epidemic Status - create a chart
        if epidemic_statuses[s] != status:
            continue
        fig = plt.figure(figsize=(7, 4), dpi= 200)
        plt.rcParams.update({'font.size': FONT_SIZE})
        for g in range(len(groups_name)): # For each Age groups - create a line
            normValues = [ x/groups_size[g] for x in dataToPlot[g][s+1]]
            plt.plot( range(maxItr), normValues[:maxItr], label=groups_name[g] )
            if epidemic_statuses[s] == 'infected':
                maxY = max(normValues)
                maxX = normValues.index(maxY)
                plt.vlines(maxX, 0, maxY, colors='black', linestyles='solid', linewidth=0.5)
                plt.annotate(f'step {maxX}\n{maxY*100:.1f}%', xy=(maxX, maxY), xytext=(-3, -12), textcoords="offset points", ha='right', va='bottom', size='small', weight='normal', stretch='condensed', bbox=bbox )
                normValuesR = [ x/groups_size[g] for x in dataToPlot[g][s+2]]
                for idx in range(len(normValues)):
                    val = normValuesR[idx]+normValues[idx]
                    if val >= 0.9:
                        plt.vlines(idx, 0, maxY, colors='black', linestyles='solid', linewidth=0.5)
                        plt.annotate(f'step {idx}\nI+R > 90%\n{groups_name[g]}', xy=(idx, maxY), xytext=(2, -22), textcoords="offset points", ha='left', va='bottom', size='small', weight='normal', stretch='condensed', bbox=bbox )
                        break
        normValues = [ x/pop_size for x in dataToPlot['total'][s+1]]
        plt.plot( range(maxItr), normValues[:maxItr], linestyle='dotted', label='all', alpha=0.7 )
        plt.title(epidemic_statuses[s].title() , fontweight='normal', fontsize='large')
        plt.legend()
        plt.grid(True, alpha=0.2)
        plt.tight_layout()
        plt.savefig(f"{output_dir}/{epidemic_statuses[s]}_linePlot.pdf")
        plt.close(fig)    


###################################
# PLOT Infected and Recovered
###################################  
def line_plot_IR(maxItr, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir, colors):
    fig = plt.figure(figsize=(7, 4), dpi= 200)
    plt.rcParams.update({'font.size': FONT_SIZE})
    for s in range(len(epidemic_statuses)): # For each Epidemic Status - create a chart
        for g in range(len(groups_name)): # For each Age groups - create a line
            normValues = [ x/groups_size[g] for x in dataToPlot[g][s+1]]
            if epidemic_statuses[s] == 'infected':
                plt.plot( range(maxItr), normValues[:maxItr], color=colors[g], linestyle='solid')
            if epidemic_statuses[s] == 'recovered':
                plt.plot( range(maxItr), normValues[:maxItr], color=colors[g], linestyle='dashed')
        normValues = [ x/pop_size for x in dataToPlot['total'][s+1]]
        if epidemic_statuses[s] == 'infected':
            plt.plot( range(maxItr), normValues[:maxItr], color=colors[len(groups_name)] )
        if epidemic_statuses[s] == 'recovered':
            plt.plot( range(maxItr), normValues[:maxItr], linestyle='dashed', color=colors[len(groups_name)] )
    plt.title('Infected - Recovered' , fontweight='normal', fontsize='large')
    # Create the legend for the plot
    custom_lines = ['groups']
    custom_labels = ['']
    for g in range(len(groups_name)):
        custom_lines.append( Line2D([0], [0], color=colors[g]) )
    custom_lines.append(Line2D([0], [0], color=colors[len(groups_name)]))
    custom_labels.extend(groups_name)
    custom_labels.extend(['all',''])
    custom_lines.append('status')
    custom_lines.extend([ Line2D([0], [0], color='black', linestyle='solid' ),
                          Line2D([0], [0], color='black', linestyle='dashed') ] )
    custom_labels.extend(['infected','recovered'])
    plt.legend(custom_lines, custom_labels, handler_map={str: LegendTitle()})
    plt.grid(True, alpha=0.2)
    plt.tight_layout()
    plt.savefig(f"{output_dir}/IR_linePlot.pdf")
    plt.close(fig)
    
#############################################
# PLOT Susceptible, Infected and Recovered
#############################################  
def line_plot_SIR(maxItr, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir, colors):
    fig = plt.figure(figsize=(7, 4), dpi= 200)
    plt.rcParams.update({'font.size': FONT_SIZE})
    for s in range(len(epidemic_statuses)): # For each Epidemic Status - create a chart
        for g in range(len(groups_name)): # For each Age groups - create a line
            normValues = [ x/groups_size[g] for x in dataToPlot[g][s+1]]
            if epidemic_statuses[s] == 'infected':
                plt.plot( range(maxItr), normValues[:maxItr], linestyle='solid' )
            if epidemic_statuses[s] == 'recovered':
                plt.plot( range(maxItr), normValues[:maxItr], linestyle='dashed')
            if epidemic_statuses[s] == 'susceptible':
                plt.plot( range(maxItr), normValues[:maxItr], linestyle='dotted')
        normValues = [ x/pop_size for x in dataToPlot['total'][s+1]]
        if epidemic_statuses[s] == 'infected':
            plt.plot( range(maxItr), normValues[:maxItr], linestyle='solid' )
        if epidemic_statuses[s] == 'recovered':
            plt.plot( range(maxItr), normValues[:maxItr], linestyle='dashed')
        if epidemic_statuses[s] == 'susceptible':
            plt.plot( range(maxItr), normValues[:maxItr], linestyle='dotted' )
    plt.title('SIR' , fontweight='normal', fontsize='large')
    # Create the legend for the plot
    custom_lines = ['groups']
    custom_labels = ['']
    for g in range(len(groups_name)):
        custom_lines.append(Line2D([0], [0], color=colors[g]))
    custom_lines.append(Line2D([0], [0], color=colors[len(groups_name)]))
    custom_labels.extend(groups_name)
    custom_labels.extend(['all',''])
    custom_lines.append('status')
    custom_lines.extend([ Line2D([0], [0], color='black', linestyle='dotted'),
                          Line2D([0], [0], color='black', linestyle='solid' ),
                          Line2D([0], [0], color='black', linestyle='dashed') ] )
    custom_labels.extend(epidemic_statuses)
    plt.legend(custom_lines, custom_labels, handler_map={str: LegendTitle()})
    # Show the grid
    plt.grid(True, alpha=0.2)
    plt.tight_layout()
    plt.savefig(f"{output_dir}/SIR_linePlot.pdf")
    plt.close(fig)

DEFAULT_CONFIG = 'config/analysis.ini'
FILE_NAME_PICKLE = 'usnData.pickle'
def main():
    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-g', '--graph-file', dest='g_file', required=True, type=str, help='')
    parser.add_argument('-e', '--epidemic-file', dest='e_file', required=True, type=str, help='')
    parser.add_argument('-c', '--config-file', dest='config_file', required=True, default=DEFAULT_CONFIG, type=str, help='')
    parser.add_argument('-o', '--output', dest='output_dir', required=True, type=str, help='')
    parser.add_argument('-y', '--city', dest='city', required=True, type=str, help='')
    parser.add_argument('--plot', dest='plot', action='store_true', default=False, help='')
    parser.add_argument('--dump', dest='dump', action='store_true', default=False, help='')

    args = parser.parse_args()
    graph_file =  args.g_file
    epidemic_file = args.e_file
    config_file = args.config_file
    output_dir = args.output_dir
    city = args.city
    plot = args.plot
    dump = args.dump
    
    # Creates the Output Directory
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass
                
    if not dump and not plot:
        sys.exit("WARNING: At least one parameter between --dump or --plot must be provided")
    else:
        # if os.path.isfile(f"{output_dir}/{city}_{FILE_NAME_PICKLE}"):
        #     data = pickle.load( open( f"{output_dir}/{city}_{FILE_NAME_PICKLE}", "rb" ) )
        # else:
        data = build_data_structure(config_file, graph_file, epidemic_file, city, output_dir)

    tile_id = data['tile_id']
    hh_id = data['hh_id']
    type_id = data['type_id']
    sim_steps = data['sim_steps']
    groups_size = data['groups_size']
    groups_name = data['groups_name']
    epidemic_statuses = data['epidemic_statuses']
    pop_size = data['pop_size']
    city = data['city']
    dimx, dimy = data['tiles']
    dim = dimx*dimy
    nHH = data['nHouseholds']
    noHH = data['nIsolated']
    nIterations = len(sim_steps)
    data=None
    
    print()
    print(f"City: {city}")
    print(f"Population Size: {pop_size}")
    print(f"Age Groups Names: {groups_name}")
    print(f"Age Groups Sizes: {groups_size}")
    print(f"Epidemic Statuses: {epidemic_statuses}")
    print(f"Number of Iterations: {nIterations}")
    print(f"Number of Households {nHH}")
    print(f"Number of Nodes in any Households: {noHH}")
    print(f"Number of Tiles: {dimx}x{dimy} = {dim}")
    print(f"Tiles max ID: {max(tile_id)} min ID: {min(tile_id)}")
    print()

    if not plot:
        sys.exit()
        
    colors = []
    for _, color in mcolors.TABLEAU_COLORS.items():
        colors.append(color)
    maxItr = 100
    dataToPlot = prepare_data_for_plotting(sim_steps, groups_name, epidemic_statuses, pop_size, nIterations, type_id)
    line_plot_status('infected', maxItr, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir)
    line_plot_IR(nIterations, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir, colors)
    line_plot_SIR(nIterations, epidemic_statuses, groups_name, dataToPlot, groups_size, pop_size, output_dir, colors)
        
    #for i in range(nIterations):
    #    get_heatmap(i, data, output_dir)
    #get_heatmap(0, data, output_dir)
    
    get_density_heatmap(pop_size, dimx, dimy, tile_id, output_dir)

if __name__ == "__main__":
    main()
    
    
    
