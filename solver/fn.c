#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multiroots.h>

int yf (const gsl_vector *x, void *params, gsl_vector *f) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   double *k = p->k;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,deg_i,deg_j,y_IJ,L_IJ; 
       
   int mpairs = (int)(m*(m+1)/2);
   for(i=0; i<n+mpairs; i++) {
        gsl_vector_set(f,i,0);
   }

// #pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      deg_i = gsl_vector_get(f,i);
      I = block[i];
#pragma omp parallel for reduction(+:deg_i)
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
      	 deg_j = gsl_vector_get(f,j);
	 J = block[j];
//	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 if(I<J){
		b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 } else {
		b2b_id = n+J*(m-1)-(int)(J*(J-1)/2)+I;
	 }
      	 y_IJ = gsl_vector_get(x,b2b_id);
      	 L_IJ = gsl_vector_get(f,b2b_id);
	 deg_i += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 deg_j += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
         gsl_vector_set(f,j,deg_j);
	 L_IJ += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
         gsl_vector_set(f,b2b_id,L_IJ);
      }
      gsl_vector_set(f,i,deg_i);
   }

#pragma omp parallel for 
   for (i=0;i<n+(int)(m*(m+1)/2);i++){
      gsl_vector_set(f,i,gsl_vector_get(f,i)-k[i]);
   }

   return GSL_SUCCESS;
}

     
int yf_deriv (const gsl_vector *x, void *params, gsl_matrix *df) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,a,b,c,y_IJ; 

   int mpairs = (int)(m*(m+1)/2);
   for(i=0; i<n+mpairs; i++) {
      for(j=0; j<n+mpairs; j++) {
        gsl_matrix_set(df,i,j,0);
      }
   }

// #pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      I = block[i];
      b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+I;
      y_IJ = gsl_vector_get(x,b2b_id);
      //b = x_i*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
      //gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+2*b);
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
	 J = block[j];
//	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 if(I<J){
		b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 } else {
		b2b_id = n+J*(m-1)-(int)(J*(J-1)/2)+I;
	 }
	 y_IJ = gsl_vector_get(x,b2b_id);
      	 
	 a = x_i*x_j/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 b = x_i*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 c = x_j*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 
         gsl_matrix_set(df,i,b2b_id,gsl_matrix_get(df,i,b2b_id)+a);
         gsl_matrix_set(df,j,b2b_id,gsl_matrix_get(df,j,b2b_id)+a);
         gsl_matrix_set(df,b2b_id,b2b_id,gsl_matrix_get(df,b2b_id,b2b_id)+a);
         
	 gsl_matrix_set(df,b2b_id,i,gsl_matrix_get(df,b2b_id,i)+c);
         gsl_matrix_set(df,b2b_id,j,gsl_matrix_get(df,b2b_id,j)+b);

	 gsl_matrix_set(df,i,j,+b);
	 gsl_matrix_set(df,j,i,+c);
         gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+c);
         gsl_matrix_set(df,j,j,gsl_matrix_get(df,j,j)+b);

      }
   }

   return GSL_SUCCESS;
}
     
int yf_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *df) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   double *k = p->k;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,deg_i,deg_j,y_IJ,L_IJ,a,b,c; 

   int mpairs = (int)(m*(m+1)/2);
   for(i=0; i<n+mpairs; i++) {
        gsl_vector_set(f,i,0);
         for(j=0; j<n+mpairs; j++) {
        	gsl_matrix_set(df,i,j,0);
	}
   }
       
// #pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      deg_i = gsl_vector_get(f,i);
      I = block[i];
      b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+I;
      y_IJ = gsl_vector_get(x,b2b_id);
      //b = x_i*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
      //gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+2*b);
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
      	 deg_j = gsl_vector_get(f,j);
	 J = block[j];
//	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 if(I<J){
		b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 } else {
		b2b_id = n+J*(m-1)-(int)(J*(J-1)/2)+I;
	 }
      	 y_IJ = gsl_vector_get(x,b2b_id);
      	 
	 a = x_i*x_j/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 b = x_i*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 c = x_j*y_IJ/((1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ));
	 
         gsl_matrix_set(df,i,b2b_id,gsl_matrix_get(df,i,b2b_id)+a);
         gsl_matrix_set(df,j,b2b_id,gsl_matrix_get(df,j,b2b_id)+a);
         gsl_matrix_set(df,b2b_id,b2b_id,gsl_matrix_get(df,b2b_id,b2b_id)+a);

         gsl_matrix_set(df,b2b_id,i,gsl_matrix_get(df,b2b_id,i)+c);
         gsl_matrix_set(df,b2b_id,j,gsl_matrix_get(df,b2b_id,j)+b);

	 L_IJ = gsl_vector_get(f,b2b_id);
	 deg_i += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,i,deg_i);
	 deg_j += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,j,deg_j);
	 L_IJ += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,b2b_id,L_IJ);
	 gsl_matrix_set(df,i,j,+b);
	 gsl_matrix_set(df,j,i,+c);
         gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+c);
         gsl_matrix_set(df,j,j,gsl_matrix_get(df,j,j)+b);
      }
   }

   for (i=0;i<n+(int)(m*(m+1)/2);i++){
      gsl_vector_set(f,i,gsl_vector_get(f,i)-k[i]);
   }

   return GSL_SUCCESS;
}


int print_state (unsigned int iter, gsl_multiroot_fdfsolver *s, unsigned int n, FILE *fout){
    int i, l;
    double residual = 0;
    for (i=0;i<n;i++){
        residual += abs(gsl_vector_get (s->f, i));
    }
    if (n>10){
	    l = 10;
    }else{
	    l = n;
    }
    fprintf (stdout, "iter = %3u\n residual = %.3f\n", iter, residual);
    fprintf (stdout, "x[0:%d] = ", l);
    for (i=0;i<l;i++){
        fprintf (stdout, "%.3f ", gsl_vector_get (s->x, i));
    }
    fprintf (stdout, "\n");
    fprintf (stdout, "f(x)[0:%d] = ", l);
    for (i=0;i<l;i++){
        fprintf (stdout, "%.3f ", gsl_vector_get (s->f, i));
    }
    fprintf (stdout, "\n");
}

