import configparser as cp
from pyproj import CRS
from pyproj import Transformer
import pyproj
import fiona
import geopandas as gp
import contextily as ctx
import numpy as np
import matplotlib
import  matplotlib.pylab as plt

plt.rcParams["figure.figsize"] = (8,8)
plt.style.use('ggplot')

# Esplicitly computes all the vertices of the grid tiles, the tiles centers and
# the population inside each tile.
def build_grid_dictionary (territory, pop2d=None):
    """ Create all the vertices in the grid.
        Used for debugging.
    """
    # Get input var from territory 
    x0 , y0  = territory['origin'] 
    ntx, nty = territory['ntiles']
    tx , ty  = territory['tile']
    tx2 = tx/2.0
    ty2 = ty/2.0

    grid = {}
    
    x, y = x0, y0
    for j in range(nty):
        for i in range(ntx):
            tile  = [(x, y), (x, y+ty), (x+tx, y+ty),(x+tx, y)]
            ctile = (x + tx2, y + ty2)
            gid   = i + j*ntx
            
            # Add popultaion to each tile
            if pop2d is not None:
                grid[gid]  = {'coord':ctile, 'vertices': tile, 'value': pop2d[i,j]}
            # Population is not defined
            else:
                grid[gid]  = {'coord':ctile, 'vertices': tile, 'value': 0}
            x = x + tx
        y = y + ty
        x = x0

    return grid

# Read the territory info from the config file and compute the projected version
def build_projected_territory(config_file, inCRS, outCRS, city=None):
    """ Read the config file, build a dictionary that stores vaious information about
        the territory.

        Args: 
            config_file (:obj:`str`): path to the config file.

        Returns: 
            (:obj:`dict`): a dictionary containing territory information.

            ::

                territory   = {
                    'origin':  (ox, oy),   # bottm left
                    'center':  center,     # gird center
                    'bbox':    bbox,       # bbox = [sw, nw, ne, se]
                    'borders': borders,    # borders = (max_x, min_x, max_y, min_y)
                    'tile':    tile,       # tuple with dimensions of the tile in meters
                    'ntiles':  ntiles      # tuple with the number of tiles in the grid 
                }


    """
    config = cp.ConfigParser()
    config.read(config_file)
    # coordinates (lat,lon) of the bottom left corner of the box in WGS84
    if not city:
        city   = eval(config.get('CITY','city')) 
    origin = eval(config.get('TERRITORY','origin'))[city] 
    # size in km of the h,w dimensions of each tile in the grid
    tile = eval(config.get('TERRITORY','tile'))
    # number of tiles in the grid in the h, w directions 
    ntiles = eval(config.get('TERRITORY','ntiles'))[city]

    # compute total height and width of the grid == bounding box == bbox
    xdim = tile[0]*ntiles[0]
    ydim = tile[1]*ntiles[1]
    
    (ox, oy) = utm_origin = project_coord((origin[1], origin[0]), inCRS, outCRS)
    
    sw = (ox, oy) 
    nw = (ox, oy + ydim)
    ne = (ox + xdim, oy + ydim)
    se = (ox + xdim, oy)
    bbox = [sw, nw, ne, se]
    
    # Compute territory borders and center
    min_x = ox
    min_y = oy
    max_x = ox + xdim
    max_y = oy + ydim
    
    borders = (max_x, min_x, max_y, min_y)
    
    center  = (ox + xdim/2.0, oy + ydim/2.0)
    
    # return a dictionary
    territory   = {
        'origin':  (ox, oy), 
        'center':  center,
        'bbox':    bbox, 
        'borders': borders,   
        'tile':    tile,
        'ntiles':  ntiles
    }
    print(territory)
    print(xdim, ydim)

    return territory
    



###############################################################################
#
#                           VARIOUS FUNCTIONS
#
###############################################################################

#Get info about projection
def get_CRS_info(csr_id):
    crs_utm = CRS.from_user_input(4326)
    return crs_utm

# Dummy function to check the dimensions of the bounding 
# box computed by euclidean distance
def test_bbox_dimensions(territory):
    sw, nw, ne, se = territory['bbox']
    length_we = np.sqrt((sw[0]-se[0])*(sw[0]-se[0]) + (sw[1] - se[1])*(sw[1] - se[1]))
    length_ns = np.sqrt((sw[0]-nw[0])*(sw[0]-nw[0]) + (sw[1] - nw[1])*(sw[1] - nw[1]))
    print(length_ns, length_we)
    return

# From wgs84 to wgs84_32n
# coord = (lat, lon)
def project_coord(coord, inCRS, outCRS):
    inproj     = pyproj.CRS(inCRS)
    outproj    = pyproj.CRS(outCRS)
    
    transf = Transformer.from_crs(inproj, outproj)
    a = transf.transform(coord[1], coord[0])
    return a

# interleave two numpy arrays
def interleave(a, b):
    c = np.empty((a.size + b.size,), dtype=a.dtype)
    c[0::2] = a
    c[1::2] = b
    return c


# Just a wrapper to read a specific filed from config.ini
def read_from_config(config_file, section, name):
    """ Wrapper around configparser to read a specific value
        from the .ini configuration file.
    """
    config = cp.ConfigParser()
    config.read(config_file)
    var = eval(config.get(section, name)) 
    return var

def plot_geojson(geodf, myname):
    # Convert to the same epsg as common maps (openstreet map)
    fig = plt.figure()
    geodf = geodf.to_crs(epsg=3857)
    ax = geodf.plot(figsize=(10, 10), alpha=0.5, edgecolor='k')
    ctx.add_basemap(ax, zoom = 12)
    try:
        myname = myname + '.pdf'
    except TypeError:
        myname = myname.with_suffix(".pdf")
    plt.savefig(myname)
    print("geojson plot saved to {}".format(myname))
    return

def plot_numpy_matrix(data, outfile, change_lim=True, cmap='viridis'):

    fig = plt.figure(figsize=(12, 8), dpi=300, facecolor='w', edgecolor='k')

    xdim = data.shape[1]
    ydim = data.shape[0]

    ax = fig.add_subplot(111)
    im = ax.imshow(data, interpolation='none', cmap=cmap)
    c  = fig.colorbar(im, ax=ax, shrink=.5, pad=.01, aspect=20)

    if change_lim:
        plt.xlim(0, xdim)
        plt.ylim(0, ydim)
    try:
        outfile = outfile + '.pdf'
    except TypeError:
        outfile = outfile.with_suffix(".pdf")
    
    plt.savefig(outfile)
    return


