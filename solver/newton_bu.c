#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_linalg.h>
// #include <gsl/gsl_errno.h>
// #include <gsl/gsl_math.h>
// #include <gsl/gsl_roots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#define MINARG 5
#define MAXELEMENTS 100000000
#define MAXLINE 1024


double L(int n, int m, double *theta, double *eta, double *k, double *K, int *block){
       int i,j,I,J,IJ;
       double result = 0.0;
       int mpairs = (int)(m*(m+1)/2);
       for (i=0;i<n;i++){
           I = block[i];
           for (j=i+1;j<n;j++){
               J = block[j];
               if(I<J){
                   IJ = I*(m-1)-(int)(I*(I-1)/2)+J;
               } else {
                   IJ = J*(m-1)-(int)(J*(J-1)/2)+I;
               }
               result += log(1+exp(-theta[i]-theta[j]-eta[IJ]));
	   }   
           result += theta[i]*k[i];
       }
       
       for (i=0;i<mpairs;i++){
               result += eta[i]*K[i];
       }
       return result;
}

void grad_L(int n, int m, double *theta, double *eta, double *k, double *K, int *block, gsl_vector *result){
       int i,j,I,J,IJ;
       int mpairs = (int)(m*(m+1)/2);
       double x;
       gsl_vector_set_zero(result); 
       for (i=0;i<n;i++){
           gsl_vector_set(result,i,gsl_vector_get(result,i)+k[i]);
           I = block[i];
           for (j=i+1;j<n;j++){
               J = block[j];
               if(I<J){
                   IJ = I*(m-1)-(int)(I*(I-1)/2)+J;
               } else {
                   IJ = J*(m-1)-(int)(J*(J-1)/2)+I;
               }
               x = exp(-theta[i]-theta[j]-eta[IJ]);
	       x = x/(1+x);
               gsl_vector_set(result,i,gsl_vector_get(result,i)-x);
               gsl_vector_set(result,j,gsl_vector_get(result,j)-x);
               gsl_vector_set(result,n+IJ,gsl_vector_get(result,n+IJ)-x);
	   }   
       }
       for (i=0;i<mpairs;i++){
               gsl_vector_set(result,n+i,gsl_vector_get(result,n+i)+K[i]);
       }
       return;
}

void hess_L(int n, int m, double *theta, double *eta, double *k, double *K, int *block, gsl_matrix *result){
       int i,j,I,J,IJ;
       int mpairs = (int)(m*(m+1)/2);
       double x;
       gsl_matrix_set_zero(result); 
       for (i=0;i<n;i++){
           I = block[i];
           for (j=i+1;j<n;j++){
               J = block[j];
               if(I<J){
                   IJ = I*(m-1)-(int)(I*(I-1)/2)+J;
               } else {
                   IJ = J*(m-1)-(int)(J*(J-1)/2)+I;
               }
               x = exp(-theta[i]-theta[j]-eta[IJ]);
	       x = x/((1+x)*(1+x));
	       gsl_matrix_set(result,i,i,gsl_matrix_get(result,i,i)+x);
	       gsl_matrix_set(result,j,j,gsl_matrix_get(result,j,j)+x);
	       gsl_matrix_set(result,i,j,gsl_matrix_get(result,i,j)+x);
	       gsl_matrix_set(result,j,i,gsl_matrix_get(result,j,i)+x);
	       gsl_matrix_set(result,n+IJ,n+IJ,gsl_matrix_get(result,n+IJ,n+IJ)+x);
	       gsl_matrix_set(result,n+IJ,i,gsl_matrix_get(result,n+IJ,i)+x);
	       gsl_matrix_set(result,i,n+IJ,gsl_matrix_get(result,i,n+IJ)+x);
	       gsl_matrix_set(result,n+IJ,j,gsl_matrix_get(result,n+IJ,j)+x);
	       gsl_matrix_set(result,j,n+IJ,gsl_matrix_get(result,j,n+IJ)+x);
	   }   
       }
       return;
}


void init_random(double *x, int n){
    int i;
    for (i=0;i<n;i++){
        x[i] = rand()/RAND_MAX;  
    }
}

void init_alt_1(double *x, int n, double *k, int degs){
    int i;
    double L = 0.0;
    for (i=0;i<n;i++){
        L += k[i];
    }
    if (degs){
        L = L/2;
    }
    for (i=0;i<n;i++){
        x[i] = -log(k[i]/sqrt(2*L)); 
    }
}

void init_alt_2(double *x, int n, double *k){
    int i;
    for (i=0;i<n;i++){
        x[i] = -log(k[i]/sqrt((float)n));
    }
}


void get_expected_degrees(int n, int m, double *theta, double *eta, int *block, double *degrees){
    int i, j, I, J, IJ;
    int mpairs = (int)(m*(m+1)/2);
    double num, denom, y_IJ;
    memset(degrees, 0, sizeof(double)*(n+mpairs)); 
    for (i=0;i<n;i++){
        I = block[i];
        for (j=i+1;j<n;j++){
            J = block[j];
            if(I<J){
                IJ = I*(m-1)-(int)(I*(I-1)/2)+J;
            } else {
                IJ = J*(m-1)-(int)(J*(J-1)/2)+I;
            }
            y_IJ = eta[IJ];
            denom = 1.0+exp(-theta[i]-theta[j]-y_IJ); // x[i]*x[j]*y_IJ; // 
            num = exp(-theta[i]-theta[j]-y_IJ); // x[i]*x[j]*y_IJ; // 
            degrees[i] += num/denom;
            degrees[j] += num/denom;
            degrees[n+IJ] += num/denom;
        }
    }
}
   

double get_made(int n, int mpairs, double *exp_deg, double *k, double *K){
    int i; 
    double made=0.0;
    for (i=0;i<n;i++){
        if (fabs(exp_deg[i]-k[i])>made){
            made = fabs(exp_deg[i]-k[i]);
        }
    }
    for (i=0;i<mpairs;i++){
        if (fabs(exp_deg[n+i]-K[i])>made){
            made = fabs(exp_deg[n+i]-K[i]);
        }
    }
    return made;
}
   

void print_degrees(int h, int n, int mpairs, double *exp_deg, double *k, double *K, FILE *flog){
    int i;
    if (n+mpairs<h){
        h = n+mpairs;
    }
    fprintf(flog, "%8s %8s\n", "k*", "<k>");
    for (i=0;i<h;i++){
        if (i<n){
            fprintf(flog, "%d: %8.3f %8.3f\n", i, k[i], exp_deg[i]);
        }else{
            fprintf(flog, "%d: %8.3f %8.3f\n", i, K[i-n], exp_deg[i]);
        }
    }
}


double compute_quality_1(int n, int m, double *u1, double *u2, double *v1, double *v2){
    int i;
    double quality = 0.0;
    for (i=0;i<n;i++){
        quality += (u1[i]-u2[i])*(u1[i]-u2[i]);
    }
    for (i=0;i<m;i++){
        quality += (v1[i]-v2[i])*(v1[i]-v2[i]);
    }
    quality = sqrt(quality);
    return quality;
}


double compute_quality_2(int n, gsl_vector *u){
    int i;
    double temp, quality = 0.0;
    for (i=0;i<n;i++){
        temp = gsl_vector_get(u,i);
        quality += temp*temp;
    }
    quality = sqrt(quality);
    return quality;
}



int main (int argc, char *argv[]) {
       int sign;
       unsigned int i, j, iter, max_iter, n, m, mpairs, dim;
       unsigned int *block;
       char line[MAXLINE];
       double precision, quality, made, temp, y_IJ, alpha, eps, w, gamma, beta, t, l1, l2;
       double *k, *K, *theta, *eta, *theta_temp, *eta_temp, *exp_deg;
       /*---PARAMS---*/
       eps = 0.001;
       gamma = 0.1;
       beta = 0.1;
       /*------------*/
       FILE *flog = fopen("log.txt", "w");
       //
       flog = stdout;
       //
       fprintf(flog, "solving the system with Newton method\n"); 
       srand(time(NULL));
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_nodes n_blocks precision max_iter\n",argv[0]);
	  exit(1);
       }
       n=atoi(argv[1]);
       m=atoi(argv[2]);
       mpairs = (int)(m*(m+1)/2);
       dim = n+mpairs;
       precision=atof(argv[3]);
       max_iter=atoi(argv[4]);
       if(dim>MAXELEMENTS) {
	fprintf(stderr,"Too many elements: max number is %d\n",MAXELEMENTS);
	exit(1);
       }
       theta = (double *)malloc(sizeof(double)*n);
       eta = (double *)malloc(sizeof(double)*mpairs);
       if((theta==NULL)||(eta==NULL)){
	fprintf(stderr,"Could not get memory for %d items of theta+eta\n",dim);
	exit(1);
       }
       k=(double *)malloc(sizeof(double)*n);
       K=(double *)malloc(sizeof(double)*mpairs);
       if((k==NULL)||(K==NULL)){
	fprintf(stderr,"Could not get memory for %d items of k+K\n",dim);
	exit(1);
       }
       block=(unsigned int *)malloc(sizeof(unsigned int)*n);
       if(block==NULL) {
	fprintf(stderr,"Could not get memory for %d items of block\n",n);
	exit(1);
       }
       for(i=0; i<n; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf %d %lf",k+i,block+i,&temp);
	theta[i] = -log(temp);
       }
       for(j=0; j<mpairs; j++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i+j);
		exit(1);
	}
	sscanf(line,"%lf %lf",K+j,&temp);
	eta[j] = -log(temp);
       }
       
       /* alternative initializations */
       // init_random(eta, n);
       // init_random(eta, mpairs);
       // init_alt_1(theta, n, k, 1);
       // init_alt_1(eta, mpairs, K, 0);
       // init_alt_2(theta, n, k);
       // init_alt_2(eta, mpairs, K);

       // double l = L(n, m, theta, eta, k, K, block);
       // printf("the value of the function to be optimized at the initial value is %lf\n", l);

       // double *dl = (double *)malloc(sizeof(double)*(n+mpairs));
       // double *Hl[n+mpairs];
       // for (i=0;i<n+mpairs;i++){
       //     Hl[i] = (double *)malloc(sizeof(double)*(n+mpairs)); 
       // }
       
       gsl_matrix *Hl;
       gsl_vector *x, *dl;
       gsl_permutation *perm;
       gsl_vector_view diag;

       /* allocate Hl, x, dl, perm */
       Hl = gsl_matrix_alloc(dim,dim);
       x = gsl_vector_alloc(dim);
       dl = gsl_vector_alloc(dim);
       perm = gsl_permutation_alloc(dim);

       exp_deg = (double *)malloc(sizeof(double)*(dim));
       theta_temp = (double *)malloc(sizeof(double)*n);
       eta_temp = (double *)malloc(sizeof(double)*mpairs);
       iter = 0;
       /* print initial state */
       // printf("iter %5d:\n", iter);
       // printf("theta: %lf, %lf, %lf, %lf\n", theta[0], theta[1], theta[2], theta[3]);
       // printf("eta: %lf, %lf, %lf\n", eta[0], eta[1], eta[2]);
       // get_expected_degrees(n, m, theta, eta, block, exp_deg);
       // made = get_made(n, mpairs, exp_deg, k, K);
       // print_degrees(20, n, mpairs, exp_deg, k, K);
       // quality = 0.0;
       // printf("quality=%lf, made=%lf\n", quality, made);
       /* start iterating */
       do {
           iter++;
           grad_L(n, m, theta, eta, k, K, block, dl);
           fprintf(flog, "grad computed\n");
           gsl_vector_scale(dl, -1.0);
	   hess_L(n, m, theta, eta, k, K, block, Hl);
           fprintf(flog, "hess computed\n");
	   diag = gsl_matrix_diagonal(Hl);
	   gsl_vector_add_constant(&diag.vector,eps);
	   
	   /* copy Hl into a temporary matrix C */ 
           // gsl_matrix *C;
           // C = gsl_matrix_alloc(dim,dim);
           // gsl_matrix_memcpy (C, Hl); 
           
	   /* LU decomposition and forward&backward substition */
           gsl_linalg_LU_decomp(Hl, perm, &sign);
           gsl_linalg_LU_solve(Hl, perm, dl, x);
           
           /* compute b = C*x */
           // gsl_vector *b;
           // b = gsl_vector_alloc(dim);
           // gsl_blas_dgemv(CblasNoTrans, 1.0, C, x, 0.0, b);

           // /* print b compared with dl (the two should be equal) */
           // printf("index             Hl*x              dl\n");
           // for(i=0;i<dim; i++){
           //     printf("%5d %15.7f %15.7f\n", i, gsl_vector_get(b, i), gsl_vector_get(dl, i));
           // }
           // gsl_vector_free(b);
           // gsl_matrix_free(C);
	   // break;
	   
	   /* select proper alpha */
	   alpha = 1.0;
	   gsl_blas_ddot(dl, x, &t);
	   t = -t;
	   l1 = L(n, m, theta, eta, k, K, block);
           for (i=0;i<n;i++){
	       theta_temp[i] = theta[i];
	   }
           for (i=0;i<mpairs;i++){
	       eta_temp[i] = eta[i];
	   }
	   while (0==0) {
               for (i=0;i<n;i++){
	           theta[i] = theta_temp[i]+alpha*gsl_vector_get(x,i);
	       }
               for (i=0;i<mpairs;i++){
	           eta[i] = eta_temp[i]+alpha*gsl_vector_get(x,n+i);
	       }
	       l2 = L(n, m, theta, eta, k, K, block);
	       if (l2>=l1+gamma*alpha*t){
	           alpha = beta*alpha;
	       }else{
	           break;
	       }
	   }	   
           quality = compute_quality_1(n,mpairs,theta,theta_temp,eta,eta_temp);
           // quality = compute_quality_2(dim,dl);
           /* compute expected degrees based on the obtained values */
           // get_expected_degrees(n, m, theta, eta, block, exp_deg);
           // fprintf(flog, "theta, theta_temp:\n");
	   // for (i=0;i<n;i++){
	   //         fprintf(flog, "%lf, %lf\n", theta[i], theta_temp[i]);
           // }
           // fprintf(flog, "\neta, eta_temp:\n");
	   // for (i=0;i<mpairs;i++){
	   //         fprintf(flog, "%lf, %lf\n", eta[i], eta_temp[i]);
           // }
	   // fprintf(flog, "\n");
           // printf("eta: %lf, %lf, %lf\n", eta[0], eta[1], eta[2]);
	   /* compute maximum absolute degree error */
           // made = get_made(n, mpairs, exp_deg, k, K);
           // print_degrees(20, n, mpairs, exp_deg, k, K);
	   // printf("quality=%lf, made=%lf\n", quality, made);
	   fprintf(flog, "Iter=%5d, precision=%13.11f\n", iter, quality);
       } while (quality>=precision && iter<max_iter);
           
       /* compute expected degrees based on the obtained values */
       get_expected_degrees(n, m, theta, eta, block, exp_deg);
       /* compute maximum absolute degree error */
       made = get_made(n, mpairs, exp_deg, k, K);
       fprintf(flog, "Solver converged after %d iterations!\nFinal precision=%13.11f, final MADE=%13.11f\n", iter, quality, made);
       /* imposed and expected degree for the first 20 vertices of the network */
       // print_degrees(n+mpairs, n, mpairs, exp_deg, k, K, flog);
      
       for (i=0;i<n;i++){
           printf("%lf\n", exp(-theta[i]));
       }
       for (i=0;i<mpairs;i++){
           printf("%lf\n", exp(-eta[i]));
       }
       /* free all */
       gsl_matrix_free(Hl);
       gsl_vector_free(x);
       gsl_vector_free(dl);
       gsl_permutation_free(perm);

       fclose(flog);
       return 0;
}
