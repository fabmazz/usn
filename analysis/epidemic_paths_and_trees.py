#!/usr/bin/env python
# coding: utf-8

# In[45]:


import os, sys
import pickle
import pandas as pd
import numpy as np
from scipy.sparse.linalg import eigsh
import igraph
from collections import Counter
import itertools

# PLOTTING
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

##########
from network_epidemics import *

##########
from edmonds import edmonds


# In[69]:

sns.set_style('whitegrid', rc={'figure.figsize':(17,6)})
sns.set_context("talk")

############## MACROS ###############
graph_fname = 'data/graphs/viterbo/muPOLYMOD_EP1_t500_min10pop/sb_graph.pickle'
# epidemics_paths = {f"combo{j}": f"./output/epidemics/viterbo/threshold/combo{j}/" for j in range(1,7)}
epidemics_paths = {}
epidemics_paths['HM']  = f'output/epidemics/viterbo/last_runs/threshold/combo1/'
epidemics_paths['SN']  = f'output/epidemics/viterbo/last_runs/threshold/combo2/'
epidemics_paths['HN']  = f'output/epidemics/viterbo/last_runs/threshold/combo3/'
epidemics_paths['AN']  = f'output/epidemics/viterbo/last_runs/threshold/combo4/'
epidemics_paths['DN']  = f'output/epidemics/viterbo/last_runs/threshold/combo5/'
epidemics_paths['ADN'] = f'output/epidemics/viterbo/last_runs/threshold/combo6/'
city = 'viterbo'
delta = 1/3
nruns = 100

recap_data = {'threshold value':[], 'configuration':[], 'threshold type':[]} 

for combo,epidemics_path in epidemics_paths.items():
    print(f'at {combo}')
    # try:
    plots_path = epidemics_path+'plots/'
    if not os.path.exists(plots_path):
        os.makedirs(plots_path)

    # **Epidemic Threshold**

    # In[2]:


    ## load social graph
    G = igraph.read(graph_fname)
    tile_count = Counter(G.vs['tile_id'])
    tile_count = [tile_count.get(i,0) for i in range(max(tile_count)+1)]
    source_tile = np.argmax(tile_count)
    ## get degrees
    degrees = []
    for fname in [fname for fname in os.listdir(epidemics_path.replace('threshold','R0')) if 'epi_init' in fname]:
        with open(os.path.join(epidemics_path.replace('threshold','R0'),fname), 'r') as fdegs:
            # lines = fdegs.readlines()
            # for line in lines: #[-nruns:]:
            #     line = line.strip()
            #     fields = line.split(', ')
            #     static_deg = int(fields[4])
            #     hh_deg = int(fields[5])-1
            #     if combo[-1]=='1':
            #         degs = [int(c) for c in fields[10:]]
            #     elif combo[-1] in ['2','3']:
            #         degs = [int(c)+hh_deg for c in fields[10:]]
            #     elif combo[-1]=='4':
            #         degs = [static_deg for c in fields[10:]]
            #     else:
            #         degs = [int(c)+hh_deg+(np.random.uniform(0,1,static_deg-hh_deg)>0.5).sum() for c in fields[10:]]
            #     degrees.extend(degs) 
            next(fdegs)
            for line in fdegs:
                fields = line.split(', ')
                static_deg = int(fields[4])
                hh_deg = int(fields[5])-1
                # ER
                if epidemics_path[-2]=='1':
                    degs = [int(c) for c in fields[10:]]
                # HH + FF (static)
                elif epidemics_path[-2]=='2':
                    degs = [static_deg for c in fields[10:]]
                # # HH + ER or PM
                # elif epidemics_path[-2] in ['2','3']:
                #     degs = [int(c)+hh_deg for c in fields[10:]]
                # HH + FF/2 + ER or PM
                else:
                    degs = [int(c)+hh_deg+(np.random.uniform(0,1,static_deg-hh_deg)>0.5).sum() for c in fields[10:]]
                degrees.extend(degs) 
    degrees = np.asarray(degrees)
    ## compute the three threshold approximations
    t1 = degrees.mean()/((degrees**2).mean()-degrees.mean())
    t_index = degrees.mean()/((degrees**2).mean()-2*degrees.mean())
    # t3 = 1/eigsh(G.get_adjacency_sparse().astype('f'),1)[0][0]
    # print(f'{combo}: the mean degree is {degrees.mean()}')
    # print(f'{combo}: the three threshold approximations are {t1*delta}, {t2*delta} and {t3*delta}')
    
    # t_all now disabled
    # degree_files = [os.path.join(epidemics_path,fname) for fname in os.listdir(epidemics_path) if 'infected_degrees' in fname and fname.endswith('.csv')]
    # df = pd.DataFrame()
    # for fname in degree_files:
    #     print(f'reading degrees from {fname}') 
    #     df_ = pd.read_csv(fname, skipinitialspace=True)
    #     df = pd.concat([df,df_])
    # if len(df):
    #     df['degree'] = df[['household','friendship','random']].sum(axis=1)
    #     degrees = df['degree'].to_numpy()
    #     # degrees = np.asarray(G.degree())
    #     ## compute the three threshold approximations
    #     # t1 = degrees.mean()/((degrees**2).mean()-degrees.mean())
    #     t_all = degrees.mean()/((degrees**2).mean()-2*degrees.mean())
    #     # t3 = 1/eigsh(G.get_adjacency_sparse().astype('f'),1)[0][0]
    #     # print(f'the three threshold approximations are {t1*delta}, {t2*delta} and {t3*delta}')
    # # else:
    # #     print('it was impossible to retrieve infected degrees! exit')
    # #     sys.exit()
    print(f'{combo}: the mean degree is {degrees.mean()}')
    print(f'{combo}: the threshold approximation is {t1*delta}') # with only index cases, {t_all*delta} with all infected')


    # In[3]:


    ## select files storing the result of the epidemic simulation
    filenames = []
    for root, dirs, files in os.walk(epidemics_path):
        for file in files:
            if file.endswith("iterations.pickle") and 'beta' in file:
                filenames.append(os.path.join(root, file))

    ## get iteration data containing information about the epidemic outbreak and extract the attack rate 
    data = {'beta':[], 'r':[]}
    for filename in filenames:
        fname = filename.rsplit('/',1)[-1]
        i = fname.find('beta')
        beta = float(fname[i+4:i+4+fname[i+4:].find('_')])
        with open(filename, 'rb') as f:
            iters = pickle.load(f)
        r = iters[-1]['node_count'][3]
        data['beta'].append(beta)
        data['r'].append(r)

    ## build and preview the dataframe
    df = pd.DataFrame(data)
    print(df.describe())
    print(df.head())


    # In[4]:


    ## create attack rate plot with inset
    plt.clf()
    g = sns.lineplot(data=df, x='beta', y='r', c='r')
    g.set_ylabel('attack rate', c='r')
    # g.axvline(t1*delta, 0, 1, c='b', label=r'$\frac{\langle k \rangle}{\langle k^2 \rangle - \lange k \rangle}$')
    # g.axvline(t2*delta, 0, 1, c='g', label=r'$\frac{\langle k \rangle}{\langle k^2 \rangle - 2\lange k \rangle}$')
    # g.axvline(t3*delta, 0, 1, c='y', label=r'$\Lambda^{-1}$')
    # axins = inset_axes(plt.gca(), "30%", "40%", loc='lower right', borderpad=1.7)
    # s = min([t1,t2,t3])
    # e = max([t1,t2,t3])
    # g.axvline(t_index*delta, 0, 1, c='b', label=r'$\tau_{\mathrm{index}}$')
    # g.axvline(t_all*delta, 0, 1, c='g', label=r'$\tau_{\mathrm{all}}$')
    # axins = inset_axes(plt.gca(), "30%", "40%", loc='lower right', borderpad=1.7)
    # s = min([t_index,t_all])
    # e = max([t_index,t_all])
    # g_inset = sns.lineplot(data=df.query('beta>(@s*@delta-(@e-@s)*@delta/2) and beta<(@e*@delta+(@e-@s)*@delta/2)'), x='beta', y='r', )
    # g_inset.axvline(t1*delta, 0, 1, c='b')
    # g_inset.axvline(t2*delta, 0, 1, c='g')
    # g_inset.axvline(t3*delta, 0, 1, c='y')
    # g_inset.axvline(t_index*delta, 0, 1, c='b')
    # g_inset.axvline(t_all*delta, 0, 1, c='g')
    # g_inset.set(xlabel=None, ylabel=None)
    # plt.legend()
    # plt.savefig(plots_path+'attack_rate.png')
    # print(f'attack rate plotted at {plots_path+"attack_rate.png"}')


    # In[5]:


    ## define function to compute the variability measure for a given set of attack rate values
    def compute_delta(r):
        return np.sqrt((r**2).mean()-(r.mean())**2)/r.mean()

    ## compute the variability measure for each set of epidemic runs having the same beta (contagion rate)
    delta_df = df.groupby('beta')[['r']].apply(compute_delta)
    delta_df = delta_df.reset_index()
    print(delta_df.describe())
    print(delta_df.head())


    ## plot the variability as a function of beta, with the thresholds
    # plt.clf()
    ax = g.twinx()
    ax = sns.lineplot(data=delta_df, x='beta', y='r', ax=ax, c='g')
    
    deltas = delta_df['r'].to_numpy()
    betas  = delta_df['beta'].to_numpy()
    bmax = betas[np.argmax(deltas)]
    ax.axvline(bmax, 0, 1, c='g', ls=':', label=r'$\beta_c^{\Delta}$')

    ax.axvline(t1*delta, 0, 1, c='y', label=r'$\beta_c^*$')
    # ax.axvline(t2*delta, 0, 1, c='g', label=r'$\frac{\langle k \rangle}{\langle k^2 \rangle - 2\lange k \rangle}$')
    # ax.axvline(t3*delta, 0, 1, c='y', label=r'$\Lambda^{-1}$')
    ax.axvline(t_index*delta, 0, 1, c='b', label=r'$\beta_c$')
    # ax.axvline(t_all*delta, 0, 1, c='g', label=r'$\tau_{\mathrm{all}}$')
    ax.set_ylabel('variability', c='g')
    plt.legend()
    plt.savefig(plots_path+'variability.png', bbox_inches='tight')
    print(f'variability plotted at {plots_path+"variability.png"}')

    # recap_data['threshold value'].extend([t1*delta,t_index*delta,bmax])
    # recap_data['threshold type'].extend([r'$\beta_c^*$',r'$\beta_c$',r'$\beta_c^{\Delta}$'])
    # recap_data['configuration'].extend([combo,combo,combo])
    recap_data['threshold value'].extend([t1*delta,bmax])
    recap_data['threshold type'].extend([r'$\beta_c^{\mathrm{HMF}}$',r'$\beta_c^{\Delta}$'])
    recap_data['configuration'].extend([combo,combo])

    continue
    # In[ ]:





    # **Invasion Trees**

    # In[6]:


    ## build path graphs and extract sources for all epidemic runs
    G = igraph.read(graph_fname)
    main_path = epidemics_path
    path_graphs = {}
    sources = {}
    for root, dirs, files in os.walk(main_path):
        for file in files:
            if file.endswith("infections.pickle") and 'beta' in file:
                i = file.find('beta')
                beta = float(file[i+4:i+4+file[i+4:].find('_')])
                run  = int(file[file.find('run')+4:file.find('_sir')])
                with open(os.path.join(root,file), 'rb') as f:
                    infections = pickle.load(f)
                path_graph = build_pathways(infections, G.vs['tile_id'])
                path_graphs.setdefault(beta, {})[run] = path_graph
                source = get_sources(infections, G.vs['tile_id'])
                sources.setdefault(beta, {})[run] = source


    # In[ ]:





    # In[7]:


    ## define functions to change graph format and compute the invasion tree
    def transform_graph(g):
        g_dict = {}
        for e in g.es:
            s,t = e.tuple
            w = e['weight']
            g_dict.setdefault(s,{})[t] = w
        return g_dict

    def inverse_transform_graph(g_dict):
        edges = []
        weights = []
        for k,v in g_dict.items():
            for h,w in v.items():
                edges.append((k,h))
                weights.append(w)
        g = igraph.Graph(edges=edges, directed=True, edge_attrs={'weight':weights})
        return g

    def compute_aggregate_graph(path_graphs):
        for path_graph in path_graphs:
            path_graph.es['weight'] = 1
        h = igraph.disjoint_union(path_graphs)
        h.contract_vertices(h.vs['tile_id'], combine_attrs={'tile_id':lambda x:x[0] if x else None})
        h = h.components('WEAK').giant()
        h.simplify(combine_edges={'weight':'sum'})
        h.es['weight'] = [np.sqrt(1-w/len(path_graphs)) for w in h.es['weight']]
        print('union graph generated')
        print(f'union graph has {h.vcount()} vertices and {h.ecount()} edges')
        print(f'min weight is {min(h.es["weight"])}, max weight is {max(h.es["weight"])}')
        if h.is_connected('STRONG'):
            s = 'strongly'
        elif h.is_connected('WEAK'):
            s = 'weakly'
        else:
            s = 'not'
        print(f'the graph is {s} connected') 
        return h

    def compute_invasion_tree(aggregate_graph, use_edmonds=True, source=None):
        if use_edmonds:
            if source is None:
                print('[error] no source given! returning None')
                return None
            h_dict = transform_graph(aggregate_graph)
            # for k,v in h_dict.items():
            #     print(k, v)
            t = edmonds.mst(source,h_dict)
            t = inverse_transform_graph(t)
            t.vs['tile_id'] = aggregate_graph.vs['tile_id']
        else:
            t = aggregate_graph.spanning_tree(weights=aggregate_graph.es['weight'], return_tree=True)
        return t


    # In[ ]:





    # In[8]:


    ## compute the tree
    aggregate_graph = compute_aggregate_graph([x for c in path_graphs.values() for x in c.values()])
    t_edmonds = compute_invasion_tree(aggregate_graph, source=source_tile)
    t_mst     = compute_invasion_tree(aggregate_graph, use_edmonds=False)


    # In[ ]:


    # Parameters
    ntiles    = {'firenze':(15, 12), 'sabaudia':(16, 20), 'viterbo':(52, 64), 'vo':(6, 6)}
    dimx      = ntiles[city][0]
    dimy      = ntiles[city][1]

    pop_array = np.zeros(dimx*dimy)
    from collections import Counter
    pop_dict = Counter(G.vs['tile_id'])
    for k,v in pop_dict.items():
        pop_array[k] = v
    c1 = pop_array.reshape(dimy,dimx)

    # Heatmap Parameters
    cmap='viridis'#"YlGn" #'hot_r'
    annot = False
    annot_kws={"fontsize":14}
    vmin = np.min([np.min(c1), np.min(c1)])
    vmax = np.max([np.max(c1), np.max(c1)])

    def tile2coord(tid, dimx, shift=0.5):
        return tid%dimx+shift, tid//dimx+shift 

    # Heatmap
    plt.clf()
    sns.heatmap(c1, mask=c1==0, xticklabels=False, yticklabels=False, cmap=cmap, annot=annot, vmin=vmin, vmax=vmax, annot_kws=annot_kws)

    edges_edmonds = [(t_edmonds.vs['tile_id'][e.tuple[0]],t_edmonds.vs['tile_id'][e.tuple[1]]) for e in t_edmonds.es]
    edges_mst     = [(t_mst.vs['tile_id'][e.tuple[0]],t_mst.vs['tile_id'][e.tuple[1]]) for e in t_mst.es]
    edges_1       = set(edges_edmonds)-set(edges_mst)
    edges_2       = set(edges_mst)-set(edges_edmonds)
    edges_3       = set(edges_edmonds).intersection(set(edges_mst))
    # print(sum([len(edges_1), len(edges_2), len(edges_3)]))
    for edges,color,lab in zip([edges_1,edges_2,edges_3],['red','blue','purple'],['edmonds_only','mst_only','both']):
        _l = True
        for e in edges:
            source,target = e
            x, y = tile2coord(source,dimx)
            dx, dy = tile2coord(target,dimx)
            dx = dx-x
            dy = dy-y
            if _l: 
                plt.arrow(x, y, dx, dy, color=color, head_width=.1, alpha=.5, label=lab)
                _l = False
            else:
                plt.arrow(x, y, dx, dy, color=color, head_width=.1, alpha=.5)

    plt.xlim(0, dimx)
    plt.ylim(0, dimy)
    plt.legend()
    plt.savefig(plots_path+'tree_edmonds_mst_compare.png', bbox_inches='tight')
    print(f'tree comparison plotted at {plots_path+"tree_edmonds_mst_compare.png"}')


    # In[ ]:





    # In[39]:


    def get_tile_graph(G):
        tile_graph = G.copy()
        tile_graph.contract_vertices(tile_graph.vs['tile_id'], combine_attrs={'tile_id':lambda x:x[0] if x else None, 'fitness':lambda x:np.mean(x) if x else None, 'type':lambda x:np.mean(x) if x else None})
        tile_graph.es['weight'] = 1
        tile_graph.simplify(combine_edges={'weight':'sum', 'distance':'first'})
        tile_graph = tile_graph.components('WEAK').giant()
        tile_graph.to_directed()
        for v in tile_graph.vs:
            tot_w = sum(e['weight'] for e in v.out_edges())
            for e in v.out_edges():
                e['weight'] = e['weight']/tot_w
        tile_graph.es['weight'] = [np.sqrt(1-w) for w in tile_graph.es['weight']]
        print('tile graph generated')
        print(f'tile graph has {tile_graph.vcount()} vertices and {tile_graph.ecount()} edges')
        print(f'min weight is {min(tile_graph.es["weight"])}, max weight is {max(tile_graph.es["weight"])}')
        if tile_graph.is_connected('STRONG'):
            s = 'strongly'
        elif tile_graph.is_connected('WEAK'):
            s = 'weakly'
        else:
            s = 'not'
        print(f'the graph is {s} connected') 
        return tile_graph


    # In[40]:


    tile_graph = get_tile_graph(G)
    t_tile_mst = compute_invasion_tree(tile_graph, use_edmonds=False)
    t_tile_edmonds = compute_invasion_tree(tile_graph, source=source_tile)


    # In[ ]:





    # In[41]:


    # Parameters
    ntiles    = {'firenze':(15, 12), 'sabaudia':(16, 20), 'viterbo':(52, 64), 'vo':(6, 6)}
    dimx      = ntiles[city][0]
    dimy      = ntiles[city][1]

    pop_array = np.zeros(dimx*dimy)
    pop_dict = Counter(G.vs['tile_id'])
    for k,v in pop_dict.items():
        pop_array[k] = v
    c1 = pop_array.reshape(dimy,dimx)

    # Heatmap Parameters
    cmap='viridis'#"YlGn" #'hot_r'
    annot = False
    annot_kws={"fontsize":14}
    vmin = np.min([np.min(c1), np.min(c1)])
    vmax = np.max([np.max(c1), np.max(c1)])

    def tile2coord(tid, dimx, shift=0.5):
        return tid%dimx+shift, tid//dimx+shift 

    # Heatmap
    plt.clf()
    sns.heatmap(c1, mask=c1==0, xticklabels=False, yticklabels=False, cmap=cmap, annot=annot, vmin=vmin, vmax=vmax, annot_kws=annot_kws)

    edges_edmonds = [(t_tile_edmonds.vs['tile_id'][e.tuple[0]],t_tile_edmonds.vs['tile_id'][e.tuple[1]]) for e in t_tile_edmonds.es]
    edges_mst     = [(t_tile_mst.vs['tile_id'][e.tuple[0]],t_tile_mst.vs['tile_id'][e.tuple[1]]) for e in t_tile_mst.es]
    edges_1       = set(edges_edmonds)-set(edges_mst)
    edges_2       = set(edges_mst)-set(edges_edmonds)
    edges_3       = set(edges_edmonds).intersection(set(edges_mst))
    # print(sum([len(edges_1), len(edges_2), len(edges_3)]))
    for edges,color,lab in zip([edges_1,edges_2,edges_3],['red','blue','purple'],['edmonds_only','mst_only','both']):
        _l = True
        for e in edges:
            source,target = e
            x, y = tile2coord(source,dimx)
            dx, dy = tile2coord(target,dimx)
            dx = dx-x
            dy = dy-y
            if _l: 
                plt.arrow(x, y, dx, dy, color=color, head_width=.1, alpha=.5, label=lab)
                _l = False
            else:
                plt.arrow(x, y, dx, dy, color=color, head_width=.1, alpha=.5)

    plt.xlim(0, dimx)
    plt.ylim(0, dimy)
    plt.legend()
    plt.savefig(plots_path+'tree_tile_edmonds_mst_compare.png', bbox_inches='tight')
    print(f'tree comparison plotted at {plots_path+"tree_tile_edmonds_mst_compare.png"}')


    # In[ ]:





    # In[28]:


    def select_min_inweight(aggregate_graph):
        to_delete = []
        for v in aggregate_graph.vs:
            edges = v.in_edges()
            min_weight = min(e['weight'] for e in edges)
            to_delete.extend([e for e in edges if e['weight']>min_weight])
        h = aggregate_graph.copy()
        h.delete_edges(to_delete)
        return h
        


    # In[29]:


    min_weight_graph = select_min_inweight(aggregate_graph)


    # In[ ]:


    # Parameters
    ntiles    = {'firenze':(15, 12), 'sabaudia':(16, 20), 'viterbo':(52, 64), 'vo':(6, 6)}
    dimx      = ntiles[city][0]
    dimy      = ntiles[city][1]

    pop_array = np.zeros(dimx*dimy)
    pop_dict = Counter(G.vs['tile_id'])
    for k,v in pop_dict.items():
        pop_array[k] = v
    c1 = pop_array.reshape(dimy,dimx)

    # Heatmap Parameters
    cmap='viridis'#"YlGn" #'hot_r'
    annot = False
    annot_kws={"fontsize":14}
    vmin = np.min([np.min(c1), np.min(c1)])
    vmax = np.max([np.max(c1), np.max(c1)])

    def tile2coord(tid, dimx, shift=0.5):
        return tid%dimx+shift, tid//dimx+shift 

    # Heatmap
    plt.clf()
    sns.heatmap(c1, mask=c1==0, xticklabels=False, yticklabels=False, cmap=cmap, annot=annot, vmin=vmin, vmax=vmax, annot_kws=annot_kws)

    edges = [(min_weight_graph.vs['tile_id'][e.tuple[0]],min_weight_graph.vs['tile_id'][e.tuple[1]]) for e in min_weight_graph.es]
    for e in edges:
        source,target = e
        x, y = tile2coord(source,dimx)
        dx, dy = tile2coord(target,dimx)
        dx = dx-x
        dy = dy-y
        plt.arrow(x, y, dx, dy, color='orange', head_width=.1, alpha=.5, label='min_weight_graph')

    plt.xlim(0, dimx)
    plt.ylim(0, dimy)
    plt.legend()
    plt.savefig(plots_path+'min_weight_graph.png', bbox_inches='tight')
    print(f'min weight graph and tree comparison plotted at {plots_path+"min_weight_graph.png"}')


    # In[27]:


    def jaccard_similarity(a,b):
        a = set(a)
        b = set(b)
        return len(a.intersection(b))/len(a.union(b))


    # In[46]:


    trees = {'edmonds':t_edmonds, 'mst':t_mst, 'min_w':min_weight_graph, 'tile_ed':t_tile_edmonds, 'tile_mst':t_tile_mst}
    s = {}
    for (k1,t1),(k2,t2) in itertools.combinations(trees.items(), 2):
        s[f'{k1} vs. {k2}'] = jaccard_similarity([e.tuple for e in t1.es],[e.tuple for e in t2.es])


    # In[64]:

    plt.clf()
    for i,(k,v) in enumerate(s.items()):
        plt.scatter(i, v, label=k)
    plt.legend()
    # plt.stem(s.values())
    plt.xticks(range(len(s)), s.keys(), rotation=90) #my_range, ordered_df['group'])
    plt.savefig(plots_path+'jaccard.png', bbox_inches='tight')
    print(f'jaccard similarity scatter plotted at {plots_path+"jaccard.png"}')


        # In[ ]:
    # except:
    #     pass
recap_data = pd.DataFrame(recap_data)

recap_data.to_pickle('epidemic_threshold_data.pickle')

# print(recap_data.head())
plt.clf()
# sns.catplot(x="configuration", y="threshold value", hue="threshold type", kind="point", data=recap_data, palette={r'$\beta_c$':'b',r'$\beta_c^*$':'y',r'$\beta_c^{\Delta}$':'g'})
# sns.catplot(x="configuration", y="threshold value", hue="threshold type", s=0, kind="point", data=recap_data, palette={r'$\beta_c^{\mathrm{HMF}}$':'y',r'$\beta_c^{\Delta}$':'g'}, linestyles=["-", "--"])
# sns.catplot(x="configuration", y="threshold value", data=recap_data, jitter=False, s=10, order=['HM','SN','HN','AN','DN','ADN'], palette=sns.color_palette()) #, palette={r'$\beta_c^{\mathrm{HMF}}$':'y',r'$\beta_c^{\Delta}$':'g'})
# sns.catplot(x="configuration", y="threshold value", hue="threshold type", kind="point", data=recap_data, palette={r'$\beta_c^{\mathrm{HMF}}$':'y',r'$\beta_c^{\Delta}$':'g'})
            # markers=["^", "o"], linestyles=["-", "--"],
# plt.xticks(rotation=45)
plt.figure(figsize=(17,6))
fg = sns.FacetGrid(data=recap_data)
fg.map_dataframe(sns.pointplot, x="configuration", y="threshold value", hue="threshold type", markers='', palette={r'$\beta_c^{\mathrm{HMF}}$':'k',r'$\beta_c^{\Delta}$':'gray'}, linestyles=['-','--'])
colors = sns.color_palette()[:6]
fg.map_dataframe(sns.stripplot, x="configuration", y="threshold value", jitter=False, s=10, order=['HM','SN','HN','AN','DN','ADN']) #, color=colors) #, palette={r'$\beta_c^{\mathrm{HMF}}$':'y',r'$\beta_c^{\Delta}$':'g'})
plots_path = plots_path.replace('combo6/','')
if not os.path.exists(plots_path):
    os.makedirs(plots_path)
plt.savefig(plots_path+'threshold_comparison.png', bbox_inches='tight')
print(f'threshold comparison plotted at {plots_path+"threshold_comparison.png"}')

