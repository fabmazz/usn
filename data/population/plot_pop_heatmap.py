import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
from pathlib import Path

def make_heatmap_name(name):
    folder = Path(name)
    terr = pd.read_pickle(folder / "territory_projected.pickle")
    ### recreate bbox
    bbox=np.array(terr["bbox"])
    ntiles = terr["ntiles"]

    popgen = pd.read_csv(folder / f"population_{name}.csv")
    popgen["tile_id"] = popgen["tile_id"].astype(int)

    npeople=popgen.groupby("tile_id").agg("count")

    npeople = npeople.reset_index()
    ##the x size is the major index
    BIG_TILE = ntiles[0]
    little_i=npeople.tile_id % BIG_TILE
    big_i = (npeople.tile_id / BIG_TILE).astype(int)

    npeople["x"] = little_i
    npeople["y"] = big_i

    print(f"Have {len(npeople)}/{np.prod(ntiles)} tiles with population")
    print(f"Total population: {npeople['type'].sum()}")

    f,ax = plt.subplots()
    nnp=npeople.pivot(columns="x", index="y",values="type")
    ## reverse order of rows (y axis) because the top has higher values
    sns.heatmap(nnp[::-1], cmap="viridis", ax=ax)
    ax.axis("equal")

    return npeople.rename(columns={"type":"count"}), f, ax

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Script to plot the heatmap of the individuals in the tiles")
    parser.add_argument("-n", "--name", dest="name", help="population folder and csv name", required=True)

    args = parser.parse_args()
    namef = args.name

    npeople_df, fig, ax = make_heatmap_name(args.name)

    fold_out = Path(namef)
    fout=fold_out/"heatmap_pop.png"
    fig.savefig(fout,bbox_inches="tight",dpi=140)
    print(f"Saved population heatmap to {fout}")

    fout = fold_out/"pop_count_tile.csv"
    npeople_df.to_csv(fout, index=False)
    print(f"Saved population count by tile to {fout}")
