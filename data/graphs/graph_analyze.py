#!/usr/bin/env python
# coding: utf-8

import argparse
import polars as pl
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

import seaborn as sns
import igraph
import pandas as pd

# Function to parse command-line arguments
def parse_arguments():
    parser = argparse.ArgumentParser(description="Process folder path")
    parser.add_argument("folder", type=str, help="Path to the folder containing data files")
    return parser.parse_args()

# Function to process the folder
def process_folder(folder):
    Fold = Path(folder)

    tile_data = pl.read_csv(Fold/"pop_count_tile.csv")
    nodes_info = pl.read_parquet(Fold/"nodes_info.pq")
    edges_data = pl.read_parquet(Fold/"edges_data.pq")

    no_hh_c = nodes_info.lazy().filter(pl.col("household")<0).group_by("tile_id").len()\
        .join(tile_data.lazy(),on="tile_id")\
        .with_columns((pl.col("len")/pl.col("count")).alias("no_hh_density")).rename({"len":"no_hh"})\
        .collect()

    u = nodes_info.filter(pl.col("household")>=0)
    r=u.lazy().group_by("tile_id").agg(pl.col("household").unique().count().alias("hh_count"))
    tile_household =r.collect().join(tile_data,on="tile_id")


    print("Finding household count...")
    tile_hh_pd = tile_household.to_pandas()

    f,ax = plt.subplots(figsize=(7,7))
    nnp=tile_hh_pd.pivot(columns="x", index="y",values="hh_count")
    sns.heatmap(nnp[::-1], cmap="plasma", ax=ax)
    ax.axis("equal")
    f.savefig(Fold/"hh_count.png",bbox_inches="tight",dpi=130)

    print("Finding household density...")

    tile_hh_pd["hh_dens"] = tile_hh_pd["hh_count"] / tile_hh_pd["count"]

    no_hh_pd = no_hh_c.to_pandas()

    f,ax = plt.subplots(figsize=(8,7))
    nnp=no_hh_pd[no_hh_pd["count"]>1].pivot(columns="x", index="y",values="no_hh_density")
    sns.heatmap(nnp[::-1], cmap="viridis", ax=ax)
    ax.axis("equal")
    ax.set_title(f"Fraction of nodes with no household, min = {no_hh_c['no_hh_density'].min():3.2%}, >=2 people")
    f.savefig(Fold/"no_hh_density.png",bbox_inches="tight",dpi=130)

    sel=nodes_info[["tile_id","i"]]

    dat=edges_data.join(sel,how="left",left_on="j",right_on="i", suffix="_j").rename({"tile_id":"tid_j"})
    dat=dat.join(sel,how="left",left_on="i",right_on="i").rename({"tile_id":"tid_i"})

    edges_fre = dat.filter(pl.col("layer")=="friendship")
    edges_hh = dat.filter(pl.col("layer")=="household")

    count_hh=edges_hh.lazy().group_by(["tid_i","tid_j"]).len().collect()
    count_hh.join(tile_data, left_on="tid_i",right_on="tile_id")

    count_fre =edges_fre.lazy().group_by(["tid_i","tid_j"]).len().collect()

    count_fre = count_fre.sort(["tid_j","tid_i"],)

    
    print("Make graph of tiles from frendship and compute centrality")
    nodes_gn = np.unique(count_fre[["tid_i","tid_j"]])
    idsnew = dict(zip(nodes_gn,range(len(nodes_gn))))

    newedges = count_fre.with_columns(pl.col("tid_i").replace(idsnew), pl.col("tid_j").replace(idsnew))

    newedges.to_numpy()

    len(nodes_gn)

    gn = igraph.Graph(len(nodes_gn))

    ee=np.sort(newedges[["tid_i","tid_j"]].to_numpy())

    gn.add_edges(ee.tolist(), attributes=dict(c=newedges["len"].to_numpy().tolist()) )    

    betw = gn.betweenness(directed=False,weights="c")
    betwennes = np.array(betw)

    tile_data_pd = tile_data.to_pandas()

    centr_ser = pl.DataFrame({"betweenness":betwennes,"tile_id":nodes_gn})

    tile_betwn = tile_data.join(centr_ser,how="left",on="tile_id")

    tile_betwn.write_csv(Fold/"tile_betwenness.csv")

    tile_data_pd = tile_betwn.to_pandas()

    tile_data_pd["centrality"] = tile_data_pd.betweenness /np.nanmax(tile_data_pd.betweenness)
    tile_data_pd

    f,ax = plt.subplots(figsize=(8,8))
    nnp=tile_data_pd.pivot(columns="x", index="y",values="betweenness")
    sns.heatmap(nnp[::-1], cmap="viridis", ax=ax)
    ax.axis("equal")
    f.savefig(Fold/"betweenness_cent.png",bbox_inches="tight",dpi=130)

    print("Saved plots, saving graph")
    gn.save(Fold/"friends_tile_g.graphmlz")
# Main function
def main():
    args = parse_arguments()
    process_folder(args.folder)

if __name__ == "__main__":
    main()