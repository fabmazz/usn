import sys
import pyproj
from pyproj import CRS
from pyproj import Transformer
import numpy as np
import configparser as cp
from   geopy.distance import distance #The default is the WGS-84
import matplotlib.pyplot as plt
import matplotlib

plt.rcParams["figure.figsize"] = (8,6)
plt.style.use('ggplot')


###############################################################################
#
#                           VARIOUS FUNCTIONS
#
###############################################################################

#Get info about projection
def get_CRS_info(csr_id):
    crs_utm = CRS.from_user_input(4326)
    return crs_utm

# Dummy function to check the dimensions of the bounding 
# box computed by euclidean distance
def test_bbox_dimensions(territory):
    sw, nw, ne, se = territory['bbox']
    length_we = np.sqrt((sw[0]-se[0])*(sw[0]-se[0]) + (sw[1] - se[1])*(sw[1] - se[1]))
    length_ns = np.sqrt((sw[0]-nw[0])*(sw[0]-nw[0]) + (sw[1] - nw[1])*(sw[1] - nw[1]))
    print(length_ns, length_we)
    return

# From wgs84 to wgs84_32n
# coord = (lat, lon)
def project_coord(coord, inCRS, outCRS):
    inproj     = pyproj.CRS(inCRS)
    outproj    = pyproj.CRS(outCRS)
    
    transf = Transformer.from_crs(inproj, outproj)
    a = transf.transform(coord[1], coord[0])
    return a

# interleave two numpy arrays
def interleave(a, b):
    c = np.empty((a.size + b.size,), dtype=a.dtype)
    c[0::2] = a
    c[1::2] = b
    return c


###############################################################################
#
#                       READ CONFIG, BUILD DATA STRUCT
#
###############################################################################


# Read the territory info from the config file and compute the projected version
def build_projected_territory(config_file, inCRS, outCRS):
    """ Read the config file, build a dictionary that stores vaious information about
        the territory.

        Args: 
            config_file (:obj:`str`): path to the config file.

        Returns: 
            (:obj:`dict`): a dictionary containing territory information.

            ::

                territory   = {
                    'origin':  (ox, oy),   # bottm left
                    'center':  center,     # gird center
                    'bbox':    bbox,       # bbox = [sw, nw, ne, se]
                    'borders': borders,    # borders = (max_x, min_x, max_y, min_y)
                    'tile':    tile,       # tuple with dimensions of the tile in meters
                    'ntiles':  ntiles      # tuple with the number of tiles in the grid 
                }


    """
    config = cp.ConfigParser()
    config.read(config_file)
    # coordinates (lat,lon) of the bottom left corner of the box in WGS84
    city   = eval(config.get('CITY','city')) 
    origin = eval(config.get('TERRITORY','origin'))[city] 
    # size in km of the h,w dimensions of each tile in the grid
    tile = eval(config.get('TERRITORY','tile'))
    # number of tiles in the grid in the h, w directions 
    ntiles = eval(config.get('TERRITORY','ntiles'))[city]

    # compute total height and width of the grid == bounding box == bbox
    xdim = tile[0]*ntiles[0]
    ydim = tile[1]*ntiles[1]
    
    (ox, oy) = utm_origin = project_coord((origin[1], origin[0]), inCRS, outCRS)
    
    sw = (ox, oy) 
    nw = (ox, oy + ydim)
    ne = (ox + xdim, oy + ydim)
    se = (ox + xdim, oy)
    bbox = [sw, nw, ne, se]
    
    # Compute territory borders and center
    min_x = ox
    min_y = oy
    max_x = ox + xdim
    max_y = oy + ydim
    
    borders = (max_x, min_x, max_y, min_y)
    
    center  = (ox + xdim/2.0, oy + ydim/2.0)
    
    # return a dictionary
    territory   = {
        'origin':  (ox, oy), 
        'center':  center,
        'bbox':    bbox, 
        'borders': borders,   
        'tile':    tile,
        'ntiles':  ntiles
    }
    return territory

# ALL CORDINATES ARE ASSUMED TO BE IN THE FORM (LAT, LON)
# Latitude grow from bottom to up (from the equator to the north pole so it's an y coordinate)
# Longitude grow from left to right (from greenwitch anticlock-wise watching from the north, thus an x coordinate)
def build_territory(config_file):
    """ Reads initial paramters from the config file a return a data structure
        with territory informations.

        Args:

        Returns:
    """
    config = cp.ConfigParser()
    config.read(config_file)
    # coordinates (lat,lon) of the bottom left corner of the box in WGS84
    city   = eval(config.get('CITY','city')) 
    origin = eval(config.get('TERRITORY','origin'))[city] 
    # size in km of the h,w dimensions of each tile in the grid
    tile = eval(config.get('TERRITORY','tile'))
    # number of tiles in the grid in the h, w directions 
    ntiles = eval(config.get('TERRITORY','ntiles'))[city]

    # Tile is stored in meters but i want it in km
    tile = (tile[0]/1000, tile[1]/1000)

    # compute total height and width of the grid == bounding box == bbox
    lenght_w = tile[0]*ntiles[0]
    lenght_h = tile[1]*ntiles[1]

    # COMPUTE THE BBOX VERTICES
    # To explicitly compute all the bbox points, starting from the origin
    # we move lenght_w along the WE direction and find the
    # coordinates of the bottom rigthmost point of the bbox,
    # using the destination method of geopy.distance.
    # Then we move length_h along the NS direction and find
    # the coordinates of the upper leftmost vertex of the bbox.
    # Finally, as above, for the upper rightmost vertex.
    #
    # compute the bottom rightmost vertex of the bbox (SE)
    d_w     = distance(kilometers = lenght_w)
    end     = d_w.destination(point=origin, bearing=90) # bearing 90 == direction W-E
    bbox_se = (end[0], end[1])
    # compute the upper leftmost vertex of the bbox (NW)
    d_h     = distance(kilometers = lenght_h)
    end     = d_h.destination(point=origin, bearing=0)  # bearing 0 == direction N-S
    bbox_nw = (end[0], end[1])
    # compute the upper rightmost vertex of the bbox (NE)
    end     = d_w.destination(point=bbox_nw, bearing=90)  # bearing 90 == direction W-E
    bbox_ne = (end[0], end[1])
    # The bbox rectangle as a list of vertices!
    bbox_sw = origin
    bbox    = [bbox_sw, bbox_nw, bbox_ne, bbox_se]

    # Compute territory borders and center
    min_lat = origin[0]
    min_lon = origin[1]
    max_lat = bbox_ne[0]
    max_lon = bbox_ne[1]
    borders = (max_lat, min_lat, max_lon, min_lon)
    center  = ((max_lat + min_lat)/2.0, (max_lon + min_lon)/2.0)

    # return a dictionary
    territory   = {
        'origin':  origin,
        'center':  center,
        'bbox':    bbox,
        'borders': borders,
        'tile':    tile,
        'ntiles':  ntiles
    }
    return territory


    

# Just a wrapper to read a specific filed from config.ini
def read_from_config(config_file, section, name):
    """ Wrapper around configparser to read a specific value
        from the .ini configuration file.
    """
    config = cp.ConfigParser()
    config.read(config_file)
    var = eval(config.get(section, name)) 
    return var



def plot_numpy_matrix(data, outfile, change_lim=True, cmap='viridis'):

    fig = plt.figure()

    xdim = data.shape[1]
    ydim = data.shape[0]
    
    my_cmap = matplotlib.cm.get_cmap(cmap)
    my_cmap.set_under('w')

    plt.imshow(data, interpolation='none', cmap=cmap)
    if change_lim:
        plt.xlim(0, xdim)
        plt.ylim(0, ydim)

    plt.colorbar()
    outfile = outfile + '.pdf'
    plt.savefig(outfile)
    return

