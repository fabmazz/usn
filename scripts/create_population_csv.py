import sys
import argparse
from pathlib import Path
import numpy as np
from   numpy.random import default_rng
import pandas as pd
import geopandas as gpd
from shapely.geometry import shape, box, Point, Polygon
import shapely
import pickle, json
import  matplotlib.pylab as plt
import contextily as ctx

from pathlib import Path

plt.rcParams["figure.figsize"] = (8,8)
plt.style.use('ggplot')

from lib.libfunc import read_from_config, plot_geojson, plot_numpy_matrix, build_projected_territory, build_grid_dictionary
from config import RANDOM_SEED, AGE_GROUPS, GROUP_NAMES, DEFAULT_PATH, TERRITORY_PICKLE_FILE_NOT_PROJECTED, TERRITORY_PICKLE_FILE_PROJECTED

WORLDPOP_TILE_DIM = 100
CENTER_COLUMN = 'centers'
SQUARE_COLUMN = 'squares'
POP_CSV_FILE  = 'population'
POP_DENSITY_PLOT_FILE = 'population_density'
SIMPLIFIED_SHAPE_PLOT = 'simplified_shape_plot'
GEODATA_CETERS_PLOT_FILE = 'pop_geodata_plot.pdf'


def set_coords(N, tile_id, ntx, tx, ty, origin, seed=None):
    """
        Given the a tile (by its id) generate N random points inside the tile.
        ntx: number of tile in direction x
        tx:  lenght of tile in direction x
        ty:  lenght of tile in direction y
        origin: origin of the bbox
        tile_id: linear id of the tile in the tile grid: i + j*ntx
    """

    rng = default_rng(seed)
    tx2 = tx/2.0
    ty2 = ty/2.0
    # gid = i + j*ntx
    j = tile_id//ntx
    i = tile_id - ntx*j
    deltax = i*tx + tx2
    deltay = j*ty + ty2
    x = origin[0] + deltax
    y = origin[1] + deltay

    #print(f'DEBUG: TX2={tx2}, TY2={ty2}')

    xnoise  = rng.uniform(-tx2, tx2, N)
    ynoise  = rng.uniform(-ty2, ty2, N)

    coords = list(zip(x + xnoise, y + ynoise))

    return coords

def extract_pop_points(df, group_names, territory, sample_size=0, seed=None):

    # Get input var from territory
    origin   = territory['origin']
    ntx, nty = territory['ntiles']
    tx , ty  = territory['tile']
    tx2 = tx/2.0
    ty2 = ty/2.0
    
    tile_id = []
    fitness = []
    coord   = []
    types   = [] 

    lost_pop = 0.0
    added_pop = 0.0
    tot_pop = 0.0
    exp_pop = 0.0
    for i in df['tile_id']:
        for g in group_names: 
            #Ng = int(df[g][i])
            Ng = round(df[g][i])

            if not isinstance(Ng, int):
            #    print(f'WARNING round(Ng) returned a float:{Ng} taking the int() {int(Ng)}')
                Ng = int(Ng)

            if Ng < df[g][i]:
                lost_pop += df[g][i] - Ng
            else:
                added_pop += Ng - df[g][i] 
            tot_pop += Ng
            exp_pop += df[g][i]
            if Ng > 0:
                # Generate x,y coordinates for each of the Ng nodes in tile i
                # by adding small noise to the tile center
                coords = set_coords(Ng, i, ntx, tx, ty, origin)
                tile = [i]*Ng
                # The pop type array uses indices not names
                idx = group_names.index(g)
                group = [idx]*Ng
                tile_id.append(tile)
                coord.append(coords)
                types.append(group)

    tile_id = np.array([item for sublist in tile_id for item in sublist])
    coord   = np.array([item for sublist in coord for item in sublist])
    types   = np.array([item for sublist in types for item in sublist])

    # Use pandas to dump the pickle 
    pop_df = pd.DataFrame()
    pop_df['tile_id']        = tile_id
    pop_df['x'], pop_df['y'] = zip(*coord)
    pop_df['type']           = types

    
    if sample_size > 0:
        print("read_population: sampling {} rows from population".format(sample_size))
        pop_df = pop_df.sample(sample_size)

    diff_pop = added_pop-lost_pop
    print(f"WARNING: Expected: {exp_pop} - Total: {tot_pop} - Lost: {lost_pop} - Added: {added_pop} - Difference: {diff_pop} - {(diff_pop/exp_pop)*100:.2f}%")
    return pop_df

def merge_select_pop_to_tiles(pop_df, geoj, territory, group_names):
    
    # Select centers inside the input shape
    if geoj is not None:
        # Multipolygon to polygon to use the method within
        geoj_geometry = geoj.unary_union
        # geoj['geometry'] = geoj_geometry
        if debug:
            plot_geojson(geoj, SIMPLIFIED_SHAPE_PLOT)

        # Find the point inside the shape
        points_inside = [False]*len(pop_df['centers'])
        for i,c in enumerate(pop_df['centers']):
            if c.within(geoj_geometry):
                points_inside[i] = True

        # Set to 0.0 all the pop outside the shape
        points_inside = np.array(points_inside, dtype=bool)
        for group in group_names:       
            if group in pop_df.columns:
                pop_df.loc[~points_inside, group] = 0

    # Assign tile to data centers
    tx, ty   = territory['tile']
    ntx, nty = territory['ntiles']
    x0, y0   = territory['origin']
    
    xarray = [c.coords.xy[0][0] for c in pop_df['centers']]
    yarray = [c.coords.xy[1][0] for c in pop_df['centers']]
    
    # DEBUG
    # Check that data points x, y are above the origin 
    # count = 0
    # for x,y in zip(xarray, yarray):
    #     if x < x0 or y < y0:
    #         print("{},{} < {},{}".format(x, y, x0, y0))
    #         count += 1
    # print("TOT WRONG:", count)

    # Set the territory tile for each data center
    xarray = np.array(xarray)
    yarray = np.array(yarray)
    xtile  = (xarray - x0)//tx
    ytile  = (yarray - y0)//ty
    xytile = xtile + ntx*ytile
    pop_df['tile_id'] = xytile

    # Sum population inside the same tile_id and return the dataframe
    # with only the group values and the tile
    pop=pop_df[group_names + ['tile_id']].groupby('tile_id', as_index=False).sum()
    
    return pop


def create_pop_groups(input_pop_df, age_groups, group_names):
    
    print("reading population dataframe from: {}".format(input_pop_df))
    df = gpd.read_parquet(input_pop_df)

    columns = df.columns
    #print(columns)

    col_age_groups_m={}
    col_age_groups_f={}
    #print(col_age_groups_m)

    current_age_group_names_m = set()
    current_age_group_names_f = set()
    for c in columns:
        if 'm' in c:
            n = int(c.split('_')[1])
            myidx = np.searchsorted(age_groups, n)
            mygroup = group_names[myidx]
            #print(c, n, myidx, mygroup)
            col_age_groups_m.setdefault(mygroup, [])
            col_age_groups_m[mygroup].append(c)
            current_age_group_names_m.add(mygroup)
        elif 'f' in c:
            n = int(c.split('_')[1])
            myidx = np.searchsorted(age_groups, n)
            mygroup = group_names[myidx]
            col_age_groups_f.setdefault(mygroup, [])
            col_age_groups_f[mygroup].append(c)
            current_age_group_names_f.add(mygroup)


    # Create new df with male groups
    current_age_group_names_m = list(current_age_group_names_m)
    male_df = pd.DataFrame(columns=current_age_group_names_m) 
    for myg in current_age_group_names_m:
        for i, cols in enumerate(col_age_groups_m[myg]):
            if i == 0:
                male_df[myg] = df[cols] 
            else: 
                male_df[myg] += df[cols]

    # Create new df with female groups
    current_age_group_names_f = list(current_age_group_names_f)
    female_df = pd.DataFrame(columns=current_age_group_names_f) 
    for myg in current_age_group_names_f:
        for i, cols in enumerate(col_age_groups_f[myg]):
            if i == 0:
                female_df[myg] = df[cols] 
            else: 
                female_df[myg] += df[cols]

    print("male per age group:\n", male_df.sum())
    print("female per age group:\n", female_df.sum())

    # Sum up the male and female df
    tot_df = male_df.add(female_df)
    # Add square centers and square list
    tot_df[CENTER_COLUMN] = df[CENTER_COLUMN]
    tot_df[SQUARE_COLUMN] = df[SQUARE_COLUMN]

    return tot_df



if __name__ == "__main__":

    parser=argparse.ArgumentParser(description="")
    #parser.add_argument('-c', '--config', dest='config', type=str, required=True, help='configuration file in ini format')
    parser.add_argument('-p', '--input_population', dest='input_population', type=str, required=True, help='input pickle with population dataframe')
    parser.add_argument('-g', '--input_geojson', dest='input_geojson', type=str, help='input geojson with the shape of the area')
    parser.add_argument('-d', '--input_tiff_info', dest='input_tiff_info', type=str, help='input tiff_info with rows and cols of the original tiff data, this option is used only to plot population density')
    parser.add_argument('--debug', dest='debug', type=int, help='activate debug levels')
    parser.add_argument('--city', dest='city', type=str, required=True, help='specify the city')
    parser.add_argument("-w", "--work_dir" , help="Working directory", default="")

    args=parser.parse_args()

    input_population = args.input_population
    #config_file =args.config
    city=args.city
    workdir = Path(args.work_dir)
    
    if args.input_geojson:
        input_geojson = args.input_geojson
        geoj = gpd.read_file(input_geojson)
        #print(geoj)
    else:
        geoj = None

    if args.input_tiff_info:
        input_tiff_info = args.input_tiff_info
    else:
        input_tiff_info = None

    if args.debug:
        debug = args.debug
    else:
        debug = 0
    
    sample_size = 0
    #seed        = read_from_config(config_file, 'RANDOM', 'seed')
    #group_names = read_from_config(config_file, 'POPULATION', 'group_names')
    #age_groups  = read_from_config(config_file, 'POPULATION', 'age_groups')
    #inCRS       = read_from_config(config_file, 'PROJECTION', 'input_CRS')
    #outCRS      = read_from_config(config_file, 'PROJECTION', 'output_CRS')
    # territory_file_pj     = read_from_config(config_file, 'TERRITORY', 'territory_file_pj')
    # territory_file_not_pj = read_from_config(config_file, 'TERRITORY', 'territory_file_not_pj')
    seed = RANDOM_SEED
    group_names = GROUP_NAMES
    age_groups = AGE_GROUPS
    territory_file_not_pj = TERRITORY_PICKLE_FILE_NOT_PROJECTED
    territory_file_pj = Path(TERRITORY_PICKLE_FILE_PROJECTED)

    with open(workdir/territory_file_not_pj, 'rb') as fin:
        territory = pickle.load(fin)
    
    with open(workdir/territory_file_pj, 'rb') as fin:
        if territory_file_pj.suffix == ".json":
            territory_pj = json.load(fin)
        else:
            territory_pj = pickle.load(fin)
        

    inCRS  = territory['projection']
    outCRS = territory_pj['projection']

    pop_df = create_pop_groups(input_population, age_groups, group_names)
    for g in group_names:
        if g not in pop_df.columns:
            pop_df[g] = [0]*pop_df.shape[0]
    
    # dump density plot (the number of rows and clos are needed)
    if input_tiff_info:
        tiff_info = pd.read_pickle(workdir/input_tiff_info)
        rows, cols = tiff_info['dim']
        tot_df = pd.DataFrame()
        tot_df['n'] = pop_df[group_names[0]]
        for g in group_names:
            tot_df['n'] = tot_df['n'] + pop_df[g]

        pop_density = tot_df['n'].to_numpy()
        pop_density = pop_density.reshape(cols, rows)
        pop_density_f = workdir / POP_DENSITY_PLOT_FILE
        plot_numpy_matrix(pop_density, pop_density_f, change_lim=False, cmap='magma')
        print("population density plot saved to {}".format(pop_density_f))

    #print("Total number per age group:\n", pop_df.sum())
    if debug:
        centers = pop_df['centers'].copy()
        squares = pop_df['squares'].copy()

    # Merge pop to tiles and filter only data inside the geojson
    # Tile dimension must be > original tiff tiles
    pop_df = merge_select_pop_to_tiles(pop_df, geoj, territory, group_names)
    pop_sum = pop_df.drop('tile_id', axis=1).sum()
    tot = pop_sum.sum()
    print("tot = ", tot)
    print(pop_sum)

    if debug:    
        pop_df_groups = pop_df.copy()
    ## Save df with centers
    #PARQ_POPFILE="pop_tile_unprojected.parquet"
    #pop_df.to_parquet(PARQ_POPFILE)
    #print("Saved raw population to "+PARQ_POPFILE)

    # Unroll all the points, add coordinates, fittnes and type to each point
    # Extract points from tiles: create a row for each point 
    pop_df = extract_pop_points(pop_df, group_names, territory_pj, sample_size=sample_size, seed=None)
    
    # Write the population file
    #pop_csv_file = POP_CSV_FILE + '_' + city.lower() + '.csv'
    pop_csv_file = workdir / ( POP_CSV_FILE + '_' + city.lower().replace(" ","_").replace("'","") + '.csv')
    pop_df = pop_df.round(1)
    pop_df.to_csv(pop_csv_file, index=False)
    print("population data written to {} ".format(pop_csv_file))

    if debug:
        # bounding box
        maxx, minx, maxy, miny = territory['borders']
        bbox = box(minx, miny, maxx, maxy)
        b = gpd.GeoSeries(bbox, crs=inCRS)

        # tiff data centers
        # c = gpd.GeoSeries(centers, crs=inCRS)
        # tiff data squares
        # s = gpd.GeoSeries(squares, crs=inCRS)
    
        # convert to CTX MAP CRS
        b = b.to_crs(epsg=3857)
        g = geoj.to_crs(epsg=3857)
        # c = c.to_crs(epsg=3857)
        # s = s.to_crs(epsg=3857)

        # first plot the centers
        # ax = c.plot(alpha=0.5, edgecolor='b', marker='o', color='blue', markersize=0.1)
        ax = b.plot(alpha=0.2, edgecolor='g')
        # add bbox in green
        # b.plot(alpha=0.5, edgecolor='g', ax=ax)
        # add squares
        # s.plot(alpha=0.5, edgecolor='b', ax=ax)
        # add shape in red
        g.plot(alpha=0.2, edgecolor='r', ax=ax)
        
        # add population
        if sample_size == 0 or sample_size > 1000:
            sample_size = 1000
        pop_df = extract_pop_points(pop_df_groups, group_names, territory, sample_size=sample_size, seed=None)
        points = [Point(x, y) for x, y in zip(pop_df['x'], pop_df['y'])]
        p = gpd.GeoSeries(points, crs=inCRS)
        p = p.to_crs(epsg=3857)
        p.plot(alpha=0.5, edgecolor='black', marker='o', markersize=2, ax=ax)


        # grid = build_grid_dictionary (territory)
        # Plot GRID
        # polylist=[]
        # for k,v in grid.items():
        #    poly = v['vertices'] + [v['vertices'][0]]
        #    polylist.append(Polygon(poly))
            #xtile, ytile = zip(*poly)
            #plt.plot(xtile, ytile, 'r', linewidth=0.1, ax=ax)
            # ctx, cty = v['coord']
            #plt.plot(ctx, cty, 'b+', markersize=1, ax=ax)
        #poly = gpd.GeoSeries(polylist, crs=inCRS)
        #poly.plot(alpha=0.2, edgecolor='r', ax=ax)

        ctx.add_basemap(ax, zoom = 12)
        geodata_file = workdir / GEODATA_CETERS_PLOT_FILE
        plt.savefig(geodata_file, bbox_inches="tight")
        print("Bbox, shape and tiff data centers saved to {}".format(geodata_file))







