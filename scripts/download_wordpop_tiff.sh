
year="2020"
outdir="worldpopData"
mkdir ${outdir}

for sex in "m" "f"
do
    for ((n=0; n<100; n+=5))
    do
        url="ftp://ftp.worldpop.org.uk/GIS/AgeSex_structures/Global_2000_2020/2020/ITA/"
        tiffname="ita_${sex}_${n}_${year}.tif" 
        output=$tiffname
        echo "download: ${url}${tiffname}"
        curl -L -o $output -C - "${url}${tiffname}"
        mv ${output} ${outdir} 
    done
done

#Extra file for one-year-old actors

for sex in "m" "f"
do
    n=1
    url="ftp://ftp.worldpop.org.uk/GIS/AgeSex_structures/Global_2000_2020/2020/ITA/"
    tiffname="ita_${sex}_${n}_${year}.tif" 
    output=$tiffname
    echo "download: ${url}${tiffname}"
    curl -L -o $output -C - "${url}${tiffname}"
    mv ${output} ${outdir} 
done
