#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
     
#include "fn.h"
#include "fn.c"
#define MINARG 5
#define MAXPAIRS 1000000
#define MAXLINE 1024

int main (int argc, char *argv[]) {
       int status, i;
       int iter = 0, max_iter = 100;
       const gsl_root_fdfsolver_type *T;
       gsl_root_fdfsolver *s;
       char line[MAXLINE];
       double x0, x, c, precision;
       double *xi, *xj;
       unsigned int n;
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_pairs L initial_guess precision\n",argv[0]);
	  exit(1);
       }
       n=atoi(argv[1]);
       c=atof(argv[2]);
       x=atof(argv[3]);
       precision=atof(argv[4]);
       if(n>MAXPAIRS) {
	fprintf(stderr,"Too many pairs: max number is %d\n",MAXPAIRS);
	exit(1);
       }
       xi=(double *)malloc(sizeof(double)*n);
       xj=(double *)malloc(sizeof(double)*n);
       if(xi==NULL) {
	fprintf(stderr,"Could not get memory for %d items of xi\n",n);
	exit(1);
       }
       if(xj==NULL) {
	fprintf(stderr,"Could not get memory for %d items of xi\n",n);
	exit(1);
       }
       for(i=0; i<n; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf %lf",xi+i, xj+i);
       }
       gsl_function_fdf FDF;
       struct yf_params params = {xi, xj, n, c};
     
       FDF.f = &yf;
       FDF.df = &yf_deriv;
       FDF.fdf = &yf_fdf;
       FDF.params = &params;
     
       T = gsl_root_fdfsolver_newton;
       s = gsl_root_fdfsolver_alloc (T);
       gsl_root_fdfsolver_set (s, &FDF, x);
     
       printf ("using %s method, starting from value %f\n", 
               gsl_root_fdfsolver_name (s),yf(x,&params));
     
       printf ("%-5s %10s\n", "iter", "root");
       do {
           iter++;
           status = gsl_root_fdfsolver_iterate (s);
           x0 = x;
           x = gsl_root_fdfsolver_root (s);
           printf ("%5d %10.7f %10.7f\n", iter, x, yf(x,&params));
	   status = gsl_root_test_residual(yf(x, &params), precision);
//         status = gsl_root_test_delta (x, x0, 0, 1e-2);
     
//           if (status == GSL_SUCCESS) { printf ("Converged:\n"); }
     
       } while (status == GSL_CONTINUE && iter < max_iter);
       return status;
}
