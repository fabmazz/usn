import json
import argparse
import numpy as np
from shapely import Polygon, Point
import geopandas as gpd
import pandas as pd
from pathlib import Path
from matplotlib import pyplot as plt
import contextily as cx

parser = argparse.ArgumentParser(description="Script to calculate the grid centers and outlines")
parser.add_argument("city_folder", help="path to the folder with the population data")
parser.add_argument("-p","--projected", action="store_true")
parser.add_argument("--tile_pop", help="name of csv with population data", default="pop_count_tile.csv")

args = parser.parse_args()

FOLD = Path(args.city_folder)

tried_dat = []
if args.projected:
    print("Loading projected territory")
    fopen = "territory_projected"
else:
    print("Loading unprojected territory (in WGS84)")
    fopen ="territory_not_projected"
try:
    with open(FOLD/f"{fopen}.json") as f:
        data = json.load(f)
except FileNotFoundError:
    data = pd.read_pickle(FOLD/f"{fopen}.pickle")

origin = np.array(data["origin"])

CRS_PROJ=data["projection"]


tilesize = np.array(data["tile"])

tilex, tiley = data["ntiles"]

tiles_save=[]
for y in range(tiley):
    ## this is outer most range
    for x in range(tilex):
        corner = origin + tilesize*np.array([x,y])
        center = corner + tilesize / 2
        tid = y*tilex + x
        box = np.stack([corner, corner+np.array([0,tilesize[1]]), corner+tilesize, corner+np.array([tilesize[0],0])])
        #print(box.shape)
        tiles_save.append(dict(tile_id=tid,i=x,j=y, corner=Point(*corner), center=Point(*center), border=Polygon(shell=box))  )

cells = gpd.GeoDataFrame(tiles_save, geometry="center",crs=CRS_PROJ).set_geometry("corner",crs=CRS_PROJ).set_geometry("border",crs=CRS_PROJ)

fout = FOLD/("tiles_data_pr.parq" if args.projected else  "tiles_data_unpr.parq")
print(f"Saving grid data to {fout}")
cells.to_parquet(fout)
plt.close()

have_city_shape = False
try:
    city_shape = gpd.read_file(FOLD/"city.geojson")
    have_city_shape  =True
    ax = city_shape.to_crs(cells.crs).boundary.plot(color="royalblue",figsize=(8,8))
    cells.set_geometry("border").boundary.plot(color="#555",lw=1,ax=ax)
except:
    ax=cells.set_geometry("border").boundary.plot(color="#555",lw=1,figsize=(8,8))

cx.add_basemap(ax,crs=cells.crs)
figfile=FOLD / ("grid_city_pr.pdf" if args.projected else  "grid_city_unpr.pdf")
plt.savefig(figfile, bbox_inches="tight", dpi=180)
print(f"Saved grid plot in {figfile}")
plt.close()

tile_pop_f =  FOLD / args.tile_pop

if tile_pop_f.exists():

    popdata = pd.read_csv(tile_pop_f)
    tilecoords=gpd.GeoDataFrame(pd.merge(cells, popdata, on="tile_id")).set_geometry("border")
    ax=tilecoords.boundary.plot(figsize=(7,7), lw=1)
    if have_city_shape:
        city_shape.to_crs(tilecoords.crs).boundary.plot(ax=ax, color="red")
    #cx.add_basemap(ax, crs=tilecoords.crs)

    figfile=FOLD / ("tiles_city_pr.pdf" if args.projected else  "tiles_city_unpr.pdf")
    plt.savefig(figfile, bbox_inches="tight")
    print(f"Plot of tiles with population > 0 and boundary in {figfile}")

    plt.close()
    if not args.projected:
        tilecoords.to_parquet(FOLD / "tiles_data_pop.parq")

else:
    print(f"Population csv file {tile_pop_f} not found.")