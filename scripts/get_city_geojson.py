#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#
import sys
import requests
import geojson
import osm2geojson
import argparse
from pathlib import Path
from config import OVERPASS_URL

def get_geojson(city, debug=False, dump=True):

    url = OVERPASS_URL #'https://overpass-api.de/api/interpreter?data='
    query = '[out:xml];relation["name" = "' + city + '"]["admin_level"="8"]; out geom;'
    #query = '[out:xml];relation["name" = "Brixen - Bressanone"]["admin_level"="8"]; out geom;'

    if debug:
        print("Executing query: {}".format(query))

    query = url + query

    response = requests.get(query)
    string = response.text.replace("\n","")
    city_geojson = osm2geojson.xml2geojson(string)

    return city_geojson


if __name__ == '__main__':

    #if len(sys.argv) < 2:
    #    print("Usage: {} <city name>".format(sys.argv[0]))
    #    sys.exit(111)
    parser = argparse.ArgumentParser()
    parser.add_argument("city_name", help="City to download")
    parser.add_argument("-w", "--work_dir" , help="Working directory", default="")
    
    args = parser.parse_args()
    city = args.city_name
    fold = Path(args.work_dir)
    if not fold.exists():
        print("Folder doesn't exist, creating it")
        fold.mkdir()

    if not city[0].isupper():
        notupper = True
    else:
        notupper = False

    gj = get_geojson(city, True, True)

    features = len(gj['features']) 
    if features < 1:
        print(f'ERROR: get_city_geojson: the query returned an empty geojson: {gj}! Quit.', file=sys.stderr)
        print(f'You passed a lower case city name but city names in OpenStreatMap normally start with a capital letter, try with: {city.title()}', file=sys.stderr)
        sys.exit(111)
    elif features > 1:
        print(f'WARNING: get_city_geojson: the query returned a geojson with more than 1 features, perhaps it returned more cities? Check below the wikipedia city names:')
        for i,f in enumerate(gj['features']):
            try:
                print(f"{i}) {f['properties']['tags']['wikipedia']}")
            except:
                print(f"{i}) {f['properties']['tags']}")
        j=int(input('Insert the index corresponding to the desired entry: '))
        gj['features'] = [gj['features'][j]]

    outfile = fold / 'city.geojson'
    with open(outfile, 'w') as f:
        geojson.dump(gj, f)
    
    print("{} geojson saved to {}".format(city, outfile))



