#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multiroots.h>
     
#include "fn.h"
#include "fn.c"
#define MINARG 4
#define MAXELEMENTS 100000000
#define MAXLINE 1024

int main (int argc, char *argv[]) {
       int status, i, j;
       int max_iter = 10000;
       const gsl_multiroot_fdfsolver_type *T;
       gsl_multiroot_fdfsolver *s;
       char line[MAXLINE];
       double precision, temp;
       double *k;
       unsigned int n, m, mpairs;
       unsigned int *block;
       FILE *flog = fopen("log.txt", "w");
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_nodes n_blocks precision\n",argv[0]);
	  exit(1);
       }
       n=atoi(argv[1]);
       m=atoi(argv[2]);
       mpairs = (int)(m*(m+1)/2);
       precision=atof(argv[3]);
       if(n+mpairs>MAXELEMENTS) {
	fprintf(stderr,"Too many elements: max number is %d\n",MAXELEMENTS);
	exit(1);
       }
       gsl_vector *x = gsl_vector_alloc(n+mpairs);
       k=(double *)malloc(sizeof(double)*(n+mpairs));
       block=(unsigned int *)malloc(sizeof(unsigned int)*n);
       if(x==NULL) {
	fprintf(stderr,"Could not get memory for %d items of x\n",n+mpairs);
	exit(1);
       }
       for(i=0; i<n; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf %d %lf",k+i,block+i,&temp);
        gsl_vector_set(x,i,temp);
       }
       for(j=0; j<mpairs; j++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i+j);
		exit(1);
	}
	sscanf(line,"%lf %lf",k+i+j,&temp);
        gsl_vector_set(x,i+j,temp);
       }

       unsigned int iter = 0;
     
       struct yf_params params = {k, block, n, m};
#if 0
       abcd(&params);
       exit(0);
#endif
       gsl_multiroot_function_fdf FDF = {&yf, &yf_deriv, &yf_fdf, n+mpairs, &params};
//       FDF.f = &yf;
//       FDF.df = &yf_deriv;
//       FDF.fdf = &yf_fdf;
//       FDF.params = &params;

//       gsl_multiroot_function_fdf f = {&rosenbrock_f,
//                                       &rosenbrock_df,
//                                       &rosenbrock_fdf,
//                                       n, &p};
       
       T = gsl_multiroot_fdfsolver_hybridsj; // newton; // hybridj; //gnewton;
       s = gsl_multiroot_fdfsolver_alloc (T,n+mpairs);
//       printf("S=%d, F=%d\n",s->x->size, (&FDF)->n);

       gsl_multiroot_fdfsolver_set (s, &FDF, x);

       printf("Out of gsl_multiroot_fdfsolver_set\n");
     
       print_state (iter, s, n+mpairs, flog);
       
       fprintf(stdout, "using %s method\n", gsl_multiroot_fdfsolver_name(s));
       // fprintf(flog, "%-5s %10s\n", "iter", "root");
     
       do
         {
           iter++;
           status = gsl_multiroot_fdfsolver_iterate (s);
           print_state (iter, s, n+mpairs, flog);
           if (status)
             break;
           // fprintf(flog, "%5d %10.7f %10.7f\n", iter, x, yf(x,&params));
           status = gsl_multiroot_test_residual (s->f, precision);
         }
       while (status == GSL_CONTINUE && iter < max_iter);
     
       printf ("status = %s\n", gsl_strerror (status));
       // gsl_vector_fprintf(stdout, s->x, "%lf");
       printf("Iter=%d\n",iter);
       fclose(flog);
       gsl_multiroot_fdfsolver_free (s);
       gsl_vector_free (x);
       return 0; //status;
}
