#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import sys
import argparse
from pathlib import Path
import geopandas as gp
from shapely.geometry import box
import pyproj
from pyproj import CRS
from pyproj import Transformer
import pickle
import json
from geopy.distance import distance #The default is the WGS-84
from config import INPUT_CRS, OUTPUT_CRS, TERRITORY_PICKLE_FILE_PROJECTED, TERRITORY_PICKLE_FILE_NOT_PROJECTED

# From wgs84 to wgs84_32n
# coord = (lat, lon)
def project_coord(coord, inCRS, outCRS):
    inproj     = pyproj.CRS(inCRS)
    outproj    = pyproj.CRS(outCRS)

    transf = Transformer.from_crs(inproj, outproj)
    a = transf.transform(coord[1], coord[0])
    return a

# WARNING the origin of tiff files is up-left while the origin of our territory is bottom-left
def get_info_for_territory(input_geojson, tile_dim_x, tile_dim_y, default_CRS, output_CRS, debug=False):

    df = gp.read_file(input_geojson)

    geoj_crs = str(df.crs).upper()
    if default_CRS != geoj_crs:
        print("Error the input geojson CRS is {}, but we expected: {}".format(geoj_crs, default_CRS), file=sys.stderr)
        exit(111)

    # IN WGS84 SAVE COORDS AS LON, LAT (geopandas style): x = LON, y = LAT
    oy, ox = origin_WGS84 = df.bounds['miny'][0], df.bounds['minx'][0]
    print(f'get_info_for_territory, origin {ox}, {oy}')

    # First we compute all territory infos in WGS84, these are needed to process the geo tiff files with population
    boundary = df.bounds
    minx, miny, maxx, maxy = df.bounds['minx'][0], df.bounds['miny'][0], df.bounds['maxx'][0], df.bounds['maxy'][0]
    # print(minx, miny, maxx, maxy)

    xlen = abs(minx - maxx)
    ylen = abs(miny - maxy)

    bbox = box(*df.total_bounds)
    cx, cy = bbox.centroid.x, bbox.centroid.y
    bbox_center = (cx, cy)
    x, y = bbox.exterior.coords.xy
    bbox_vertices = list(zip(x, y))
    origin = bbox_vertices[3]
    
    p0 = (maxy, minx)
    p1 = (maxy, maxx)
    p2 = (miny, maxx)
    #print("p0={}".format(p0))
    #print("p1={}".format(p1))
    #print("p2={}".format(p2))

    dw = distance(p0,p1).m
    dh = distance(p1,p2).m
    #print("dw={}, dh={}".format(dw, dh))

    ntile_x = int(dw//tile_dim_x)
    ntile_y = int(dh//tile_dim_y)
    tile_x = xlen/ntile_x
    tile_y = ylen/ntile_y
    
    # Build the territory datastruct as defined in sb_lib + xlen, ylen
    borders = (maxx, minx, maxy, miny)
    tile = (tile_x, tile_y)
    ntiles = (ntile_x, ntile_y)
    bbox = bbox_vertices
    center = bbox_center
    territory_default = {
        'origin':  origin,      # bottm left
        'center':  center,      # gird center
        'bbox':    bbox,        # bbox = [sw, nw, ne, se]
        'borders': borders,     # borders = (max_x, min_x, max_y, min_y)
        'tile':    tile,        # tuple with dimensions of the tile in meters
        'ntiles':  ntiles,      # tuple with the number of tiles in the grid
        'dim': (xlen, ylen),     # x, y dimensions of the bbox
        'projection': geoj_crs # the actual crs
    }
    
    print("Using default==input CRS: {}".format(default_CRS))
    if debug:
        print(territory_default)

    return territory_default

    #
    # PROJECTION
    #
    # Now save also the territory in PROJECTED coords for the simulation
    # !!! using directly the projection method of the dataframe does not work corectly!!!
    # df = df.to_crs(output_CRS)
    #

    
    #bbox = box(minx, miny, maxx, maxy)  
    #b    = gp.GeoSeries(bbox, crs=INPUT_CRS)    
    #bpj  = b.to_crs(OUTPUT_CRS)

    #boundary = bpj.boundary
    #minx, miny, maxx, maxy = bpj.bounds['minx'][0], bpj.bounds['miny'][0], bpj.bounds['maxx'][0], bpj.bounds['maxy'][0]
    #xlen = abs(minx - maxx)
    #ylen = abs(miny - maxy)

    #ntile_x = int(xlen//tile_dim_x)
    #ntile_y = int(ylen//tile_dim_y)

    #cx, cy = bpj.centroid.x, bpj.centroid.y
    #bbox_center = (cx, cy)
    #x, y = bpj[0].exterior.coords.xy
    #bbox_vertices = list(zip(x, y))

    #origin = bbox_vertices[3]
    
    # Build the territory datastruct as defined in sb_lib + xlen, ylen
    #borders = (maxx, minx, maxy, miny)
    #tile = (tile_dim_x, tile_dim_y)
    #ntiles = (ntile_x, ntile_y)
    #bbox = bbox_vertices
    #center = bbox_center
    #territory_projected = {
    #    'origin':  origin,       # bottm left
    #    'center':  center,       # gird center
    #    'bbox':    bbox,         # bbox = [sw, nw, ne, se]
    #    'borders': borders,      # borders = (max_x, min_x, max_y, min_y)
    #    'tile':    tile,         # tuple with dimensions of the tile in meters
    #    'ntiles':  ntiles,       # tuple with the number of tiles in the grid
    #    'dim': (xlen, ylen),     # x, y diemsnion of the bbox
    #    'projection': output_CRS # the actual crs
    #}
    
    #print("Using output CRS: {}".format(output_CRS))
    #if debug:
    #    print(territory_projected)

    #with open(TERRITORY_PICKLE_FILE_PROJECTED, 'wb') as fout:
    #    pickle.dump(territory_projected, fout)
    #print("Territory data structure saved to {}".format(TERRITORY_PICKLE_FILE_PROJECTED))

    #return territory_default

# Origin must be in (LAT, LON)
def build_projected_territory(origin, ntiles, tile, inCRS, outCRS, city=None, origin_is_projected=False):

    # compute total height and width of the grid == bounding box == bbox
    xdim = tile[0]*ntiles[0]
    ydim = tile[1]*ntiles[1]
    
    # LON, LAT
    if origin_is_projected == False:
        (ox, oy) = utm_origin = project_coord((origin[1], origin[0]), inCRS, outCRS)
    else:
        (ox, oy) = (origin[0], origin[1])

    
    sw = (ox, oy) 
    nw = (ox, oy + ydim)
    ne = (ox + xdim, oy + ydim)
    se = (ox + xdim, oy)
    bbox = [sw, nw, ne, se]
    
    # Compute territory borders and center
    min_x = ox
    min_y = oy
    max_x = ox + xdim
    max_y = oy + ydim
    
    borders = (max_x, min_x, max_y, min_y)
    
    center  = (ox + xdim/2.0, oy + ydim/2.0)
    
    # return a dictionary
    territory   = {
        'origin':  (ox, oy), 
        'center':  center,
        'bbox':    bbox, 
        'borders': borders,   
        'tile':    tile,
        'ntiles':  ntiles,
        'dim':     (xdim, ydim),
        'projection': outCRS
    }

    return territory



# Compute the number of tiles needed to cover the territory.
# Get the surrounding bbox in wgs84, project it to UTM_32N,
# get the height and width, divide by tile dimension and round down
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("input_geojson", help="geojson")
    parser.add_argument("tile_x", help="tile dimension x", type=float)
    parser.add_argument("tile_y", help="tile dimension y", type=float)
    parser.add_argument("-w", "--work_dir" , help="Working directory", default="")
    args = parser.parse_args()

    input_file = args.input_geojson #sys.argv[1]
    tile_dim_x = args.tile_x
    tile_dim_y = args.tile_y
    output_CRS = OUTPUT_CRS
    input_CRS  = INPUT_CRS

    fold_save = Path(args.work_dir)
    ## TODO: the two bounding boxes, although calculated in the same way, are not the same. 
    ## This should be fixed, choosing only *one* territory to avoid confusion 
    territory_not_pj = get_info_for_territory(input_file, tile_dim_x, tile_dim_y, input_CRS, output_CRS, debug=True)
    with open(fold_save/TERRITORY_PICKLE_FILE_NOT_PROJECTED, 'wb') as fout:
        pickle.dump(territory_not_pj, fout)
    print("Territory data structure saved to {}".format(fold_save/TERRITORY_PICKLE_FILE_NOT_PROJECTED))

    origin = (territory_not_pj['origin'][1], territory_not_pj['origin'][0])
    ntiles = territory_not_pj['ntiles']
    tile   = (tile_dim_x, tile_dim_y)
    territory_pj = build_projected_territory(origin, ntiles, tile, input_CRS, output_CRS)

    with open(fold_save/TERRITORY_PICKLE_FILE_PROJECTED, 'w') as fout:
        json.dump(territory_pj, fout)
    print("Territory data structure saved to {}".format(fold_save/TERRITORY_PICKLE_FILE_PROJECTED))

        

