#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
     
#include "fn.h"
#include "fn.c"
#define MINARG 5
#define MAXELEMENTS 1000000
#define MAXLINE 1024

int main (int argc, char *argv[]) {
       int status, i;
       int iter = 0, max_iter = 100;
       const gsl_root_fdfsolver_type *T;
       gsl_root_fdfsolver *s;
       char line[MAXLINE];
       double x0, x, c, precision;
       double *xi, *xj;
       unsigned int n1, n2;
       FILE *flog = fopen("log.txt", "w");
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_elements1 n_elements2 L initial_guess precision\n",argv[0]);
	  exit(1);
       }
       n1=atoi(argv[1]);
       n2=atoi(argv[2]);
       c=atof(argv[3]);
       x=atof(argv[4]);
       precision=atof(argv[5]);
       if(n1>MAXELEMENTS || n2>MAXELEMENTS) {
	fprintf(stderr,"Too many elements: max number is %d\n",MAXELEMENTS);
	exit(1);
       }
       xi=(double *)malloc(sizeof(double)*n1);
       if(xi==NULL) {
	fprintf(stderr,"Could not get memory for %d items of xi\n",n1);
	exit(1);
       }
       for(i=0; i<n1; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf",xi+i);
       }
       if(n2>0) {
       	xj=(double *)malloc(sizeof(double)*n2);
       	if(xj==NULL) {
	 fprintf(stderr,"Could not get memory for %d items of xi\n",n2);
	 exit(1);
        }
        for(i=0; i<n2; i++) {
         if(fgets(line,sizeof(line),stdin)==NULL) {
         	fprintf(stderr,"Could not get line %d of input\n",i);
         	exit(1);
         }
         sscanf(line,"%lf",xj+i);
        }
       }
       gsl_function_fdf FDF;
       struct yf_params params = {xi, xj, n1, n2, c};
     
       FDF.f = &yf;
       FDF.df = &yf_deriv;
       FDF.fdf = &yf_fdf;
       FDF.params = &params;
     
       T = gsl_root_fdfsolver_newton;
       s = gsl_root_fdfsolver_alloc (T);
       gsl_root_fdfsolver_set (s, &FDF, x);
     
       fprintf(flog, "using %s method, starting from values %f,%f\n", 
               gsl_root_fdfsolver_name (s),x,yf(x,&params));
     
       fprintf(flog, "%-5s %10s\n", "iter", "root");
       do {
           iter++;
           status = gsl_root_fdfsolver_iterate (s);
           x0 = x;
           x = gsl_root_fdfsolver_root (s);
           fprintf(flog, "%5d %10.7f %10.7f\n", iter, x, yf(x,&params));
	   status = gsl_root_test_residual(yf(x, &params), precision);
//         status = gsl_root_test_delta (x, x0, 0, 1e-2);
     
//           if (status == GSL_SUCCESS) { printf ("Converged:\n"); }
     
       } while (status == GSL_CONTINUE && iter < max_iter);
       printf("%f", x);
       fclose(flog);
       return status;
}
