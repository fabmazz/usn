#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#   
#    This file is part of USN.
#   
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#   
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#   
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#   
#   

#!/usr/bin/env python
# coding: utf-8

import sys
import os
import argparse
import configparser as cp
import matplotlib.pylab as plt
plt.rcParams["figure.figsize"] = (10,10)
plt.style.use('ggplot')
import pickle, json
from pathlib import Path
import pandas as pd
import igraph
import numpy as np
from scipy.sparse import csr_matrix, coo_matrix
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import colors
import seaborn as sns
sns.set(rc={'figure.figsize':(11, 10)})
figargs = dict(figsize=(10,10))
from config.config import get_territory_file
from lib.libfunc import read_from_config



def get_metrics(graphs):
        vcount    = graphs.vcount()
        ecount    = graphs.ecount()
        density   = round( (2*ecount)/(vcount*(vcount-1)), 4)
        avgdeg    = round(2*ecount/vcount, 2)
        globalC   = round(graphs.transitivity_undirected(mode="zero"), 4)
        avglocC   = round(graphs.transitivity_avglocal_undirected(mode="zero"), 4)
        degAssort = round(graphs.assortativity_degree(directed=False), 4)
        cc        = graphs.components(mode=igraph.WEAK)
        numCC     = len(cc)
        sizeGCC   = len(cc.giant().vs)
        columns = ['nodes', 'edges', 'density', 'avgdeg', 'globalC', 'avglocC', 'assortativity', 'numC', 'sizeGCC']
        metrics = [vcount, ecount, density, avgdeg, globalC, avglocC, degAssort, numCC, sizeGCC]
        df = pd.DataFrame()
        df['metrics']=columns
        df['values']=metrics
        return df

def savefig_comp(name, *args, **kwargs):
    kwargs["bbox_inches"]="tight"
    plt.savefig(name, *args, **kwargs)


def make_table_nodes_stats(graph):
    data=[]
    for i in range(len(graph.vs)):
        v = graph.vs[i]
        attr = dict(v.attributes())
        attr["i"] = i
        data.append(attr)

    data_df = pd.DataFrame(data)

    for k in ["tile_id","type","household","block"]:
        data_df[k] = data_df[k].astype(int)

    return data_df

def count_households(nodes_data_pd):
    num_no_hh = nodes_data_pd[nodes_data_pd.household==-1].shape[0]

    hhcount = nodes_data_pd[nodes_data_pd.household>=0].groupby("household").agg({"id":"count"}).reset_index()

    x,y = np.unique(hhcount["id"], return_counts=True)
    x = np.insert(x, 0, 0)
    y = np.insert(y, 0, num_no_hh)
    return x, y

def get_coord_tile(idcs, dimx, dimy):
    y = np.round(idcs//dimx).astype(int)
    x = idcs - dimx*y
    return x,y


def calculate_harmonic_centrality(x, y , poptile):
    """
    Compute harmonic centrality with \\gamma = 1 and 2
    Give the x and y coordinates of all the cells to consider, and the population count of each cell
    """
    if len(x)!=len(y) or len(y)!=len(poptile):
        raise ValueError("All arguments must have the same length")
    ## 1 tile length = 2 in normalized distance
    d=np.sqrt((x-x[:,np.newaxis])**2 + (y-y[:,np.newaxis])**2)
    d_c = d *2 +np.eye(d.shape[1])
    ## formula (12) in Celestini et al
    hc1 = (np.dot(np.power(d_c,-1),poptile) * poptile)/sum(poptile)
    #npeople_df["hc1"] = hc1.astype(np.float32)

    hc2 = (np.dot(np.power(d_c,-2),poptile) * poptile)/sum(poptile)
    #npeople_df["hc2"] = hc2.astype(np.float32)
    return hc1, hc2

def make_heatmap(df, x, y, z, figsize=(8,6.5), cmap=None, equal_axis=True, zorder=4,**kwargs):
    dfp = df.pivot(columns=x, index=y, values=z)

    f=plt.figure(figsize=figsize)
    ax=sns.heatmap(dfp, cmap=cmap, zorder=zorder,**kwargs)
    ax.invert_yaxis()
    #ax.grid(ls="--")
    if equal_axis:
        ax.axis("equal")
    return f, ax

INPUT_DIR="SINGLE_RUN_OUTPUT/sabaudia/euclideanPower_2_real_lognormal_reader_uniform_10_real__0/"
GRAPH_FILE="sb_graph"
DISTANCE_INPUT_FILE="DISTANCE_AllGroups.pickle"

GLOBAL_METRICS_TABLE_CSV = "globalMetrics.csv"
GLOBAL_METRICS_TABLE_LATEX = "globalMetrics.tex"
IGRAPH_DEGREE_DISTRIB_FILE = 'igraph_degree_distrib.txt'
HOUSEHOLD_DIST_FILE ="household_distrib.svg"
NO_HOUSEHOLD_HEATMAP_FILE="no_hh_tile_distr.svg"
GCC_HEATMAP_FILE="gcc_tile_fraction.svg"
HEATMAP_FILE='heatmap.pdf'
DEG_VS_PEOPLE_FILE='deg_vs_people.pdf'
MEAN_TILE_DEG_VS_PEOPLE_FILE='mean_tile_deg_vs_people.pdf'
TILE_DEG_DIST_FILE='tile_deg_dist.pdf'
PEOPLE_PER_TILE_FILE='people_per_tile.pdf'
GRAPH_DEG_DISTRIB_FILE='deg_distrib.pdf'
EDGE_LENGHT_DISTRIB_FILE='edge_length_distrib.pdf'

def main():

    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-c', '--config', dest='config', type=str, required=True, help='configuration file in .ini format (needed only for territory info)')
    parser.add_argument('-i', '--input_dir', dest='input_dir', type=str, required=True, help='directory with the graph and distances pickle files')
    parser.add_argument('-r', '--remove_households', dest='remove_households', action="store_true",  help='remove househlods edges from input graph')
    parser.add_argument("-gf","--graph_format", default="pickle", help="graph file format")
    args=parser.parse_args()

    config_file = args.config
    input_dir = args.input_dir
    path_dir_out = Path(input_dir)

    if args.remove_households:
        remove_households = True
    else:
        remove_households = False
    
    city=read_from_config(config_file, 'CITY', 'city')
    territory_file_pj = get_territory_file(city)

    with open(territory_file_pj, 'rb') as fin:
        ends = territory_file_pj.split(".")[-1]
        if ends == "pickle":
            territory = pickle.load(fin)
        elif ends == "json":
            territory = json.load(fin)
        else:
            raise ValueError()

    ntiles=territory['ntiles']

    dimx, dimy = ntiles[0], ntiles[1]
    dim=dimx*dimy

    fin = os.path.join(input_dir, GRAPH_FILE+"."+args.graph_format)
    print("reading graph: {}".format(fin))

    g1=igraph.load(fin)
    print(g1.summary())
    print("Graph edges layers: {}".format(np.unique(g1.es['layer'])))


    hh_edges = [e for e in g1.es if "household" in e['layer']]
    fs_edges = [e for e in g1.es if "friendship" in e['layer']]
    print("Num hh edges: {}, num fs edges; {}".format(len(hh_edges), len(fs_edges)))

    nodes_data = make_table_nodes_stats(g1)
    totpeop=nodes_data.groupby("tile_id")["i"].count()
    ## household distributions
    cnts,v = count_households(nodes_data)
    f=plt.figure(figsize=(6,5))
    plt.title(f"Households distribution (tot={sum(v[1:])})")
    plt.bar(cnts,v/v.sum()*100)
    plt.ylabel("Frequency (%)")
    plt.xlabel("Household size")

    savefig_comp(path_dir_out / HOUSEHOLD_DIST_FILE)
    plt.close(f)


    if remove_households:
        g1=g1.subgraph_edges(fs_edges, delete_vertices=True)

    
    # Compute global metrics and dump tex and csv
    dfmetrics=get_metrics(g1)
    dfmetrics = dfmetrics.round(5)
    
    fout_tex = os.path.join(input_dir, GLOBAL_METRICS_TABLE_LATEX)
    fout_csv = os.path.join(input_dir, GLOBAL_METRICS_TABLE_CSV)

    with open(fout_tex, 'w') as tf:
        tf.write(dfmetrics.to_latex())

    dfmetrics.to_csv(fout_csv)

    ## Plot distribution of no households and no connected components
    comps=g1.connected_components()
    comps =  sorted(comps, key=lambda x: len(x),reverse=True)
    ncomp=nodes_data[nodes_data.i.isin(comps[0])]
    ngiantc=ncomp.groupby("tile_id")["i"].count()

    #c=nodes_data[nodes_data.household == -1].groupby("tile_id").agg({"i":"count"}).rename(columns={"i":"num_no_hh"})
    tile_idcs = np.unique(nodes_data.tile_id)
    totc = pd.DataFrame(index=tile_idcs, data = np.full(len(tile_idcs),0), columns=["num_no_hh"])
    vv=nodes_data[nodes_data.household == -1].groupby("tile_id")["i"].count()
    totc.loc[vv.index,"num_no_hh"] = vv.values
    totc["people"]=totpeop
    totc["ngiant"]=ngiantc
    totc["frac_no_hh"] = totc.num_no_hh / totc.people
    tilestats = totc.reset_index(names="tile_id")
    tilestats.loc[:,"ngiant"]=tilestats["ngiant"].fillna(0)
    tilestats["frac_giant"]= tilestats.ngiant / tilestats.people
    x,y = get_coord_tile(tilestats["tile_id"], dimx, dimy)
    tilestats["x"] =x
    tilestats["y"] =y
    hc1, hc2 = calculate_harmonic_centrality(x.values, y.values , tilestats["people"])
    tilestats["hc1"] = hc1.astype(np.float32).values
    tilestats["hc2"] = hc2.astype(np.float32).values

    plt.style.use("default")
    f,ax = make_heatmap(tilestats, "x", "y", "frac_no_hh", cmap="flare_r")
    ax.grid(alpha=0.6)
    plt.title("Fraction of nodes with no household")
    savefig_comp(path_dir_out / NO_HOUSEHOLD_HEATMAP_FILE)
    plt.close()

    f,ax = make_heatmap(tilestats, "x", "y", "frac_giant", cmap="viridis")
    ax.grid(alpha=0.6)
    plt.title("Fraction of nodes in giant component (avg: {:4.2%})".format(len(comps[0]) / len(g1.vs)))
    savefig_comp(path_dir_out / GCC_HEATMAP_FILE)
    plt.close()

    f,ax = make_heatmap(tilestats, "x", "y", "people", cmap="viridis")
    ax.grid(alpha=0.6)
    plt.title("Number of people per tile")
    savefig_comp(path_dir_out / "heatmap_pop.png", dpi=120)
    plt.close()
    plt.style.use("ggplot")

    f,ax = make_heatmap(tilestats, "x", "y", "hc1", cmap="flare_r", figsize=(7,6))
    ax.grid(alpha=0.6)
    plt.title("Harmonic centrality $HC^1_i$")
    savefig_comp(path_dir_out / "harm_centr_p1.png",dpi=200)
    plt.close()

    ## Save result of analysis
    tilestats.to_csv(path_dir_out / "tile_stats.csv", index=False)


    # Compute degree distribution with igraph and dump to text
    a=np.count_nonzero([i for i in g1.degree() if i > 100])
    print("Number of nodes with deg > 100: {}".format(a))
    dd=g1.degree_distribution()
    
    fout_igraph_dd = os.path.join(input_dir, IGRAPH_DEGREE_DISTRIB_FILE)
    with open(fout_igraph_dd, 'w') as tf:
        print(dd, file=tf)


    # DEGREE HEATMAT
    plt.figure()
    df=pd.DataFrame()
    df['degree']=g1.degree()
    df['tile']=g1.vs['tile_id']
    df=df.sort_values(by=['tile'])

    dfdeg=df.groupby('tile').sum()
    dfcount=df.groupby('tile').count()
    dfdeg=dfdeg.reindex(np.arange(0, dim)).fillna(0)
    dfcount=dfcount.reindex(np.arange(0, dim)).fillna(0)

    degree=dfdeg.to_numpy()
    count=dfcount.to_numpy()
    degree2=degree/count
    #degree2=np.nan_to_num(degree2)
    d=degree2.reshape(dimy,dimx)

    plt.style.use('default')
    ntiles = np.prod(d.shape)
    print("Num tiles:",ntiles)
    ax=sns.heatmap(d, cmap="flare_r",xticklabels=True if dimx < 30 else "auto",
            yticklabels=True if dimy < 30 else "auto",
            annot = True if (ntiles < 200) else False)
    plt.xlim(0,dimx)
    plt.ylim(0,dimy)
    ax.axis("equal")
    FOUT=os.path.join(input_dir, HEATMAP_FILE)
    plt.title('Heatmap of average degree per tile in {}'.format(city.capitalize()))
    savefig_comp(FOUT)
    plt.close()
    ##reset style
    plt.style.use('ggplot')



    # # NODE DEGREE VS PEOPLE IN TILE
    plt.figure()
    df=pd.DataFrame()
    df['degree']=g1.degree()
    df['tile']=g1.vs['tile_id']
    df=df.sort_values(by=['tile'])
    pop=df.groupby('tile')['degree'].count()
    df['pop']=[pop[p] for p in df['tile']]

    plt.title('Node degree vs people in tile for {}'.format(city.capitalize()))
    sns.scatterplot(x=df['pop'],y=df['degree'], ax=plt.gca())
    FOUT=os.path.join(input_dir, DEG_VS_PEOPLE_FILE)
    savefig_comp(FOUT)
    plt.close()


    # # Mean Degree per Tile vs People in Tile
    plt.figure()
    sns.scatterplot(x=count.flatten(), y=degree2.flatten(), ax=plt.gca())
    plt.xlabel('people per tile for')
    plt.ylabel('mean degree per tile')
    plt.title('Mean degree vs people (per tile) in {}'.format(city.capitalize()))
    FOUT=os.path.join(input_dir, MEAN_TILE_DEG_VS_PEOPLE_FILE)
    savefig_comp(FOUT)
    plt.close()

    # # TILE DEGREE DISTRIBUTION 
    d=degree2.flatten()
    d=d[d != 0]
    # the histogram of the data
    plt.figure()
    sns.histplot(d, bins=30)
    plt.title('Distribution of average degree per tile in {}'.format(city.capitalize()))
    plt.xlabel('average degree')
    plt.ylabel('number of tiles')
    FOUT=os.path.join(input_dir, TILE_DEG_DIST_FILE)
    savefig_comp(FOUT)
    plt.close()


    # # PEOPLE IN TILE
    dfcount=dfcount.rename(columns={'degree':'people'})
    dfcount= dfcount[dfcount['people']>0]
    plt.figure()
    sns.histplot(dfcount,kde=False)
    plt.title('People in each tile (empty tiles removed) for {}'.format(city.capitalize()))
    plt.xlabel('number of persons')
    plt.ylabel('number of tiles')
    FOUT=os.path.join(input_dir, PEOPLE_PER_TILE_FILE)
    savefig_comp(FOUT)
    plt.close()


    # # GRAPH DEGREE DISTRIBUTION
    realdeg=np.array(g1.degree())
    
    plt.figure()
    sns.histplot(realdeg, stat="density", discrete=True)
    plt.title('Graph degree distribution for {}'.format(city.capitalize()))
    plt.xlabel('degree')
    plt.ylabel('density')
    FOUT=os.path.join(input_dir, GRAPH_DEG_DISTRIB_FILE)
    savefig_comp(FOUT)
    plt.close()

    # # Edge Lenght Distribution
    fin = os.path.join(input_dir, DISTANCE_INPUT_FILE)
    df=pd.read_pickle(fin)
    l=np.array(df)
    plt.figure()
    ax=sns.distplot(l, kde=False)
    plt.title('Edge length distribution for {}'.format(city.capitalize()))
    plt.xlabel('edge length (m)')
    plt.ylabel('number of edges')
    FOUT=os.path.join(input_dir, EDGE_LENGHT_DISTRIB_FILE)
    savefig_comp(FOUT)
    plt.close()

    return

if __name__ == "__main__":
    main()



