#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#
import sys
import os
import argparse
from pathlib import Path
#import gdal
from osgeo import gdal
from shapely.geometry import Polygon as SPolygon
from shapely.geometry import box
import pandas as pd
import geopandas as gpd
import contextily as ctx
import  matplotlib.pylab as plt
import json
from config import INPUT_CRS, OUTPUT_CRS, POPULATION_DATAFRAME_FILE, TIFF_INFO_FILE, GEODATA_CETERS_PLOT_FILE, FIGURE_SIZE 

plt.rcParams["figure.figsize"] = FIGURE_SIZE
plt.style.use('ggplot')

# MY LIBS
from functions import *
from gdal_lib import extract_bbox


def get_file_names(thisdir, ext='tif'):

    file_list = []
    # r=root, d=directories, f = files

    if not os.path.isdir(thisdir):
        print(f'ERROR: get file name: directory {thisdir} not found! Quit.', file=sys.stderr)
        sys.exit(111)

    for f in os.listdir(thisdir):
        if f.endswith(ext):
            file_list.append(f)
    
    return file_list


# WARNING 
# The extracted data is 2 pixel smaller than the given bbox, to have all the
# data points you should give a little larger input bbox. 
# tiff are expexted in wgs84, bbox must be the same.
# tiff file name are expected like: nation_sex_age_year.tif
def get_data_from_multiple_tiff(bbox, tiff_dir, ext='tif'):
    

    print("get_data_from_multiple_tiff: read tiff files from: {}".format(tiff_dir))
    tiff_list = get_file_names(tiff_dir)

    if len(tiff_list) == 0:
        print(f'ERROR: get_data_from_multiple_tiff: directory {tiff_dir} does not contains any tiff file! Quit.', file=sys.stderr)
        sys.exit(111)
    else:
    # tiff file name are expected like: nation_sex_age_year.tif
        first = tiff_list[0]
        name, extension = os.path.splitext(first)
        extension = extension.strip('.')
        if not ext == extension:
            print(f'ERROR: get_data_from_multiple_tiff: file extension is not {ext}: current file: {first}', file=sys.stderr)
            sys.exit(111)
        elif not name.count('_') == 3:
            print("ERROR: get_data_from_multiple_tiff: tif files are expected in the following format: nation_sex_age_year.tif (wrldpop file format)", file=sys.stderr)
            sys.exit(111)

    pop_df = pd.DataFrame()

    for i, f in enumerate(tiff_list):
        print(f"processing file: {i}\r", end='')
        # remove tiff extension
        ff = f.split('.')[0]
        # split the name to get the age and sex
        # ita_m_0_2020
        fields = ff.split('_')
        nation = fields[0]
        sex    = fields[1]
        age    = fields[2]
        year   = int(fields[3])
        col_name = sex + '_' + age

        # Extract the bbox from the tiff
        input_tiff = tiff_dir + '/' + f
        tiff_data  = extract_bbox(bbox, input_tiff, debug=False)

        # new origin is top left! territory origin is bottom left
        # Now I want the territory origin so:
        # territory_origin = tiff_origin - tiff_row*tiff_pixel_height = tiff_origin - row*pH
        cols, rows = tiff_data['dim']
        pH, pW = tiff_data['pixel']
        width = cols*pW
        height = rows*pH
        ox, oy = tiff_data['origin'][0], tiff_data['origin'][1] - height 
        #print("ox, oy: {},{}; cols, rows: {},{}; width, height: {},{}".format(oy, ox, cols, rows, width, height))

        if i == 0:
            pop_df['centers'] = tiff_data['centers']
            pop_df['squares'] = tiff_data['squares']
            pop_df[col_name] = tiff_data['data']
            tiff_info={'origin': (ox, oy), 'dim': (cols, rows), 'dir':tiff_dir, 'file0':f}
        else:
            pop_df[col_name] = tiff_data['data']

    #print(pop_df)


    return pop_df, tiff_info


if __name__ == "__main__":
    
    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-t', '--territory', dest='territory_file', type=str, required=True, help='territory file in pickle format')
    parser.add_argument('-f', '--tiff_dir', dest='tiff_dir', type=str, required=True, help='directory of tiff files with population')
    parser.add_argument('-d', '--debug', dest='debug', type=int, help='activate debug levels [1: main, 2: main + functions]')
    parser.add_argument('-g', '--geo-shape-file', dest='geo_shape_file', required=True, type=str, help='')
    parser.add_argument("-w", "--work_dir", dest="working_dir", help="Folder where to save the file", default=".")
    args=parser.parse_args()

    territory_file = args.territory_file
    tiff_dir = args.tiff_dir
    geo_shape_file = args.geo_shape_file
    workdir=Path(args.working_dir)

    # Parse command line
    if args.debug:
        debug=int(args.debug)
    else:
        debug=0

    with open(territory_file, 'rb') as fin:
        territory = pickle.load(fin)

    if territory['projection'] != INPUT_CRS:
        print(f"ERROR: wrong territory projection, must be in {INPUT_CRS}")
        sys.exit(111)

    max_lon, min_lon, max_lat, min_lat = territory['borders']
    borders = (min_lon, min_lat, max_lon, max_lat)

    # HERE I SHOULD PASS A LITTLE LARGER BBOX. BY NOW 2 PIXELS OF DATA IN BOTH x and y ARE LOST
    pop_df, tiff_info = get_data_from_multiple_tiff(borders, tiff_dir)
    ## Make geopandas dataframe and tell it which columns are geometries
    pop_df = gpd.GeoDataFrame(pop_df).set_geometry("squares").set_geometry("centers")
    dffile = workdir / POPULATION_DATAFRAME_FILE
    pop_df.to_parquet(dffile, compression="brotli")
    print("population dataframe written to {}".format(dffile))
    #print(pop_df.describe())

    # add territory info to tiff_info
    tiff_info['borders'] = borders
    tiff_info['ntiles']  = territory['ntiles']
    tiff_info['tile']    = territory['tile']
    #with open(TIFF_INFO_FILE, 'wb') as fout:
    #    pickle.dump(tiff_info, fout)
    with open(TIFF_INFO_FILE, "w") as fout:
        json.dump(tiff_info, fout)
    print("tiff info written to {}".format(TIFF_INFO_FILE))

    #
    # BELOW IS ALL DEBUG CODE
    #
    # try check origins and plot
    try:
        geo_shape_df = gpd.read_file(workdir / geo_shape_file)
        got_shape = True
    except:
        print("Warning: shape file not found!")
        got_shape = False

    # Check that the x, y of tiff origin and thus all data centers
    # are > then the original bbox origin
    if got_shape:
        tiff_origin = tiff_info['origin']
        bbox_origin = geo_shape_df.bounds['minx'][0], geo_shape_df.bounds['miny'][0]
        #print(tiff_origin, bbox_origin)
        if tiff_origin[0] < bbox_origin[0]:
            print("Tiff origin x > bbox origin x")
        if tiff_origin[1] < bbox_origin[1]:
            print("Tiff origin y > bbox origin y")

    if debug:
        # bounding box
        maxx, minx, maxy, miny = territory['borders']
        bbox = box(minx, miny, maxx, maxy)
        b = gpd.GeoSeries(bbox, crs=INPUT_CRS)

        # tiff data centers
        c = gpd.GeoSeries(pop_df['centers'], crs=INPUT_CRS)
        # tiff data squares
        s = gpd.GeoSeries(pop_df['squares'], crs=INPUT_CRS)
    
        # convert to CTX MAP CRS
        b = b.to_crs(epsg=3857)
        g = geo_shape_df.to_crs(epsg=3857)
        c = c.to_crs(epsg=3857)
        s = s.to_crs(epsg=3857)

        # first plot the centers
        ax = c.plot(alpha=0.5, edgecolor='b', marker='o', color='blue', markersize=0.1)
        # add squares
        s.plot(alpha=0.5, edgecolor='b', ax=ax)
        # add shape in red
        g.plot(alpha=0.5, edgecolor='r', ax=ax)
        # add bbox in green
        b.plot(alpha=0.5, edgecolor='g', ax=ax)
        try:
            ctx.add_basemap(ax, zoom = 12)
        except:
            print("Tried adding the base map, but error occured. Continuing")
        filefig  = workdir / GEODATA_CETERS_PLOT_FILE
        plt.savefig(filefig, bbox_inches="tight")
        print("Bbox, shape and tiff data centers saved to {}".format(filefig))


