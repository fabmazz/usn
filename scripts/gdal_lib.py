import os 
import sys
from osgeo import gdal, osr
from shapely.geometry import Polygon as SPolygon
from shapely.geometry import Point as SPoint

def pixel2coord(i, j, w, h, xoff, yoff):
    """Returns global coordinates from pixel x, y coords"""
    xp = w * i + h * j + xoff
    yp = w * i + h * j + yoff
    return (yp, xp)

def normalize(arr):
    ''' Function to normalize an input array to 0-1 '''
    arr_min = arr.min()
    arr_max = arr.max()
    return (arr - arr_min) / (arr_max - arr_min)

def get_extent(dataset):

    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    transform = dataset.GetGeoTransform()
    minx = transform[0]
    maxx = transform[0] + cols * transform[1] + rows * transform[2]

    miny = transform[3] + cols * transform[4] + rows * transform[5]
    maxy = transform[3]

    return {
            "minX": str(minx), "maxX": str(maxx),
            "minY": str(miny), "maxY": str(maxy),
            "cols": str(cols), "rows": str(rows)
            }

def create_tiles(minx, miny, maxx, maxy, n):
    width = maxx - minx
    height = maxy - miny

    matrix = []

    for j in range(n, 0, -1):
        for i in range(0, n):

            ulx = minx + (width/n) * i # 10/5 * 1
            uly = miny + (height/n) * j # 10/5 * 1

            lrx = minx + (width/n) * (i + 1)
            lry = miny + (height/n) * (j - 1)
            matrix.append([[ulx, uly], [lrx, lry]])

    return matrix

#    minx = hex_nw[1]
#    miny = hex_sw[0]
#    maxx = hex_ne[1]
#    maxy = hex_ne[0]
#    box = (minx, miny, maxx, maxy)
# RETURN:
#    tiff_data = {
#                    'data': data.reshape(-1), 
#                    'origin':(ox, oy), 
#                    'dim':(rows,  cols), 
#                    'squares':square_list, 
#                    'centers': square_centers
#                    'pixel': (pW, pH)
#                }

def extract_bbox(shape_box, input_tiff, dump=False, out_dir='./', out_file='mytiff', debug=False):

    minx, miny, maxx, maxy = shape_box

    if debug:
        print("extract_bbox: input box = {}".format(shape_box))
        print("extract_bbox tif file   = {}".format(input_tiff))
        print("minx = {}".format(minx))
        print("miny = {}".format(miny))
        print("maxx = {}".format(maxx))
        print("maxy = {}".format(maxy))

    if (minx > 0):
        tdata = split_right(input_tiff, minx, maxx, miny, maxy, dump, out_dir, out_file)
        if tdata['data'] is None:
            print(tdata)
            print(f'ERROR: None data extract from tiff file! Quit.', file=sys.stderr)
            sys.exit(111)
    else:
        print("extract_bbox: ERROR, minx < 0 not implemented!")

    data = tdata['data']
    ox, oy = tdata['origin']
    rows, cols  = tdata['dim']
    pW, pH = tdata['pixel']

    data[data < 0] = 0

    square_list = []
    square_centers = []
    for i in range(rows):
        for j in range(cols):
            # square index
            idx = cols*i + j

            # top left corner (guess) INVERSION: lon, lat
            x = ox + j*pW
            y = oy - i*pH
            #print("{}, {}:{}: {}".format(idx, (i,j), (y, x), data[i,j]))

            # square vertices
            # A - B
            # |   |
            # D - C
            A = (x, y)
            B = (x + pW, y)
            C = (x + pW, y - pH)
            D = (x, y - pH)

            #if (x < ox):
            #    print("x < ox, Point {},{} outside box!", x, y)
            #if (y > oy):
            #    print("y < oy, Point {},{} outside box!", x, y)

            square = SPolygon([A, B, C, D])
            square_c = SPoint((x + pW/2.0), (y - pH/2.0))
            #square_c = (square.centroid.y, square.centroid.x) #list(reversed(square_c))
            square_list.append(square)
            square_centers.append(square_c)


    if debug:
        print("data info:")
        print("max: {}".format(data.max()))
        print("min: {}".format(data.min()))
        print("sum: {}".format(data.sum()))

    tiff_data = {'data': data.reshape(-1), 'origin':(ox, oy), 'dim':(cols, rows), 'squares':square_list, 'centers': square_centers, 'pixel': (pW, pH)}

    return tiff_data


def split_right(file_name, minx, maxx, miny, maxy, dump=False, out_dir="./", out_file="mytiff"):

    # Read the tiff and get al the info
    driver = gdal.GetDriverByName('GTiff')
    dataset = gdal.Open(file_name)
    band = dataset.GetRasterBand(1)
    extent = get_extent(dataset)
    cols = int(extent["cols"])
    rows = int(extent["rows"])
    minx_map = float(extent["minX"])
    maxx_map = float(extent["maxX"])
    miny_map = float(extent["minY"])
    maxy_map = float(extent["maxY"])
    transform   = dataset.GetGeoTransform()
    xOrigin     = transform[0]
    yOrigin     = transform[3]
    pixelWidth  = transform[1]
    pixelHeight = -transform[5]
    # Compute the size of the map
    width    = maxx_map - minx_map
    height   = maxy_map - miny_map


    #print("\n")
    #print("INFO ON INPUT MAP:")
    #print("inuput minX: {}".format(minx_map))
    #print("imput  maxX: {}".format(maxx_map))
    #print("imput  minY: {}".format(miny_map))
    #print("imput  maxY: {}".format(maxy_map))
    #print("columns:     {}".format(cols))
    #print("rows:        {}".format(rows))
    #print("width:       {}".format(width))
    #print("height:      {}".format(height))
    #print("origin:      {}".format((xOrigin, yOrigin)))
    #print("pixel size:  {}".format((pixelWidth, pixelHeight)))
    #print("\n")


    # Compute the extraction point from the INPUT BBOX
    # add some pixels to the BBOX to be sure to get all
    # there is something wired in how the data is extracted
    # fro the tiff

    # upper left
    p1 = (minx + pixelWidth, maxy - pixelHeight)
    # bottom right
    p2 = (maxx - pixelWidth, miny + pixelHeight)

    i1 = int(((p1[0] - xOrigin) / pixelWidth)  + 0.5) 
    j1 = int(((yOrigin - p1[1]) / pixelHeight) + 0.5)
    i2 = int(((p2[0] - xOrigin) / pixelWidth)  + 0.5)
    j2 = int(((yOrigin - p2[1]) / pixelHeight) + 0.5)

    # remove one col from x and y to be sure that all the data is inside the
    # original bbox unfortunately I wasn't able to find another solution
    new_cols = i2 - i1 
    new_rows = j2 - j1 

    data = band.ReadAsArray(i1, j1, new_cols, new_rows)
    
    # Correct the new origin to center the data grid in the bbox: above I removed
    # two cols of the grid now I shift one col to the right and one to the bottom
    # to center the data
    new_x = xOrigin + i1*pixelWidth 
    new_y = yOrigin - j1*pixelHeight 
    #print("new origin: {}".format((new_x, new_y)))

    # DEBUG PRINT OF THE EXTRACTED TIFF
    #for i in range(new_rows):
    #    for j in range(new_cols):
    #        if data[i,j] < 0:
    #            data[i,j] = 0
    #        print("{}:{}: {}".format((i,j), data[i,j]))
    #print("data max = {}".format(data.max()))
    #print("data sum = {}".format(data.sum()))

    if dump:
        new_transform = (new_x, transform[1], transform[2], new_y, transform[4], transform[5])

        # Create output directory and file
        os.makedirs(out_dir, exist_ok = True)
        output_file = os.path.join(out_dir, out_file + '.tiff')
        print("writing extracted tiff to: {}".format(output_file))

        dst_ds = driver.Create(output_file,
                               new_cols,
                               new_rows,
                               1,
                               gdal.GDT_Float32)

        #writting output raster
        dst_ds.GetRasterBand(1).WriteArray(data)

        tif_metadata = {
            "minX": str(minx), "maxX": str(maxx),
            "minY": str(miny), "maxY": str(maxy)
        }
        dst_ds.SetMetadata(tif_metadata)

        #setting extension of output raster
        # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
        dst_ds.SetGeoTransform(new_transform)

        wkt = dataset.GetProjection()

        # setting spatial reference of output raster
        srs = osr.SpatialReference()
        srs.ImportFromWkt(wkt)
        dst_ds.SetProjection( srs.ExportToWkt() )

        #Close output raster dataset
        dst_ds = None

    dataset = None

    tiff_data = {'data': data, 'origin': (new_x, new_y), 'dim': (new_rows, new_cols), 'pixel': (pixelWidth, pixelHeight)}

    return tiff_data


#
#
# WARNING THIS MUST BE UPDATED AS split_right !!!!
#
def split_left(file_name, minx, maxx, miny, maxy, dump=False, out_dir="./", out_file="mytiff"):

    if 1:
        return 0

    driver = gdal.GetDriverByName('GTiff')
    dataset = gdal.Open(file_name)
    band = dataset.GetRasterBand(1)

    extent = get_extent(dataset)

    cols = int(extent["cols"])
    rows = int(extent["rows"])

    minx_map = float(extent["minX"])
    maxx_map = float(extent["maxX"])
    miny_map = float(extent["minY"])
    maxy_map = float(extent["maxY"])

    width    = maxx - minx
    height   = maxy - miny
    transform   = dataset.GetGeoTransform()
    xOrigin     = transform[0]
    yOrigin     = transform[3]
    pixelWidth  = transform[1]
    pixelHeight = -transform[5]


    #print("\n")
    #print("INFO ON INPUT MAP:")
    #print("inuput minX: {}".format(minx_map))
    #print("imput  maxX: {}".format(maxx_map))
    #print("imput  minY: {}".format(miny_map))
    #print("imput  maxY: {}".format(maxy_map))
    #print("columns:     {}".format(cols))
    #print("rows:        {}".format(rows))
    #print("width:       {}".format(width))
    #print("height:      {}".format(height))
    #print("origin:      {}".format((xOrigin, yOrigin)))
    #print("pixel size:  {}".format((pixelWidth, pixelHeight)))
    #print("\n")


    # Start extraction
    p1 = (minx, maxy)
    p2 = (maxx, miny)

    i1 = int(((p1[0] - xOrigin) / pixelWidth)  + 0.5) 
    j1 = int(((yOrigin - p1[1]) / pixelHeight) + 0.5)
    i2 = int(((p2[0] - xOrigin) / pixelWidth)  + 0.5)
    j2 = int(((yOrigin - p2[1]) / pixelHeight) + 0.5)
    new_cols = i2 - i1 
    new_rows = j2 - j1 
    data = band.ReadAsArray(i1, j1, new_cols, new_rows)

    new_x = xOrigin + i1*pixelWidth
    new_y = yOrigin - j1*pixelHeight
    #print("new origin: {}".format((new_x, new_y)))

    # DEBUG PRINT OF THE EXTRACTED TIFF
    #for i in range(new_rows):
    #    for j in range(new_cols):
    #        if data[i,j] < 0:
    #            data[i,j] = 0
    #        print("{}:{}: {}".format((i,j), data[i,j]))
    #print("data max = {}".format(data.max()))
    #print("data sum = {}".format(data.sum()))

    if dump:
        new_transform = (new_x, transform[1], transform[2], new_y, transform[4], transform[5])


        # Create output directory and file
        os.makedirs(out_dir, exist_ok = True)
        output_file = os.path.join(out_dir, out_file + '.tiff')
        print("writing extracted tiff to: {}".format(output_file))

        dst_ds = driver.Create(output_file,
                               new_cols,
                               new_rows,
                               1,
                               gdal.GDT_Float32)

        #writting output raster
        dst_ds.GetRasterBand(1).WriteArray(data)

        tif_metadata = {
            "minX": str(minx), "maxX": str(maxx),
            "minY": str(miny), "maxY": str(maxy)
        }
        dst_ds.SetMetadata(tif_metadata)

        #setting extension of output raster
        # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
        dst_ds.SetGeoTransform(new_transform)

        wkt = dataset.GetProjection()

        # setting spatial reference of output raster
        srs = osr.SpatialReference()
        srs.ImportFromWkt(wkt)
        dst_ds.SetProjection( srs.ExportToWkt() )

        #Close output raster dataset
        dst_ds = None

    dataset = None

    tiff_data = {'data': data, 'origin': (new_x, new_y), 'dim': (rows, cols), 'pixel': (pixelWidth, pixelHeight)}
    return tiff_data



# Works with positive longitudes
# To the right of the 0th parallel (greenwich)
def get_tiff_right_0(file_name):

    driver = gdal.GetDriverByName('GTiff')
    dataset = gdal.Open(file_name)
    band = dataset.GetRasterBand(1)

    extent = get_extent(dataset)

    cols = int(extent["cols"])
    rows = int(extent["rows"])

    minx_map  = float(extent["minX"])
    maxx_map  = float(extent["maxX"])
    miny_map  = float(extent["minY"])
    maxy_map  = float(extent["maxY"])

    width     = maxx_map - minx_map
    height    = maxy_map - miny_map
    transform = dataset.GetGeoTransform()
    xOrigin   = transform[0]
    yOrigin   = transform[3]
    pW        = transform[1]
    pH        = -transform[5]
    Cx        = xOrigin + (cols/2)*pW 
    Cy        = yOrigin - (rows/2)*pH 
    center    = (Cy, Cx)

    data = band.ReadAsArray(0, 0, cols, rows)
    data[data < 0] = 0
    square_list = []

    for i in range(rows):
        for j in range(cols):
            # square index
            idx = cols*i + j

            # top left corner (guess) INVERSION: lon, lat
            x = xOrigin + j*pW
            y = yOrigin - i*pH
            #print("{}:{}: {}".format((i,j), (y, x), data[i,j]))

            # square vertices
            # A - B
            # |   |
            # D - C
            A = (x, y) 
            B = (x + pW, y)
            C = (x + pW, y - pH)
            D = (x, y - pH)

            square = SPolygon([A, B, C, D])
            square_list.append(square)

    #Q0 = square_list[0]['coordinates']
    #print("Q0 = {}".format(Q0))
    #sideAB = Q0[1][0] - Q0[0][0]
    #sideBC = Q0[1][1] - Q0[2][1]
    #sideDC = Q0[1][0] - Q0[3][0]
    #sideAD = Q0[0][1] - Q0[3][1]
    #assert sideAB == sideBC == sideDC == sideAD
    #print("sideAB = {}".format(sideAB))
    #print("sideBC = {}".format(sideBC))
    #print("sideDC = {}".format(sideDC))
    #print("sideAD = {}".format(sideAD))

    print("\n")
    print("INFO ON INPUT MAP:")
    print("inuput minX: {}".format(minx_map))
    print("imput  maxX: {}".format(maxx_map))
    print("imput  minY: {}".format(miny_map))
    print("imput  maxY: {}".format(maxy_map))
    print("columns:     {}".format(cols))
    print("rows:        {}".format(rows))
    print("width:       {}".format(width))
    print("height:      {}".format(height))
    print("origin:      {}".format((xOrigin, yOrigin)))
    print("pixel size:  {}".format((pW, pH)))
    print("\n")
    

    print("xOrigin + cols*pW = {}".format(xOrigin + cols*pW))
    print("data max = {}".format(data.max()))
    print("data sum = {}".format(data.sum()))

    return center, data, square_list

# modified to work with negative longitude
def get_tiff_left_0(file_name):

    driver = gdal.GetDriverByName('GTiff')
    dataset = gdal.Open(file_name)
    band = dataset.GetRasterBand(1)

    extent = get_extent(dataset)

    cols = int(extent["cols"])
    rows = int(extent["rows"])

    minx_map  = float(extent["minX"])
    maxx_map  = float(extent["maxX"])
    miny_map  = float(extent["minY"])
    maxy_map  = float(extent["maxY"])

    width     = maxx_map - minx_map
    height    = maxy_map - miny_map
    transform = dataset.GetGeoTransform()
    xOrigin   = transform[0]
    yOrigin   = transform[3]
    pW        = transform[1]
    pH        = transform[5]
    Cx        = xOrigin + (cols/2)*pW 
    Cy        = yOrigin + (rows/2)*pH 
    center    = (Cy, Cx)

    data = band.ReadAsArray(0, 0, cols, rows)
    data[data < 0] = 0
    square_list = []

    for i in range(rows):
        for j in range(cols):
            # square index
            idx = cols*i + j

            # top left corner (guess) INVERSION: lon, lat
            x = xOrigin + j*pW
            y = yOrigin + i*pH
            #print("{}:{}: {}".format((i,j), (y, x), data[i,j]))

            # square vertices
            # A - B
            # |   |
            # D - C
            A = (x, y) 
            B = (x + pW, y)
            C = (x + pW, y + pH)
            D = (x, y + pH)

            square = SPolygon([A, B, C, D])
            square_list.append(square)

    #Q0 = square_list[0]['coordinates']
    #print("Q0 = {}".format(Q0))
    #sideAB = Q0[1][0] - Q0[0][0]
    #sideBC = Q0[1][1] - Q0[2][1]
    #sideDC = Q0[1][0] - Q0[3][0]
    #sideAD = Q0[0][1] - Q0[3][1]
    #assert sideAB == sideBC == sideDC == sideAD
    #print("sideAB = {}".format(sideAB))
    #print("sideBC = {}".format(sideBC))
    #print("sideDC = {}".format(sideDC))
    #print("sideAD = {}".format(sideAD))

    print("\n")
    print("INFO ON INPUT MAP:")
    print("inuput minX: {}".format(minx_map))
    print("imput  maxX: {}".format(maxx_map))
    print("imput  minY: {}".format(miny_map))
    print("imput  maxY: {}".format(maxy_map))
    print("columns:     {}".format(cols))
    print("rows:        {}".format(rows))
    print("width:       {}".format(width))
    print("height:      {}".format(height))
    print("origin:      {}".format((xOrigin, yOrigin)))
    print("pixel size:  {}".format((pW, pH)))
    print("\n")
    

    print("xOrigin + cols*pW = {}".format(xOrigin + cols*pW))
    print("data max = {}".format(data.max()))
    print("data sum = {}".format(data.sum()))

    return center, data, square_list

