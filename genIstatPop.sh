#!/bin/bash

if [ $# -ne 1 ]
then
    printf "Usage $0: <city_name> \n"
    printf "\t <city_name> the name of the city\n"
    exit 1
fi

city=${1}
echo "${city}"

python3 add_istat_ages_to_pop.py -p data/population/istat2020/tavola_pop_res_${city}.csv --city ${city}

