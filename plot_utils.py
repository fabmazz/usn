#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.cm import rainbow as selected_cm # replace with your favourite colormap
from matplotlib.colors import Normalize 
from igraph import plot as igplot

plt.rcParams["figure.figsize"] = (8,6)
plt.style.use('ggplot')


###############################################################################
#
#                           PLOT FUNCTIONS 
#
###############################################################################

def show_matrix(m, xdim, ydim, outfile='matrix', interpolation=None):
    fig = plt.figure()
    plt.imshow(m.toarray(), interpolation=interpolation)
    plt.colorbar()
    plt.xlim(0, xdim)
    plt.ylim(0, ydim)
    plt.title("Adjacency matrix: {}, #edges: {}".format(m.shape, m.nnz))
    outfile = outfile + '.pdf'
    plt.savefig(outfile)
    print("show_matrix: plot saved to: {}".format(outfile))
    return fig
    
def make_grid_plot(territory, pop, grid_centers, outfile='grid'):

    #pop2d = build_pop_histo2d(territory, pop_x_array, pop_y_array)
    grid = build_grid_dictionary (territory, pop2d=None)

    pop_x_array, pop_y_array = zip(*pop['coord'])
    fig = plt.figure()

    # Plot BBOX
    bbox = territory['bbox'].copy()
    bbox.append(bbox[0])
    xbox, ybox = zip(*bbox)
    plt.plot(xbox,ybox, 'r', linewidth=0.5) 

    #Plot GRID
    for k,v in grid.items():
        poly = v['vertices']
        poly.append(poly[0])
        xtile, ytile = zip(*poly)
        plt.plot(xtile, ytile, 'r', linewidth=0.1)
        ctx, cty = v['coord']
        plt.plot(ctx, cty, 'b+', markersize=5)


    # Re-Plot centers using grid_centers
    #gcx, gcy = zip(*grid_centers)
    #plt.plot(gcx, gcy, 'ro', markersize=5)
    ox, oy = territory['origin']
    plt.plot(ox, oy, 'ro', markersize=5)

    # Plot population nodes
    plt.plot(pop_x_array, pop_y_array, 'go', markersize=1)
    plt.axis('equal')

    plt.suptitle("Grid with population")
    nodes_per_tile  = count_tot_nodes_per_tile(pop)
    if len(nodes_per_tile) < 10:
        plt.title("nodes per tile: {}".format(list(nodes_per_tile.values())), fontsize=6)
    outfile = outfile + '.pdf'
    plt.savefig(outfile)
    print("make_grid_plot: plot saved to {}".format(outfile))

    return fig
    
def make_heatmap_plot(territory, pop, outfile='pop_heatmap'):
    pop_x_array, pop_y_array = zip(*pop['coord'])
    pop2d = build_pop_histo2d(territory, pop_x_array, pop_y_array)
    fig = plt.figure()
    sns.heatmap(pop2d.T, xticklabels=True, yticklabels=True, annot=True, square=True)
    ntx, nty = territory['ntiles']
    plt.ylim(0, nty)
    plt.xlim(0, ntx)
    outfile = outfile + '.pdf'
    plt.savefig(outfile)
    print("make_heatmap_plot: plot saved to {}".format(outfile))
    return 

def plot_histo(x, name='x'):
    """ Plot the histogram of x. The parameter name is the title of the plot and
        the name of the output file (" " are replaced by "_").
    """
    plt.figure()
    sns.distplot(x, kde=False, rug=True, norm_hist=False);
    plt.title("{}".format(name))
    name = name.replace(" ", "_")
    outfile = 'histo_' + name + '.pdf'
    plt.savefig(outfile)
    print("plot_histo: histrogram saved to {}".format(outfile))
    return 

###############################################################################
#
#                               GRAPH FUNCTIONS
#
###############################################################################
def plot_graph(g, coordinates=None, outfile='graph'):
    if coordinates:
        layout = coordinates
    else:
        layout = 'kk'

    N = g.vcount()
    vmin = 0
    vmax = max(g.vs['tile_id']) 
    Ntiles = vmax + 1
    norm = Normalize(vmin=vmin, vmax=vmax)
    cmap = selected_cm 
    color_tile = [cmap(norm(i)) for i in range(Ntiles)]
    vmin = 0
    vmax = max(g.vs['type'])
    Ntypes = vmax+1
    norm = Normalize(vmin=vmin, vmax=vmax)
    cmap = selected_cm
    color_type = [cmap(norm(i)) for i in range(Ntypes)]

    visual_style = {}
    visual_style["vertex_size"] = [i for i in g.degree()]
    visual_style["edge_width"] = 0.1
    visual_style["edge_curved"] = False
    visual_style["edge_color"] = 'grey'
    visual_style["layout"] = layout
    visual_style["bbox"] = (2048, 2048)
    visual_style["margin"] = 20
    
    if 'type' in g.vs.attributes():
        visual_style["vertex_color"] = [color_type[tid] for tid in g.vs['type']]
        visual_style["vertex_label"] = g.vs['type']

    outfile = outfile + '.pdf'
    igplot(g, outfile,  **visual_style)
    print("graph plot saved to: {}".format(outfile))
    return



def plot_graph_simple(G, population, geo_X, config_file, filename='graph_plot.png'):
    config = cp.ConfigParser()
    config.read(config_file)
    group_colors = eval(config.get('PLOT_OPTIONS', 'group_colors'))
    groups = population['groups']
    group_size = {g:geo_X[g].shape[0] for g in population['groups']}
    # edges = G.edges()
    # edge_colors = [G[u][v]['color'] for u,v in edges]
    node_colors = []
    for g in groups:
        node_colors.extend([group_colors[g]]*group_size[g])
    # widths = [.01 if G[u][v]['layer']=='stable' else 0.001 for u,v in edges]
    # pos = np.concatenate([geo_X[g] for g in groups])
    pos = list(np.concatenate([geo_X[g] for g in groups]))
    visual_style = {'vertex_color':node_colors, 'vertex_size':5, 'edge_width':.1, 'layout':pos}
    ig.plot(G, filename, **visual_style) 
    # plt.cla()
    # nx.draw(G, arrows=False, node_color=node_colors, pos=pos, node_size=5, width=.01) #alpha=.5,
    # plt.savefig(filename)



