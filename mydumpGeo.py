
import pickle
import os
import igraph
import argparse
import errno
import configparser as cp
import pandas as pd
import numpy as np
import sys
from config.config import get_territory_file


DEFAULT_CONFIG = 'config/analysis.ini'
DEFAULT_OUT    = 'results'
FILE_NAME_PICKLE = 'Geo.pickle'
def main():
    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-g', '--graph-file', dest='g_file', required=True, type=str, help='')
    parser.add_argument('-p', '--path', dest='path', required=True, type=str, help='')
    parser.add_argument('-c', '--config-file', dest='config_file', default=DEFAULT_CONFIG, type=str, help='')
    parser.add_argument('-o', '--output', dest='output_dir', default=DEFAULT_OUT, type=str, help='')
    parser.add_argument('-f', '--output-file', dest='output_file', required=True, type=str, help='')
    parser.add_argument('-y', '--city', dest='city', required=True, type=str, help='')

    args = parser.parse_args()
    graph_file =  args.g_file
    path = args.path+'/global_outbreak/'
    config_file = args.config_file
    output_dir = args.output_dir
    output_file = args.output_file
    city = args.city
    
    # Creates the Output Directory
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass
    config = cp.ConfigParser()
    config.read(config_file)
    epidemic_statuses = eval(config.get( 'MAIN', 'epidemic_statuses'))
 
    with open(get_territory_file(city), 'rb') as fin:
        territory = pickle.load(fin)
    dimx, dimy = territory['ntiles']
    n_tiles = dimx*dimy

    g = igraph.Graph.Read_Pickle(graph_file) 
    
    print('specified path is', path)
    files = os.listdir(path)
    epidemic_files = sorted([f for f in files if f.endswith('iterations.pickle')])
    pathways_files = sorted([f for f in files if f.endswith('infections.pickle')])

    j = 0
    for epidemic_file,pathways_file in zip(epidemic_files,pathways_files):

        print(f'at:\n\t{epidemic_file}\n\t{pathways_file}')

        e = pickle.load( open(path+epidemic_file, "rb" ) )
        p = pickle.load( open(path+pathways_file, "rb" ) )

        # Colonne del Data Frame
        df_step = []
        df_tile_id = []
        df_infected = []
        df_recovered = []
        df_susceptible = []

        firstInfected = [-1]*n_tiles
        peakInfected = [-1]*n_tiles
        maxInfected = [0]*n_tiles
        
        tilePop = [0]*n_tiles
        nNodes = g.vcount()
        tileId = g.vs['tile_id']
        for v in range(nNodes):
            tilePop[ g.vs[v]['tile_id'] ] += 1
        
        sim_steps = []  # sim_steps is a list of list [ [],[],[], ... ]
        nIterations = len(e)
        for i in range(nIterations): # for each iteration
            iteration = [0]*nNodes
            I_tiles = [0]*n_tiles
            R_tiles = [0]*n_tiles
            S_tiles = [0]*n_tiles
            steps = [i]*n_tiles
            for v in range(nNodes): # for each node
                if v in e[i]['status']: # e[i]['status'] is a dictionary { NODE_ID: STATUS }
                    iteration[v] = e[i]['status'][v]
                else:
                    iteration[v] = sim_steps[i-1][v]
                
                tile_id = g.vs[v]['tile_id']
                if iteration[v] == 2:
                    I_tiles[tile_id] += 1
                    if firstInfected[tile_id] == -1:
                        firstInfected[tile_id] = i
                elif iteration[v] == 3:
                    R_tiles[tile_id] += 1
                elif iteration[v] == 1:
                    S_tiles[tile_id] += 1
                else:
                    print("ERROR: epidemic status unexpected, value was: {iteration[v] }")
                    sys.exit()

            for t in range(n_tiles):
                if maxInfected[t] < I_tiles[t]:
                    maxInfected[t] = I_tiles[t]
                    peakInfected[t] = i

            df_step.extend([i]*n_tiles)
            df_tile_id.extend(range(n_tiles))
            df_infected.extend(I_tiles)
            df_recovered.extend(R_tiles)
            df_susceptible.extend(S_tiles)

            sim_steps.append(iteration)
        e = None 

        #for t in range(n_tiles):
        #    if peakInfected[t] != -1:
        #        print(f"Tile {t} peak infected at time {peakInfected[t]} - {df_infected[ peakInfected[t]*n_tiles+t]}|{maxInfected[t]} - population {tilePop[t]}")

        df = pd.DataFrame( { 'step':df_step, 'tile_id': df_tile_id, 'infected': df_infected, 'susceptible': df_susceptible, 'recovered': df_recovered } )
    

        data = { 'dimx': dimx, 'dimy': dimy, 'n_iterations':nIterations, 'df': df, 
             'tilePop': tilePop, 'peakInfected': peakInfected, 'tileId': tileId, 
             'firstInfected': firstInfected, 'pathways': p}

        with open(f"{output_dir}/{city}_{output_file}_{j}_{FILE_NAME_PICKLE}", "wb") as fout:
            pickle.dump( data, fout, protocol=pickle.HIGHEST_PROTOCOL)
        j += 1


if __name__ == "__main__":
    main()
    
    
    
