/*
 *    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
 *
 *    This file is part of USN.
 *
 *    USN is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    USN is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with USN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <omp.h>
#include "utils.h"
#include "iniparser.h"
#include "dictionary.h"

#define MINARG 2
#define REAL double
#define NUMBEROFPROB 4
void Usage(char *cmd) {
    fprintf(stdout,"Usage %s -c conffile [-s directory] [-p pop_file] [-d seed] [-v] \n",cmd);
    exit(1);
}

int verbose=FALSE;
char LogFileName[MAXFILELENGTH]= "BF.log";

#define MAXNCHILDRENC 5
#define MAXNCHILDRENM 4
#if MAXNCHILDRENC > MAXNCHILDRENM
  #define MAXNCHILDREN MAXNCHILDRENC
#else
  #define MAXNCHILDREN MAXNCHILDRENM
#endif

enum {CHILDREN, YOUNG, ADULTS, ELDERLY, NUMBEROFAGES};

enum {FCf, FM, C, GCf, GM, V, PS, NUMBEROFROLES};

enum {PS1, C2, Mf2, Cf3, Mf3, Cf4, Mf4, Cf5, Mf5, Cf6, Cf7, NUMBEROFFAMILYTYPES};

typedef struct people {unsigned int index; int family; unsigned int age; REAL tileid; int done; int peer; REAL posx; REAL posy; int role; int parent; int numchildren; int children[MAXNCHILDREN];} people;
typedef struct {unsigned int nom; REAL prob;} hht;
typedef struct {unsigned int nom; REAL prob;} othert;

int comptile(const void *p1, const void *p2) {
    people *p2p1=(people *)p1;
    people *p2p2=(people *)p2;
    return(p2p1->tileid < p2p2->tileid?-1:1);
}

int comprole(const void *p1, const void *p2) {
    people *p2p1=(people *)p1;
    people *p2p2=(people *)p2;
    return(p2p1->role < p2p2->role?-1:1);
}

#define DELIMITER ","

REAL **ReadProbabilityMatrix(char *filename, int nages, int nfamilystates, int skiplines) {
    int i;
    char stbuf[MAXSTRLEN], *token;
    int agescnt=0;
    FILE *fprob = Fopen(filename, "r");
    REAL **probmatrix=(REAL **)makematr(nages,nfamilystates,sizeof(REAL));
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fprob)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    while ((fgets(stbuf, MAXINPUTLINE, fprob))) {
        token=strtok(stbuf,DELIMITER); /* skip the legend: first column of each row */
        for(i=0; i<nfamilystates; i++) {
            token=strtok(NULL,DELIMITER);
            if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",agescnt+1,filename); }
            probmatrix[agescnt][i]=atof(token);
        }
        agescnt++;
    }
    if(agescnt!=nages) {
        writelog(TRUE,APPLICATION_RC,"Number of lines not consistent: %d %d\n",agescnt, nages);
    }
    fclose(fprob);
    return probmatrix;
}

char** ReadAgesString(char *filename, int nages, int skiplines) {
    int i;
    char stbuf[MAXSTRLEN], *token;
    char **agestrings;
    FILE *fprob = fopen(filename, "r");
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fprob)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    agestrings = malloc(sizeof(char *)*nages);
    i=0;
    while ((fgets(stbuf, MAXINPUTLINE, fprob))) {
        if(i==nages) { 
            writelog(TRUE,APPLICATION_RC,"Number of lines not consistent: %d %d\n",i, nages);
        }
        agestrings[i] = malloc(sizeof(char)*MAXSTRLEN);
        token=strtok(stbuf,DELIMITER); /* read the first column of each row */
        strcpy(agestrings[i], token);
        i++;
    }
    fclose(fprob);
    return agestrings;
}

hht *ReadHouseHoldProbability(char *filename, int nhhprob, int skiplines) {
    int i;
    int hhtcnt=0;
    char stbuf[MAXSTRLEN], *token;
    FILE *fprob = Fopen(filename, "r");
    hht *aohht=(hht *)Malloc(nhhprob*sizeof(hht));
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fprob)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    while ((fgets(stbuf, MAXINPUTLINE, fprob))) {
        token=strtok(stbuf,DELIMITER);
        aohht[hhtcnt].nom=atoi(token);
        token=strtok(NULL,DELIMITER); /* skip this field for the time being */
        token=strtok(NULL,DELIMITER); /* skip this field for the time being */
        token=strtok(NULL,DELIMITER);
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",hhtcnt+1,filename); }
        aohht[hhtcnt].prob=atof(token);
        hhtcnt++;
        if(hhtcnt==nhhprob) { break; }
    }
    fclose(fprob);
    return aohht;
}

othert *ReadOtherProbability(char *filename, int nother, int skiplines) {
    int i;
    int othercnt=0;
    char stbuf[MAXSTRLEN], *token;
    FILE *fprob = Fopen(filename, "r");
    othert *aoother=(othert *)Malloc(nother*sizeof(othert));
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fprob)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    while ((fgets(stbuf, MAXINPUTLINE, fprob))) {
        token=strtok(stbuf,DELIMITER);
        aoother[othercnt].nom=atoi(token);
        token=strtok(NULL,DELIMITER);
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",othercnt+1,filename); }
        aoother[othercnt].prob=atof(token);
        othercnt++;
        if(othercnt==nother) { break; }
    }
    fclose(fprob);
    return aoother;
}



int ReadPeopleFromFile(char *filename, people **p2aop, int skiplines) {
    int i;
    char stbuf[MAXSTRLEN], *token;
    int peoplecnt=0;
    FILE *fpeople = Fopen(filename, "r");
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fpeople)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    while ((fgets(stbuf, MAXINPUTLINE, fpeople))) {
        peoplecnt++;
    }
    *p2aop=(people *)Malloc(sizeof(people)*peoplecnt); 
    rewind(fpeople);
    for(i=0; i<skiplines; i++) {
        if(fgets(stbuf, MAXINPUTLINE, fpeople)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read line %d of file %s\n",i,filename); }
    }
    peoplecnt=0;  
    while ((fgets(stbuf, MAXINPUTLINE, fpeople))) {
        token=strtok(stbuf,DELIMITER);
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",peoplecnt+skiplines,filename); }    
        (*p2aop)[peoplecnt].tileid=atof(token);
        token=strtok(NULL,DELIMITER);
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",peoplecnt+skiplines,filename); }
        (*p2aop)[peoplecnt].posx=atof(token);
        token=strtok(NULL,DELIMITER);
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",peoplecnt+skiplines,filename); }
        (*p2aop)[peoplecnt].posy=atof(token);
        token=strtok(NULL,DELIMITER);    
        if(token==NULL) { writelog(TRUE,APPLICATION_RC,"Not enough fields in line %d of file %s\n",peoplecnt+skiplines,filename); }
        (*p2aop)[peoplecnt].index=peoplecnt;
        (*p2aop)[peoplecnt].age=atoi(token);
        (*p2aop)[peoplecnt].done=0;
        (*p2aop)[peoplecnt].role=-1;
        (*p2aop)[peoplecnt].parent=-1;
	(*p2aop)[peoplecnt].numchildren=-1;
        for(i=0; i<MAXNCHILDRENC; i++) {
            (*p2aop)[peoplecnt].children[i]=-1;
        }
        peoplecnt++;
    }
    fclose(fpeople);
    return peoplecnt;
}


void shuffle(unsigned int *index, unsigned int n) {
    unsigned int k, ind1, tmp;
    for (k = 0; k < n-1; k++) {
        ind1 = (k+1)+(lrand48() % (n-k-1));
        tmp=index[k];
        index[k]=index[ind1];
        index[ind1]=tmp;
    }
}

//
// In the people file, persons must be ordered by their tileid, so that persons
// living in the same tile are contiguous.
//
int main(int argc, char *argv[]) {

    char ConfFileName[MAXSTRLEN]="";
    char Directory[MAXSTRLEN]="";
    char ProbFileName[MAXSTRLEN]="";
    char peoplefilename[MAXSTRLEN]="";
    char hhfilename[MAXSTRLEN]="";
    char otherfilename[MAXSTRLEN]="";      
    char seedstring[MAXSTRLEN]="";      
    char *po; /* used for options' parsing */
    FILE *pdf;
    unsigned int nchildren, nyoung, nadult, nelder;
    long int seed;
    REAL xcenter, ycenter;
    REAL area, perimeter;
    int nskip;
    int dummycap, dummyrig, dummyatt, dummygen;
    REAL dummydist;
    char scratch[MAXSTRLEN];
    REAL **probmatrix;
    unsigned int numberofprob=0;
    int h, i, j, k, l;
    writelog(FALSE, APPLICATION_RC,"Beginning bf\n");

    REAL __tStart=Wtime();
    REAL __tEnd;

    REAL choice;
    char **agestrings;
    char *rolestrings[]={"FCf", "FM", "C", "GCf", "GM", "V", "PS"};

    if(argc<MINARG) {
        Usage(argv[0]);
    }

    for(i = 1; i < argc; i++) {
        po = argv[i];
        if (*po++ == '-') {
            /*
               'L'
               'c'
               'h'
               'v'
	       'p'
	       'd'
               */

            switch (*po++) {
                case 'L':
                    SKIPBLANK
                        strncpy(LogFileName,po,MAXFILELENGTH);
                    break;
                case 'c':
                    SKIPBLANK
                        strncpy(ConfFileName,po,MAXFILELENGTH);
                    break;
                case 's':
                    SKIPBLANK
                        strncpy(Directory,po,MAXFILELENGTH);
                    break;
                case 'p':
                    SKIPBLANK
                        strncpy(peoplefilename,po,MAXFILELENGTH);
                    break;
                case 'd':
                    SKIPBLANK
                        strncpy(seedstring,po,MAXFILELENGTH);
                    seed=atoi(seedstring);
                    break;
                case 'v':
                    verbose=TRUE;
                    break;
                case 'h':
                    Usage(argv[0]);
                    exit(OK);
                    break;
                default:
                    writelog(FALSE,APPLICATION_RC,"Unknown option: %s", argv[i]);
                    Usage(argv[0]);
                    exit(APPLICATION_RC);
                    break;
            }
        }
    }

    dictionary *ini;
    writelog(FALSE,OK,"ReadParameters: open data file <%s>\n",ConfFileName);
    char key[MAXSTRLEN];
    if(strlen(Directory)>0) {
        if(chdir(Directory)<0) { writelog(TRUE,APPLICATION_RC,"Error changing directory:%s\n",Directory); }
    }
    if(strlen(ConfFileName)==0) { writelog(TRUE,APPLICATION_RC,"No Config file\n"); }
    ini = iniparser_load(ConfFileName);

    if(strlen(seedstring)==0) {
        writelog(FALSE,APPLICATION_RC,"Read seed from config\n");
        READINTFI(seed, "random seed");
    } else {
        writelog(FALSE,APPLICATION_RC,"Seed read from command line:%s\n", seedstring);
    }

    srand48(seed);
    READINTFI(numberofprob, "Number of probabilities");
    if(numberofprob<0) {
        writelog(TRUE,APPLICATION_RC,"Invalid number of probabilities %d\n",numberofprob);
    }
    READINTFI(nskip, "Skip lines in probability file");
    READSTRFI(ProbFileName,"Name of the file with the probability of the role");
    probmatrix=ReadProbabilityMatrix(ProbFileName, NUMBEROFAGES,numberofprob,nskip);
    agestrings = ReadAgesString(ProbFileName, NUMBEROFAGES, nskip);
    writelog(FALSE,APPLICATION_RC,"Original Probability Matrix\n");
    for(i=0; i<NUMBEROFAGES; i++) {
        writelog(FALSE,APPLICATION_RC, "%c ", agestrings[i][0]);
        for(j=0; j<numberofprob; j++) {
            writelog(FALSE,APPLICATION_RC,"%f ",probmatrix[i][j]);
            if(j>0) probmatrix[i][j]+=probmatrix[i][j-1]; /* exclusive prefix sum to get the cumulative probability */
            writelog(FALSE,APPLICATION_RC,"(%f) ",probmatrix[i][j]);	
        }
        writelog(FALSE,APPLICATION_RC,"\n");
    }
    
    unsigned int nhhprob;
    READINTFI(nhhprob, "Number of household probabilities");  
    READINTFI(nskip, "Skip lines in household file");
    READSTRFI(hhfilename,"Name of the file with the probability of each household type");
    hht *aohht=ReadHouseHoldProbability(hhfilename, nhhprob, nskip);
    writelog(FALSE,APPLICATION_RC,"HouseHold Probability\n");
    for(j=0; j<nhhprob; j++) {
        writelog(FALSE,APPLICATION_RC,"%d %f ",aohht[j].nom, aohht[j].prob);
        /*    if(j>0) { aohht[j].prob+=aohht[j-1].prob; } exclusive prefix sum to get the cumulative probability */
        writelog(FALSE,APPLICATION_RC,"(%f)\n",aohht[j].prob);    
    }
    REAL normprobmc=aohht[Cf3].prob+aohht[Cf4].prob+aohht[Cf5].prob+aohht[Cf6].prob+aohht[Cf7].prob;
    REAL probmc2p[MAXNCHILDRENC];
    probmc2p[0]=aohht[Cf3].prob/normprobmc;
    probmc2p[1]=probmc2p[0]+aohht[Cf4].prob/normprobmc;
    probmc2p[2]=probmc2p[1]+aohht[Cf5].prob/normprobmc;
    probmc2p[3]=probmc2p[2]+aohht[Cf6].prob/normprobmc;
    probmc2p[4]=probmc2p[3]+aohht[Cf7].prob/normprobmc;
    normprobmc=aohht[Mf2].prob+aohht[Mf3].prob+aohht[Mf4].prob+aohht[Mf5].prob;
    REAL probmc1p[MAXNCHILDRENM];
    probmc1p[0]=aohht[Mf2].prob/normprobmc;
    probmc1p[1]=probmc1p[0]+aohht[Mf3].prob/normprobmc;
    probmc1p[2]=probmc1p[1]+aohht[Mf4].prob/normprobmc;
    probmc1p[3]=probmc1p[2]+aohht[Mf5].prob/normprobmc;    
    writelog(FALSE,APPLICATION_RC,"Cumulative multichildren prob with 2 parents: %f %f %f %f %f\n",probmc2p[0],probmc2p[1],probmc2p[2],probmc2p[3],probmc2p[4]);
    writelog(FALSE,APPLICATION_RC,"Cumulative multichildren prob with 1 parent: %f %f %f %f\n",probmc1p[0],probmc1p[1],probmc1p[2],probmc1p[3]);  
    unsigned int nother;
    READINTFI(nother, "Number of other probabilities");    
    READINTFI(nskip, "Skip lines in other file");
    READSTRFI(otherfilename,"Name of the file with the probability of belonging to other household types");
    othert *aoother=ReadOtherProbability(otherfilename, nother, nskip);
    writelog(FALSE,APPLICATION_RC,"Other Probability\n");
    for(j=0; j<nother; j++) {
        writelog(FALSE,APPLICATION_RC,"%d %f ",aoother[j].nom, aoother[j].prob);
        if(j>0) { aoother[j].prob+=aoother[j-1].prob; } /* exclusive prefix sum to get the cumulative probability */
        writelog(FALSE,APPLICATION_RC,"(%f)\n",aoother[j].prob);    

    }

    unsigned int nop[NUMBEROFAGES];
    people *aofpeople;
    READINTFI(nskip, "Skip lines in people file");
    if(!strcmp(peoplefilename,"")){
        writelog(FALSE,APPLICATION_RC, "Reading peoplefilename\n");
        READSTRFI(peoplefilename,"Name of the file with people information");
    }

    iniparser_freedict(ini);      
    unsigned int totalnumberofpeople=ReadPeopleFromFile(peoplefilename,&aofpeople,nskip);
    REAL rn;

    // ASSIGN ROLES TO PEOPLE 
    for(i=0; i<totalnumberofpeople; i++) {
        rn=drand48();
        for(j=0; j<numberofprob; j++) {
            if(rn<probmatrix[aofpeople[i].age][j]) { break; }
        }
        if(j==numberofprob) {
            writelog(TRUE,APPLICATION_RC,"Unexpected probability %f for person %d %d %f %f %f %f %f %f %f\n",rn, i, aofpeople[i].age,
                    probmatrix[aofpeople[i].age][0],probmatrix[aofpeople[i].age][1],probmatrix[aofpeople[i].age][2],probmatrix[aofpeople[i].age][3],probmatrix[aofpeople[i].age][4],probmatrix[aofpeople[i].age][5],probmatrix[aofpeople[i].age][6]);
        }
        aofpeople[i].role=j;
        aofpeople[i].peer=-1;    
        aofpeople[i].family=-1;    
        aofpeople[i].numchildren=-1;
        if(aofpeople[i].role==PS || aofpeople[i].role==V) {  aofpeople[i].done=1; } else { aofpeople[i].done=0; }
    }
    /*  qsort(aofpeople,totalnumberofpeople,sizeof(people),comptile); */ /* already sorted by tile */

    // COUNT THE PEOPLE IN EACH TILE
    // LOOP OVER ALL THE PEOPLE, WHEN THE TILEID CHANGES STORE THE NUMBER OF PEOPLE
    // IN THAT TILE IN NTILES[J], ALL OTHER ELEMENTS OF NTILES ARE 0
    //
    // NTILES STORES THE NUMBER OF PEOPLE NOT THE NUMBER OF TILES!!!
    //
    unsigned int *ntiles=(unsigned int *)Malloc(sizeof(unsigned int)*totalnumberofpeople);
    REAL tileid=aofpeople[0].tileid;
    unsigned int cnttile=1;
    for(i=1, j=0; i<totalnumberofpeople; i++) {
        if(tileid!=aofpeople[i].tileid) {
            ntiles[j]=cnttile;
            tileid=aofpeople[i].tileid;
            cnttile=0;
            j++;
        }
        cnttile++;
    }
    ntiles[j]=cnttile;  
    unsigned int totaltiles=j+1;
    unsigned int starttile=0;
    unsigned int *ntilenosingle=(unsigned int *)Malloc(sizeof(unsigned int)*totaltiles);
    // LOOP ON TILES
    for(i=0; i<totaltiles; i++) { /* sort people in each tile according to the role. All Single people will be at the end */
        char *p=(char *)aofpeople;
        p+=(starttile*sizeof(people));
        qsort(p,ntiles[i],sizeof(people),comprole);
        for(j=0; j<ntiles[i]; j++) { if(aofpeople[starttile+j].role==PS) { break; } }
        ntilenosingle[i]=j;
        starttile+=ntiles[i];
    }
    if(verbose) {  
        for(i=1, j=0; i<totalnumberofpeople; i++) {
            writelog(FALSE,APPLICATION_RC,"%f %f %f %d %d\n",aofpeople[i].tileid,aofpeople[i].posx,aofpeople[i].posy,aofpeople[i].age,aofpeople[i].role);
        }
    }
    unsigned int whichp;
    starttile=0;
    unsigned int done;
    // LOOP ON TILES
    for(i=0; i<totaltiles; i++) {
				int familyMembersC=0;
				int familyMembersM=0;
        // LOOP ON PEOPLE INSIDE THE I-TH TILE
        for(j=0; j<ntilenosingle[i]; j++) {
            // POINTER TO THE FIRST PERSON IN THE TILE
            whichp=starttile+j;

            // DEPENDING ON THE ROLE DO DIFFERENT THINGS
            switch(aofpeople[whichp].role) { /* this is the heart of the selection and combination process */
                case FCf: /* need to find two parents */
                    done=0;
                    // STARTING FROM THE FOLLOWING PERSON UNTIL THE LAST OF THE TILE (LEAVING SINGLE PERSONS)
                    // SEARCH FOR POSSIBLE PARENTS. FIRST SEARCH FOR FREE PARENTS
                    for(k=whichp+1; k<(starttile+ntilenosingle[i]); k++) {
                        if(aofpeople[k].role==GCf && (aofpeople[k].age>aofpeople[whichp].age || (aofpeople[k].age==2 && aofpeople[whichp].age==2))) { // ADDED AGE CHECK
                            if(!aofpeople[k].done) { /* can be a parent, look for the second parent */
                                for(l=k+1; l<(starttile+ntilenosingle[i]); l++) {
                                    if(aofpeople[l].role==GCf && (aofpeople[l].age>aofpeople[whichp].age || (aofpeople[l].age==2 && aofpeople[whichp].age==2)) && abs((int)aofpeople[l].age-(int)aofpeople[k].age)<2) { // ADDED AGE CHECK
                                        if(!aofpeople[l].done) {
                                            // SET THE COUPLE
                                            aofpeople[l].done=1;
                                            aofpeople[k].done=1;
                                            aofpeople[k].peer=l;			
                                            // EXTRACT NUMBER OF CHILDREN FOR THE CREATED COUPLE
                                            rn=drand48();
					    for(h=0; h<MAXNCHILDRENC; h++) {
						if(rn<probmc2p[h]) {
						    break;
						}
					    }
                                    	    if(h==MAXNCHILDRENC) {
					      writelog(TRUE,APPLICATION_RC,"Unexpected value trying to assign a new child to a couple %d\n",l);
                                    	    }
					    // numchildren IS THE NUMBER OF REMAINING CHILDREN TO SET (l WHEN WE SET THE FIRST CHILD, DECREASED FOR EACH FURTHER CHILD)
                                  	    aofpeople[k].numchildren=h; 
                                            // SET THE CHILD
                                            aofpeople[whichp].done=1;
                                            aofpeople[whichp].parent=k;
                                            // ASSIGN THE CHILD TO K, WHY NOT TO L ALSO?
                                            aofpeople[k].children[0]=whichp;
					    familyMembersC=1;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if(aofpeople[k].role>GCf || aofpeople[whichp].done) {
                            break;
                        }
                    }
                    // FREE PARENTS NOT FOUND
                    if(!aofpeople[whichp].done) {
                        /* tries with those that are already parents */
                        for(k=whichp+1; k<(starttile+ntilenosingle[i]); k++) {
                            //if(aofpeople[k].role>GCf) { break;} /* there are no other potential parents */	      	      
                            if(aofpeople[k].role==GCf && (aofpeople[k].age>aofpeople[whichp].age || (aofpeople[k].age==2 && aofpeople[whichp].age==2))) { // ADDED AGE CHECK
                                if(aofpeople[k].peer>k) { /* Check also for the peer */
                                    if(aofpeople[k].numchildren>0) { /* if this couple has other free spots for children */
                                        for(l=1; l<=familyMembersC; l++) {
                                            if(aofpeople[k].children[l]<0) {
                                                aofpeople[whichp].done=1;
                                                aofpeople[whichp].parent=k;
                                                aofpeople[k].children[l]=whichp;
                                                aofpeople[k].numchildren--;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if(aofpeople[k].role>GCf || aofpeople[whichp].done) {
			        if(!aofpeople[whichp].done && familyMembersC < MAXNCHILDRENC-1){
				  j--;
				  familyMembersC++;
			        }
                                break;
                            }
                        }
                    }
                    break;
                case FM:
                    for(k=whichp+1; k<(starttile+ntilenosingle[i]); k++) {
                        if(aofpeople[k].role==GM && (aofpeople[k].age>aofpeople[whichp].age || (aofpeople[k].age==2 && aofpeople[whichp].age==2))) { // ADDED AGE CHECK
                            if(!aofpeople[k].done) { /* first child for this parent */
                                aofpeople[whichp].done=1;
                                aofpeople[whichp].parent=k;		
                                aofpeople[k].done=1;		
                                aofpeople[k].children[0]=whichp;
                                // EXTRACT NUMBER OF CHILDREN FOR THE PARENT
                                rn=drand48();
                                for(l=0; l<MAXNCHILDRENM; l++) {
                                    if(rn<probmc1p[l]) {
                                        break;
                                    }
                                }
                                if(l==MAXNCHILDRENM) {
                                    writelog(TRUE,APPLICATION_RC,"Unexpected value trying to assign a new child to a single %d\n",l);
                                }
                                // numchildren IS THE NUMBER OF REMAINING CHILDREN TO SET (l WHEN WE SET THE FIRST CHILD, DECREASED FOR EACH FURTHER CHILD)
                                aofpeople[k].numchildren=l;
			        familyMembersM=1;	
                            }
                        }
                        if(aofpeople[k].role>GM || aofpeople[whichp].done) { break; }
                    }
                    if(!aofpeople[whichp].done) {
                        for(k=whichp+1; k<(starttile+ntilenosingle[i]); k++) {
                            // GM CAN HAVE OTHER CHILDREN
                            if(aofpeople[k].role==GM && (aofpeople[k].age>aofpeople[whichp].age || (aofpeople[k].age==2 && aofpeople[whichp].age==2))) { // ADDED AGE CHECK
                                /* it is already a parent. Check if other children are possibile */
                                if(aofpeople[k].numchildren>0) { /* if this parent has other free spots for children */
                                    for(l=1; l<=familyMembersM; l++) {
                                        if(aofpeople[k].children[l]<0) {
                                            aofpeople[whichp].done=1;
                                            aofpeople[whichp].parent=k;
                                            aofpeople[k].children[l]=whichp;
                                            aofpeople[k].numchildren--;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(aofpeople[k].role>GM || aofpeople[whichp].done) { 
			       if(!aofpeople[whichp].done && familyMembersM<MAXNCHILDRENM-1){
				  j--;
				  familyMembersM++;
			       }
			       break; 
			    }/* there are no other potential parents */	      
                        }
                    }
                    break;
                case C:
                    if(aofpeople[whichp].peer<0) {
                        for(k=whichp+1; k<(starttile+ntilenosingle[i]); k++) {
                            if(aofpeople[k].role==C && !aofpeople[k].done) {
                                aofpeople[k].done=1;
                                aofpeople[whichp].done=1;
                                aofpeople[whichp].peer=k;
                                aofpeople[k].peer=whichp;	      
                                break;
                            }
                        }
                    }
                    break;
                case GCf:
                    break;
                case GM:
                    break;
                case V:
                    break;
                default:
                    writelog(TRUE,APPLICATION_RC,"Unexpected role: %d\n",aofpeople[whichp].role);
                    break;
	    }
        }
        starttile+=ntiles[i];
    }


#if 0  
    REAL pgroup2, pgroup3;
    READREALFI(pgroup2,"Probability of being in a group of two people");
    READREALFI(pgroup3,"Probability of being in a group of three people");      
    writelog(FALSE,APPLICATION_RC,"Total number of people %d\n",totalnumberofpeople);

    for(i=0; i<totalnumberofpeople; i++) { peopleavaile[i]=i; }
    shuffle(peopleavaile,totalnumberofpeople);


#endif  

    int familyid = 0;
    int peerid = -1;
    int childid = -1;
    for(i=0; i<totalnumberofpeople; i++) {
        if (aofpeople[i].family < 0 && aofpeople[i].done==1) {
            switch(aofpeople[i].role) {
                case GCf:
                    // Set my family
                    aofpeople[i].family = familyid;
                    // Set my peer family
                    peerid = aofpeople[i].peer;
                    if (peerid >=0) {
                        aofpeople[peerid].family = familyid;
                    }
                    // Set all my children family
                    for(j=0; j<MAXNCHILDRENC; j++) { 
                        if(aofpeople[i].children[j]>=0) { 
                            childid = aofpeople[i].children[j];
                            aofpeople[childid].family = familyid;
                        } 
                    }
                    break;

                case GM:
                    // Set my family
                    aofpeople[i].family = familyid;
                    // Set all my children family
                    for(j=0; j<MAXNCHILDRENM; j++) { 
                        if(aofpeople[i].children[j]>=0) { 
                            childid = aofpeople[i].children[j];
                            aofpeople[childid].family = familyid;
                        } 
                    }
                    break;

                case C:
                    // Set my family
                    aofpeople[i].family = familyid;
                    // Set my peer family
                    peerid = aofpeople[i].peer;
                    if (peerid >=0) {
                        aofpeople[peerid].family = familyid;
                    }
                    break;

                case V:
                    aofpeople[i].family = familyid;
                    rn=drand48();
                    for(j=0; j<nother; j++) {
                        if(rn<aoother[j].prob) { break; }
                    }
                    if(j==nother) {
                        writelog(TRUE,APPLICATION_RC,"Unexpected value trying to define a family for various %d\n",i);
                    }
                    int cmvf=1;
                    // add V people to the family until cmvf < number of extracted family member (aoother[j].nom)
                    // Qui ciclo su tutte le persone dopo di me che stanno nel mio tile ...
                    for(k=i+1; k<totalnumberofpeople && (cmvf<aoother[j].nom); k++) {
                        // Check if I am the last people in the tile
                        if(aofpeople[k].tileid!=aofpeople[i].tileid) {
                            break;
                        }
                        if(aofpeople[k].role==V) {
			    aofpeople[i].children[cmvf-1]=k;
                            aofpeople[k].family=familyid;
                            cmvf++;
                        }
                    }
		    aofpeople[i].numchildren=cmvf;
                    break;
									
                case PS:
                    aofpeople[i].family = familyid;
                    break;

            }
            familyid++;
        }
    }

    // To be used with the households test notebook 
    //FILE *f = Fopen("households.csv", "w");



    unsigned int undone = 0;

    for(i=0; i<totalnumberofpeople; i++) {
        //fprintf(f,"%d,%f,%c,%s,%d\n",aofpeople[i].family, aofpeople[i].tileid, agestrings[aofpeople[i].age][0], rolestrings[aofpeople[i].role], aofpeople[i].done);
				//if(!aofpeople[i].done){undone++;}
        printf("%d %d %d %c %s %d", aofpeople[i].index, aofpeople[i].family, (int)aofpeople[i].tileid, agestrings[aofpeople[i].age][0], rolestrings[aofpeople[i].role], aofpeople[i].done);
        // THE FORMAT IS ALWAYS parent, peer, child1, child2, child3 -- WHEN UNAVAILABLE, VALUES ARE SET TO -1 
        if(aofpeople[i].role==FCf || aofpeople[i].role==FM) { printf(" %d -1 -1 -1 -1 -1 -1 -1\n",aofpeople[i].parent); }
        if(aofpeople[i].role==C) { printf(" -1 %d -1 -1 -1 -1 -1 -1\n",aofpeople[i].peer); }
        if(aofpeople[i].role==GCf) { printf(" -1 %d",aofpeople[i].peer); for(j=0; j<MAXNCHILDREN; j++) { printf(" %d",aofpeople[i].children[j]); } printf(" %d\n",aofpeople[i].numchildren); }
        if(aofpeople[i].role==GM) { printf(" -1 -1"); for(j=0; j<MAXNCHILDREN; j++) { printf(" %d",aofpeople[i].children[j]); } printf(" %d\n",aofpeople[i].numchildren); }
        if(aofpeople[i].role==V) { printf(" -1 -1 -1 -1 -1 -1 -1 %d\n", aofpeople[i].numchildren); }	
        if(aofpeople[i].role==PS) { printf(" -1 -1 -1 -1 -1 -1 -1 -1\n"); }
    }

    //printf("Total undone: %d; Fraction of undone: %lf\n", undone, undone/(totalnumberofpeople+0.));
		
    free(aofpeople);
    free(ntiles);
    free(ntilenosingle);  
    freematr(NUMBEROFAGES,(void **)probmatrix);
    __tEnd=Wtime();
    writelog(FALSE, APPLICATION_RC,"BF completed in %g seconds\n",__tEnd-__tStart);

}
