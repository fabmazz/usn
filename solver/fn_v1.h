struct yf_params {
         double *xi, *xj;
	 int n1, n2;
	 double c;
};
     
double yf (double x, void *params);
double yf_deriv (double x, void *params);
void yf_fdf (double x, void *params, double *y, double *dy);
