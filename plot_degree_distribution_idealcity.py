import igraph
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

main_path = '~/usn/idealcity_runs/'
cities = ['uniform', 'radial_decreasing', 'radial_increasing']
solvers = ['exact', 'approximation']
nruns = 10

d = {}
for u in cities:
    for t in solvers:
        for i in range(nruns):
            g = igraph.read(f'idealcity_runs/idealcity_{u}_{t}_{i}/sb_graph.pickle')
            g = g.subgraph_edges([e.tuple for e in g.es if 'friendship' in e['layer']], delete_vertices=False)
            d.setdefault('city',[]).extend([u]*g.vcount())
            d.setdefault('solver',[]).extend([t]*g.vcount())
            d.setdefault('run',[]).extend([i]*g.vcount())
            d.setdefault('degree',[]).extend(g.degree())
df = pd.DataFrame(d)
df.to_pickle('idealcities_degree_df.pickle')
# print(df.groupby(['city','solver','run']).mean())

# for i in range(nruns):
#     print(i, df[df['run']==i]['degree'].mean())


# plot
sns.displot(data=df, x="degree", hue="solver", col='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_cities.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
# xlog plot
sns.displot(data=df, x="degree", hue="solver", col='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_xlog_cities.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.xscale('log')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
# loglog plot
sns.displot(data=df, x="degree", hue="solver", col='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_loglog_cities.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.xscale('symlog')
plt.yscale('symlog')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
# plot
sns.displot(data=df, x="degree", col="solver", hue='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_solver.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
# xlog plot
sns.displot(data=df, x="degree", col="solver", hue='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_xlog_solver.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.xscale('log')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
# loglog plot
sns.displot(data=df, x="degree", col="solver", hue='city', kind='kde', fill=False)
plot_name = 'idealcity_solver_degree_compare_loglog_solver.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.xscale('symlog')
plt.yscale('symlog')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
