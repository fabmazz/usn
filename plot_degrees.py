import pandas as pd
import os, sys
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
import numpy as np
import pickle
from scipy.sparse.linalg import eigsh
import igraph

sns.set_style('whitegrid', rc={'figure.figsize':(17,6)})
sns.set_context("talk")

def vertical_mean_line(x, **kwargs):
    ls = ":" # {"0":"-","1":"--"}
    plt.axvline(x.mean(), linestyle=ls, color=kwargs.get("color", "g"))
    txkw = dict(size=8, color = kwargs.get("color", "g"), rotation=90)
    tx = "mean: {:.2f}, std: {:.2f}".format(x.mean(),x.std())
    plt.text(x.mean()+1, 0.5, tx, **txkw)

# def get_total_weight:



use = 'R0'
# use = 'all_epidemic/random'

dirs = {}
dirs['HM']  = f'output/epidemics/viterbo/last_runs/{use}/combo1/'
dirs['SN']  = f'output/epidemics/viterbo/last_runs/{use}/combo2/'
dirs['HN']  = f'output/epidemics/viterbo/last_runs/{use}/combo3/'
dirs['AN']  = f'output/epidemics/viterbo/last_runs/{use}/combo4/'
dirs['DN']  = f'output/epidemics/viterbo/last_runs/{use}/combo5/'
dirs['ADN'] = f'output/epidemics/viterbo/last_runs/{use}/combo6/'
delta = 1/3
graph_fname = 'data/graphs/viterbo/muPOLYMOD_EP1_t500_min10pop/sb_graph.pickle'
## load social graph
G = igraph.read(graph_fname)

use = use.replace('/','_')
# mode = 'all'
mode = 'R0'



if mode == 'all':
    all_df = pd.DataFrame()
    for label,epidemics_path in dirs.items():
        plots_path = epidemics_path+'plots/'
        if not os.path.exists(plots_path):
            os.makedirs(plots_path)
        ## get degrees
        degree_files = [os.path.join(epidemics_path,fname) for fname in os.listdir(epidemics_path) if 'infected_degrees' in fname and fname.endswith('.csv')]
        df = pd.DataFrame()
        for fname in degree_files:
            df_ = pd.read_csv(fname, skipinitialspace=True)
            df = pd.concat([df,df_])
        if len(df):
            df['combo'] = [label]*len(df)
            df['degree'] = df[['household','friendship','random']].sum(axis=1)
            all_df = pd.concat([all_df,df])
            degrees = df['degree'].to_numpy()
            # degrees = np.asarray(G.degree())
        ## compute the three threshold approximations
        t1 = degrees.mean()/((degrees**2).mean()-degrees.mean())
        t2 = degrees.mean()/((degrees**2).mean()-2*degrees.mean())
        t3 = 1/eigsh(G.get_adjacency_sparse().astype('f'),1)[0][0]
        print(f'{label}: the three threshold approximations are {t1*delta}, {t2*delta} and {t3*delta}')
        print(f'{label}: the mean degree is {degrees.mean()}')

else:
    data = {'degree':[], 'combo':[]}
    for label,epidemics_path in dirs.items():
        plots_path = epidemics_path+'plots/'
        if not os.path.exists(plots_path):
            os.makedirs(plots_path)
        ## get degrees
        degrees = []
        fnames = [fname for fname in os.listdir(epidemics_path) if 'epi_init' in fname]
        for fname in fnames:
            with open(os.path.join(epidemics_path,fname), 'r') as fdegs:
                next(fdegs)
                for line in fdegs:
                    fields = line.split(', ')
                    static_deg = int(fields[4])
                    hh_deg = int(fields[5])-1
                    # ER
                    if epidemics_path[-2]=='1':
                        degs = [int(c) for c in fields[10:]]
                    # HH + FF (static)
                    elif epidemics_path[-2]=='2':
                        degs = [static_deg for c in fields[10:]]
                    # # HH + ER or PM
                    # elif epidemics_path[-2] in ['2','3']:
                    #     degs = [int(c)+hh_deg for c in fields[10:]]
                    # HH + FF/2 + ER or PM
                    else:
                        degs = [int(c)+hh_deg+(np.random.uniform(0,1,static_deg-hh_deg)>0.5).sum() for c in fields[10:]]
                    degrees.extend(degs) 
        data['degree'].extend(degrees)
        data['combo'].extend([label]*len(degrees))
        degrees = np.asarray(degrees)
        ## compute the three threshold approximations
        t1 = degrees.mean()/((degrees**2).mean()-degrees.mean())
        t2 = degrees.mean()/((degrees**2).mean()-2*degrees.mean())
        t3 = 1/eigsh(G.get_adjacency_sparse().astype('f'),1)[0][0]
        print(f'{label}: the three threshold approximations are {t1*delta}, {t2*delta} and {t3*delta}')
        print(f'{label}: the mean degree is {degrees.mean()}')
    all_df = pd.DataFrame(data)

# sns.histplot(all_df, x="degree", hue="combo", element='poly', fill=False)
# ax = sns.displot(all_df, x="degree", hue="combo", kind="ecdf") # element='poly', fill=False)
sns.displot(all_df, x="degree", hue="combo", kind='kde', fill=False)
# ax = sns.displot(all_df, x="degree", hue="combo", kind='kde', fill=False)
# ax.map(vertical_mean_line, 'degree') 
plot_name = f'{use}_degree_compare.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
for l in ['household','friendship','random']:
    # sns.histplot(all_df, x=l, hue="combo", element='poly', fill=False)
    ax = sns.displot(all_df, x=l, hue="combo", kind="ecdf") #, element='poly', fill=False)
    # ax.map(vertical_mean_line, l) 
    plot_name = f'{use}_{l}_degree_compare.png'
    plt.gcf().set_size_inches(8,4)
    plt.savefig(plot_name, bbox_inches='tight')
    print(f'plot saved at {plot_name}')
