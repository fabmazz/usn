### Author: Fabio Mazza

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
import json, pickle

from pathlib import Path

def make_heatmap_name(fold,name, territory_ext=""):
    folder = Path(fold) / name 
    if territory_ext != "":
        pp_terr = folder / f"territory_projected.{territory_ext}"
        with open(pp_terr) as f:
            if territory_ext=="json":
                terr= json.load(f)
            else:
                terr = pickle.load(f)
    else:
        try:
            with open(folder /f"territory_projected.json") as f:
                terr = json.load(f)
        except:
            terr = pd.read_pickle(folder /f"territory_projected.pickle")
    #terr = pd.read_pickle(
    ### recreate bbox
    bbox=np.array(terr["bbox"])
    ntiles = terr["ntiles"]

    popgen = pd.read_csv(folder / f"population_{name}.csv")
    popgen["tile_id"] = popgen["tile_id"].astype(int)

    npeople=popgen.groupby("tile_id").agg("count")

    npeople = npeople.reset_index()
    ##the x size is the major index
    BIG_TILE = ntiles[0]
    little_i=npeople.tile_id % BIG_TILE
    big_i = (npeople.tile_id / BIG_TILE).astype(int)

    npeople["x"] = little_i
    npeople["y"] = big_i

    print(f"Have {len(npeople)}/{np.prod(ntiles)} tiles with population")
    print(f"Total population: {npeople['type'].sum()}")

    f,ax = plt.subplots()
    nnp=npeople.pivot(columns="x", index="y",values="type")
    ## reverse order of rows (y axis) because the top has higher values
    sns.heatmap(nnp[::-1], cmap="viridis", ax=ax)
    ax.axis("equal")

    return npeople.rename(columns={"type":"count"}), f, ax

def calculate_harmonic_centrality(x, y , poptile):
    ## 1 tile length = 2 in normalized distance
    d=np.sqrt((x-x[:,np.newaxis])**2 + (y-y[:,np.newaxis])**2)
    d_c = d *2 +np.eye(d.shape[1])
    ## formula (12) in Celestini et al
    hc1 = (np.dot(np.power(d_c,-1),poptile) * poptile)/sum(poptile)
    #npeople_df["hc1"] = hc1.astype(np.float32)

    hc2 = (np.dot(np.power(d_c,-2),poptile) * poptile)/sum(poptile)
    #npeople_df["hc2"] = hc2.astype(np.float32)
    return hc1, hc2


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Script to plot the heatmap of the individuals in the tiles")
    parser.add_argument("-n", "--name", dest="name", help="population folder and csv name", required=True)
    parser.add_argument("-bf","--base_folder", dest="base_folder", help="base folder (population)", default=".")

    args = parser.parse_args()
    namef = args.name
    fold_base = Path(args.base_folder)
    
    npeople_df, fig, ax = make_heatmap_name(fold_base,args.name)

    fold_out = fold_base / namef #Path(namef)
    fout=fold_out/"heatmap_pop.png"
    fig.savefig(fout,bbox_inches="tight",dpi=140)
    print(f"Saved population heatmap to {fout}")

    ## make the harmonic centrality
    x = npeople_df["x"].values
    y = npeople_df["y"].values
    pop = npeople_df["count"]

    hc1, hc2 = calculate_harmonic_centrality(x, y, pop)
    npeople_df["hc1"] = hc1.astype(np.float32)
    npeople_df["hc2"] = hc2.astype(np.float32)

    fout = fold_out/"pop_count_tile.csv"
    npeople_df.to_csv(fout, index=False)
    print(f"Saved population count by tile to {fout}")
