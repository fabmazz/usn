import pandas as pd
import os, sys
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
import numpy as np
import igraph

sns.set_style('whitegrid', rc={'figure.figsize':(17,6)})
sns.set_context("talk")

dirs = {}
dirs['HM']  = f'output/epidemics/viterbo/last_runs/R0/combo1/'
dirs['SN']  = f'output/epidemics/viterbo/last_runs/R0/combo2/'
dirs['HN']  = f'output/epidemics/viterbo/last_runs/R0/combo3/'
dirs['AN']  = f'output/epidemics/viterbo/last_runs/R0/combo4/'
dirs['DN']  = f'output/epidemics/viterbo/last_runs/R0/combo5/'
dirs['ADN'] = f'output/epidemics/viterbo/last_runs/R0/combo6/'

graph_fname = 'data/graphs/viterbo/muPOLYMOD_EP1_t500_min10pop/sb_graph.pickle'
## load social graph
G = igraph.read(graph_fname)

# df = pd.DataFrame()
data = {'infected':[], 'beta':[], 'model':[]}
for k,d in dirs.items():
    print(f'reading from the following directory: {d}')
    # files = {float(f.split('beta')[1].split('_',1)[0]):os.path.join(d,f) for f in os.listdir(d) if f.endswith('.csv')}
    # for b,f in files.items():
    #     df_ = pd.read_csv(f, skipinitialspace=True)
    #     df_['beta'] = b
    #     df_['model'] = k
    #     df = pd.concat([df,df_])
    fnames = [fname for fname in os.listdir(d) if 'epi_init' in fname]
    for fname in fnames:
        i = fname.find('beta')
        b = float(fname[i+4:i+4+fname[i+4:].find('_')])
        with open(os.path.join(d,fname), 'r') as fdegs:
            next(fdegs)
            for line in fdegs:
                fields = line.split(', ')
                infected = sum(int(c) for c in fields[7:10])
                run = int(fields[0])
                data['infected'].append(infected)
                data['beta'].append(b)
                data['model'].append(k)
# infected_cols = [c for c in df.columns if 'infected' in c]
# df['infected'] = df[infected_cols].sum(axis=1)
# df = df.query('beta<=0.1')
df = pd.DataFrame(data)
plt.cla()
ax = sns.lineplot(data=df, x='beta', y='infected', hue='model')
xmin,xmax = ax.get_xlim()
ymin,ymax = ax.get_ylim()
# for r in [1.1, 1.3, 1.5]:
for r in [1.3]:
    beta_star = r/(np.mean(G.degree())*3)
    print(f'R0={r}: the needed beta is {beta_star}')
    ax.hlines(r, xmin, beta_star, linestyles=':', colors='k')
    ax.vlines(beta_star, ymin, r, linestyles=':', colors='k')
xmin_,xmax_ = df['beta'].min(),df['beta'].max()
x = np.linspace(xmin_,xmax_,100)
y = x*np.mean(G.degree())*3
ax.plot(x,y,ls='--',c='grey')
ax.set_xlim(xmin,xmax)
ax.set_ylim(ymin,ymax)
plot_name = 'r0_plot_compare.png'
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')

sys.exit()

DIR = '.'

parser=argparse.ArgumentParser(description="The program plots the R0 graph from output files contained in the input directory")
parser.add_argument('-d', '--dir', dest='dir', type=str, default=DIR, help='input directory')
args = parser.parse_args()
d = args.dir
print(f'reading from the following directory: {d}')

files = {float(f.split('beta')[1].split('_',1)[0]):os.path.join(d,f) for f in os.listdir(d) if f.endswith('.csv')}
df = pd.DataFrame()
for b,f in files.items():
    df_ = pd.read_csv(f, skipinitialspace=True)
    df_['beta'] = b
    df = pd.concat([df,df_])
infected_cols = [c for c in df.columns if 'infected' in c]
df['infected'] = df[infected_cols].sum(axis=1)
sns.lineplot(data=df, x='beta', y='infected')
plot_name = 'r0_plot.png'
plt.savefig(plot_name)
print(f'plot saved at {plot_name}')


sys.exit()

