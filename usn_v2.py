#!/usr/bin/env python3
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#


import argparse
from distutils.util import strtobool
import pickle
import os
from datetime import datetime
import numpy as np
from numpy.random import default_rng
from pathlib import Path
import errno
import collections
import pandas as pd
import sys
import shutil

from config.config import GROUP2GROUP 
from create_social_base_v2 import get_input_data_structures, get_population_dictionary, sort_pop_dict_arrays, get_grid_centers, compute_grid_distances, preprocess_g2g_matrix, get_graph
from lib.libfunc import read_from_config
from lib.graphplot import plot_deg_dist_sub_graph, plot_global_metrics, plot_neighbours_distance, plot_deg_dist_whole_graph, plot_population_table 

def create_social_graph(in_path, config_file, rng=None, city=None, filter_pop=False, use_approximation=False, alpha=0.0, verbose=False):

    data      = get_input_data_structures(in_path, config_file, city=city)
    territory = data['territory']
    pop_info  = data['pop_info']
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Selected city: {pop_info['city']}")
    dist_info = data['dist_info']
    mu        = read_from_config(config_file, 'SOCIALITY', 'avg_deg')
    g2g_type  = read_from_config(config_file, 'SOCIALITY', 'group2group_type')
    
    # Compute the coordinates of the grid centers
    grid_centers = get_grid_centers(territory)
    # Compute tile_0 distance array: grid_distance_array_0
    grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
    
    # Create POP
    pop = get_population_dictionary(in_path, config_file, territory, pop_info, filter_pop=filter_pop, rng=rng, verbose=verbose)
    
    # check if size is consistent 
    if pop_info['size'] != len(pop['coord']):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Setting pop_info['size'] to {len(pop['coord'])}")
        pop_info['size'] = len(pop['coord'])

    # Sort population arrays by type. This is NEEDED to be able to use upper triangular matrices in the computation.
    sorted_id = pop['type'].argsort()
    sorted_pop = sort_pop_dict_arrays(sorted_id, pop)

    if g2g_type == 'constant':
        group_names = pop_info['group_names']
        n = len(group_names)
        g2g_matrix = np.ones((n,n))
    else:
        #g2g_dict = read_from_config(config_file, 'SOCIALITY', 'group2group')
        g2g_dict = GROUP2GROUP
        g2g_matrix = preprocess_g2g_matrix(g2g_dict, pop_info, pop)

    g_sb = get_graph(pop, pop_info, grid_distance_array_0, territory, dist_info, g2g_matrix, mu, use_approximation=use_approximation, rng=rng, verbose=True)
    # g_sb = get_graph(pop, pop_info, grid_distance_array_0, territory, dist_info, g2g_matrix, mu, use_approximation=use_approximation, alpha=alpha, rng=rng, verbose=True)
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Graph created, summary: \n\t{g_sb.summary()}")

    return pop, g_sb, grid_distance_array_0, pop_info

    
    
DEFAULT_IN_PATH = '.'
BF_FILE='BuildSBG/bf'

def main():
    parser=argparse.ArgumentParser(description="The program creates a Urban Social Network")
    parser.add_argument('-c', '--config', dest='config', required=True, type=str, help='configuration file in .ini format')
    parser.add_argument('-o', '--output', dest='output_dir', required=True, type=str, help='output directory where dumps are stored')
    #parser.add_argument('--dump', dest='dump', action='store_true', default=False, help='dump all data structures in pickle format')
    #parser.add_argument('--dump-pop', dest='dumpop', action='store_true', default=False, help='dump population csv and exit')
    #parser.add_argument('--plot', dest='plot', action='store_true', default=False, help='plot metrics and distributions')  
    parser.add_argument('-i', '--input_path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('--filter_pop', dest='filter_pop', const=True, default=False, action='store', nargs='?', help='whether to filter tiles based on their population; if the optional value is given, only the tiles having a number of residents not lesser than that value are considered; if the optional value is not given, the filter chooses automatically based on the knee of the plot of the population of each tile')
    parser.add_argument('--use_exact', dest='use_approximation', action='store_false', default=True, help='whether to use the exact solution rather than the sparse-graph approximation when computing the edge probabilities')
    parser.add_argument('--verbose', dest='verbose', action='store_true', default=False, help='whether to print additional info about the population')

    
    args = parser.parse_args()
    in_path =  args.in_path
    config_file = args.config
    output_dir = args.output_dir
    filter_pop = args.filter_pop
    if isinstance(filter_pop, str):
        try:
            filter_pop = int(filter_pop)
        except:
            pass
    use_approximation = args.use_approximation
    verbose = args.verbose
    #dumpop = args.dumpop
    #dump = args.dump 
    #plot = args.plot
    
    if not os.path.exists(os.path.join(in_path,config_file)):
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The configuration file '{os.path.join(in_path,config_file)}' does not exist!")
        sys.exit()   
        
    if not os.path.exists(os.path.join(in_path, BF_FILE)): 
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The file {BF_FILE} is missing. You must create it before runnging this program. Enter folder BuildSBG and run 'make all' to compile it.")
        sys.exit()  
    
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Parameters\n",
          f"\t Configuration file: {config_file}\n",
          f"\t Input path: {in_path}\n",
          f"\t Output directory: {output_dir}\n",
          #f"\t Dump: {dump}\n",
          #f"\t Dump population: {dumpop}\n",
          #f"\t Plot: {plot}"
          )
    if use_approximation:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Using the sparse-graph approximation")
    else:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Using the exact model formulation")

    city = read_from_config(config_file, 'CITY', 'city')
    alpha = read_from_config(config_file, 'LATENT', 'alpha')
    # Creates the Social Graph
    seed = read_from_config(config_file, 'RANDOM', 'seed')
    # If None, then fresh, unpredictable entropy will be pulled from the OS. (NumPy Documentation)
    rng = default_rng(seed)
    population, sb_graph, grid_distance_array_0, pop_info = create_social_graph(in_path, config_file, rng, city, filter_pop=filter_pop, use_approximation=use_approximation, alpha=alpha, verbose=verbose)
    
    # Creates the Output Directory
    #if dump or plot or dumpop:
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
    pass
            
    #with open(output_dir+"/sb_graph.pickle", "wb") as fout:
    #    pickle.dump(sb_graph, fout, protocol=pickle.HIGHEST_PROTOCOL)
    out_graph = output_dir+"/sb_graph.graphmlz"
    sb_graph.write_graphmlz(out_graph,compresslevel=3)
    if verbose:
            print(f'{datetime.now().strftime("%d/%m/%Y %H:%M:%S")} - Graph saved at {out_graph}')

    shutil.copy(config_file, output_dir+"/config.ini")
    if verbose:
        print("Copied config file")
   
    # Dumping population
    #if dumpop:
    #    pop_df = pd.DataFrame(population)
    #    print(pop_df)
    #    pop_df.to_csv(f"{output_dir}/{POP_CSV_FILE}")
    #    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - population saved to {POP_CSV_FILE}")
  
    # Dumping data
    #if dump:
    #    Path(output_dir).mkdir(parents=True, exist_ok=True)
    #    with open(output_dir+"/pop_info.pickle", "wb") as fout:
    #        pickle.dump(pop_info, fout, protocol=pickle.HIGHEST_PROTOCOL)
    #    with open(output_dir+"/population.pickle", "wb") as fout:
    #        pickle.dump(population, fout, protocol=pickle.HIGHEST_PROTOCOL)
    #    with open(output_dir+"/sb_graph.pickle", "wb") as fout:
    #        pickle.dump(sb_graph, fout, protocol=pickle.HIGHEST_PROTOCOL)
    
    # Graph analisys and plotting
    #if plot or dump:
        # Creates groups
        # groups_size - { <str:GroupName_1>: <int:size_1>, <str:GroupName_2>: <int:size_2>, ... }  
    #    groups_size = {}
    #    cnt_types = dict(collections.Counter(population["type"]))
    #    for k in cnt_types.keys():
    #        groups_size[ pop_info['group_names'][k] ] = cnt_types[k]
    #    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Groups size: {groups_size}")
        # Creates a subgraph for each group in group_size
        # { <str:GroupName_1>: graph, <str:GroupName_2>: graph  }
    #    graphs = {}
    #    for i, (group_name, _ ) in enumerate(groups_size.items()):
    #        graphs[group_name] = sb_graph.induced_subgraph( [j.index for j in sb_graph.vs if j['type'] == i ] )
    #    graphs["Whole Graph"] = sb_graph

    # Computes the degree distributions of the graph and its subgraphs
    #if dump and not plot:
    #    acc = 0
    #    for k,v in groups_size.items():
    #        degDist = sb_graph.degree( [i for i in range( acc, acc+v)] )
    #        acc +=v
    #        with open(output_dir+"/DEGREE_WholeGraph_"+k.title().replace(" ","_")+".pickle", "wb") as fout:
    #            pickle.dump(degDist, fout, protocol=pickle.HIGHEST_PROTOCOL)
    #    degDist = sb_graph.degree()
    #    with open(output_dir+"/DEGREE_WholeGraph_AllGroups.pickle", "wb") as fout:
    #        pickle.dump(degDist, fout, protocol=pickle.HIGHEST_PROTOCOL)
    #    for k,v in graphs.items():
    #        with open(output_dir+"/DEGREE_SubGraph_"+k.title().replace(" ","")+".pickle", "wb") as fout:
    #            pickle.dump(v.degree(), fout, protocol=pickle.HIGHEST_PROTOCOL)
    #    for k, g in graphs.items():
    #        distDist = g.es["distance"]
    #        with open(output_dir+"/DISTANCE_"+k.title().replace("Whole Graph","AllGroups")+".pickle", "wb") as fout:
    #            pickle.dump(distDist, fout, protocol=pickle.HIGHEST_PROTOCOL)
    
    # Plot analysis results
    #if plot:
    #    plot_deg_dist_whole_graph( groups_size, sb_graph, dump, output_dir )
    #    plot_deg_dist_sub_graph( graphs, dump, output_dir ) 
    #    plot_neighbours_distance( graphs, dump, output_dir )
    #    plot_global_metrics( graphs, output_dir )
    #    plot_population_table( groups_size, output_dir )
        

    
            
if __name__ == "__main__":
    main()
