int yf (gsl_vector *x, void *params, gsl_vector *f) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   double *x = p->x;
   double *k = p->k;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,deg_i,deg_j,y_IJ,L_IJ; 

#pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      deg_i = gsl_vector_get(f,i);
      I = block[i];
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
      	 deg_j = gsl_vector_get(f,j);
	 J = block[j];
	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
      	 y_IJ = gsl_vector_get(x,b2b_id);
      	 L_IJ = gsl_vector_get(f,b2b_id);
	 deg_i += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
         gsl_vector_set(f,i,deg_i);
	 deg_j += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
         gsl_vector_set(f,j,deg_j);
	 L_IJ += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
         gsl_vector_set(f,b2b_id,L_IJ);
      }
   }

   for (i=0;i<n+(int)(m*(m+1)/2);i++){
      gsl_vector_set(f,i,gsl_vector_get(f,i)-k[i])
   }

   return GSL_SUCCESS
}

     
int yf_deriv (gsl_vector *x, void *params, gsl_matrix *df) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   double *x = p->x;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,a,b,c,y_IJ; 

#pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      I = block[i];
      b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+I;
      y_IJ = gsl_vector_get(x,b2b_id);
      b = (x_i*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
      gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+2*b);
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
	 J = block[j];
	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
	 y_IJ = gsl_vector_get(x,b2b_id);
      	 
	 a = (x_i*x_j/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 b = (x_i*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 c = (x_j*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 
         gsl_matrix_set(df,i,b2b_id,gsl_matrix_get(df,i,b2b_id)+a);
         gsl_matrix_set(df,j,b2b_id,gsl_matrix_get(df,j,b2b_id)+a);
         gsl_matrix_set(df,b2b_id,b2b_id,gsl_matrix_get(df,b2b_id,b2b_id)+a);
         
	 gsl_matrix_set(df,b2b_id,i,gsl_matrix_get(df,b2b_id,i)+c);
         gsl_matrix_set(df,b2b_id,j,gsl_matrix_get(df,b2b_id,i)+b);

	 gsl_matrix_set(df,i,j,b);
	 gsl_matrix_set(df,j,i,c);
         gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+c);
         gsl_matrix_set(df,j,j,gsl_matrix_get(df,j,j)+b);

      }
   }

   return GSL_SUCCESS
}
     
int yf_fdf (gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *df) {
   struct yf_params *p = (struct yf_params *)params;
   unsigned int *block = p->block;
   double *x = p->x;
   double *k = p->k;
   unsigned int n = p->n;
   unsigned int m = p->m;
   unsigned int i,j,I,J,b2b_id;
   double x_i,x_j,deg_i,deg_j,y_IJ,L_IJ,a,b,c; 

#pragma omp parallel for reduction(+:v)
   for (i=0;i<n;i++){
      x_i = gsl_vector_get(x,i);
      deg_i = gsl_vector_get(f,i);
      I = block[i];
      b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+I;
      y_IJ = gsl_vector_get(x,b2b_id);
      b = (x_i*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
      gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+2*b);
      for (j=i+1;j<n;j++){
   	 x_j = gsl_vector_get(x,j);
      	 deg_j = gsl_vector_get(f,j);
	 J = block[j];
	 b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
      	 y_IJ = gsl_vector_get(x,b2b_id);
      	 
	 a = (x_i*x_j/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 b = (x_i*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 c = (x_j*y_IJ/(1.+x_i*x_j*y_IJ)*(1.+x_i*x_j*y_IJ);
	 
         gsl_matrix_set(df,i,b2b_id,gsl_matrix_get(df,i,b2b_id)+a);
         gsl_matrix_set(df,j,b2b_id,gsl_matrix_get(df,j,b2b_id)+a);
         gsl_matrix_set(df,b2b_id,b2b_id,gsl_matrix_get(df,b2b_id,b2b_id)+a);

         gsl_matrix_set(df,b2b_id,i,gsl_matrix_get(df,b2b_id,i)+c);
         gsl_matrix_set(df,b2b_id,j,gsl_matrix_get(df,b2b_id,i)+b);

	 L_IJ = gsl_vector_get(f,b2b_id);
	 deg_i += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,i,deg_i);
	 deg_j += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,j,deg_j);
	 L_IJ += (x_i*x_j*y_IJ/(1.+x_i*x_j*y_IJ));
	 gsl_vector_set(f,b2b_id,L_IJ);
	 gsl_matrix_set(df,i,j,b);
	 gsl_matrix_set(df,j,i,c);
         gsl_matrix_set(df,i,i,gsl_matrix_get(df,i,i)+c);
         gsl_matrix_set(df,j,j,gsl_matrix_get(df,j,j)+b);
      }
   }

   for (i=0;i<n+(int)(m*(m+1)/2);i++){
      gsl_vector_set(f,i,gsl_vector_get(f,i)-k[i])
   }

   return GSL_SUCCESS
}



/////////////////////////////////////////////////////////////////////////////

double yf(double x, void *params) {
       struct yf_params *p = (struct yf_params *) params;
       unsigned int n1 = p->n1;
       unsigned int n2 = p->n2;
       unsigned int i,j;
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 

#pragma omp parallel for reduction(+:v)
       for(i=0; i<n1; i++) {
        if(n2>0) {
         for(j=0; j<n2; j++) {
          v+=(1./(1.+xi[i]*xj[j]*x));
         }
        } else {
         for(j=i+1; j<n1; j++) {
          v+=(1./(1.+xi[i]*xi[j]*x));
         }
        }
       }
       return (v - c);
}
     
double yf_deriv (double x, void *params) {
       struct yf_params *p = (struct yf_params *) params;

       unsigned int n1 = p->n1;
       unsigned int n2 = p->n2;
       unsigned int i,j;
     
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 
#pragma omp parallel for reduction(+:v)
       for(i=0; i<n1; i++) {
        if(n2>0) {
         for(j=0; j<n2; j++) {
          v+=(-(xi[i]*xj[j])/((1.+xi[i]*xj[j]*x)*(1.+xi[i]*xj[j]*x)));
         }
        } else {
         for(j=i+1; j<n1; j++) {
          v+=(-(xi[i]*xi[j])/((1.+xi[i]*xi[j]*x)*(1.+xi[i]*xi[j]*x)));
         }
        }
       }	
       return (v);
     
}
     
void yf_fdf (double x, void *params, double *y, double *dy) {
       struct yf_params *p = (struct yf_params *) params;

       unsigned int n1 = p->n1;
       unsigned int n2 = p->n2;
       unsigned int i,j;
     
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 
#pragma omp parallel for reduction(+:v)
       for(i=0; i<n1; i++) {
        if(n2>0) {
         for(j=0; j<n2; j++) { 
          v+=(1./(1.+xi[i]*xj[j]*x));
         }
        } else {     
         for(j=i+1; j<n1; j++) { 
          v+=(1./(1.+xi[i]*xi[j]*x));
         }
        }	
       }
       
       double dv=0.; 
#pragma omp parallel for reduction(+:dv)
       for(i=0; i<n1; i++) { 
        if(n2>0) {
         for(j=0; j<n2; j++) { 
          dv+=(-(xi[i]*xj[j])/((1.+xi[i]*xj[j]*x)*(1.+xi[i]*xj[j]*x))); 
         }
        } else {
         for(j=i+1; j<n1; j++) { 
          dv+=(-(xi[i]*xi[j])/((1.+xi[i]*xi[j]*x)*(1.+xi[i]*xi[j]*x))); 
         }
        }	
       }
     
       *y = (v-c);
       *dy = dv;
}
