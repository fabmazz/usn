#!/usr/bin/env python3
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import os
import sys
import pickle
import argparse
import configparser as cp
from timeit import default_timer as timer
import numpy as np
# from scipy.sparse import csr_matrix
from numpy.random import default_rng
from collections import Counter
from time import time

import igraph as ig

from lib.libfunc import read_from_config

from ndlibA.models.CompositeModel import CompositeModel
from ndlibA.models.compartments.EdgeCategoricalAttribute import EdgeCategoricalAttribute
from ndlibA.models.compartments.NodeNumericalAttribute import NodeNumericalAttribute
from bokeh.io import output_file, save
# from celluloid import Camera

from ndlibA.viz.bokeh.DiffusionTrend import DiffusionTrend
from ndlibA.viz.bokeh.DiffusionPrevalence import DiffusionPrevalence

from config.config import GROUP_NAMES, GROUP2GROUP, ZERO_DIST, TERRITORY_PICKLE_FILE_PROJECTED, get_territory_file, get_population_file
from create_social_base import get_grid_centers, compute_grid_distances, preprocess_distances, preprocess_g2g_matrix, get_fitness, count_tot_nodes_per_block, compute_block_normalization_matrix, compute_single_node_edge_probability, compute_geographic_harmonic_centrality

RNG = None

#### INTERACTION GRAPH PARAMETERS ####

def read_graph_params(config_file):
    config = cp.ConfigParser()
    config.read(config_file)
    avg_degree = eval(config['CONTACTS']['avg_degree'])
    activation_probability = eval(config['CONTACTS']['activation_probability'])
    random_mode = eval(config['CONTACTS']['random_mode'])
    use_distance = eval(config['CONTACTS']['use_distance'])
    fitness_func = eval(config['CONTACTS']['fitness_func'])
    return avg_degree, activation_probability, random_mode, use_distance, fitness_func


#### DIFFUSION MODEL PARAMETERS ####

def read_epidemics_params(config_file):
    config = cp.ConfigParser()
    config.read(config_file)
    params = {k:eval(v) for k,v in config['EPIDEMICS'].items()}
    return params


#### ADDING RANDOM CONTACTS ####

def get_territory_and_distances(city, dist_func=None):
    if dist_func is None:
        dist_func = 'euclidean'
    elif isinstance(dist_func, tuple):
        dist_func, dist_power = dist_func
    else:
        dist_power = None
    if dist_func == 'one':
        zero_dist = 1
    else:
        zero_dist = ZERO_DIST
    dist_info = {'zero_dist':zero_dist, 'dist_func':dist_func, 'dist_power':dist_power}
    # Read territory
    territory_file_pj = get_territory_file(city)
    with open(territory_file_pj, 'rb') as fin:
        territory = pickle.load(fin)
    # Compute the coordinates of the grid centers
    grid_centers = get_grid_centers(territory)
    # Compute tile_0 distance array: grid_distance_array_0
    grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
    # Preprocess distances
    grid_distance_array_0 = preprocess_distances(grid_distance_array_0, dist_info)
    return territory, grid_distance_array_0
   

def get_all_distances(city, dist_func=None):
    if dist_func is None:
        dist_func = 'euclidean'
    elif isinstance(dist_func, tuple):
        dist_func, dist_power = dist_func
    else:
        dist_power = None
    if dist_func == 'one':
        zero_dist = 1
    else:
        zero_dist = ZERO_DIST
    dist_info = {'zero_dist':zero_dist, 'dist_func':dist_func, 'dist_power':dist_power}
    # Read territory
    territory_file_pj = get_territory_file(city)
    with open(territory_file_pj, 'rb') as fin:
        territory = pickle.load(fin)
    # Compute the coordinates of the grid centers
    grid_centers = get_grid_centers(territory)
    # Compute tile_0 distance array: grid_distance_array_0
    grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
    # Preprocess distances
    grid_distance_array_0 = preprocess_distances(grid_distance_array_0, dist_info)
    # Get all tile-to-tile distances
    ntx, nty = territory['ntiles']
    nt = ntx*nty
    all_tids = np.arange(nt)
    ty = all_tids//ntx
    tx = all_tids - ntx*ty
    delta_x = np.abs(np.subtract.outer(tx,tx))
    delta_y = np.abs(np.subtract.outer(ty,ty))
    gid = delta_x + ntx*delta_y
    d = grid_distance_array_0[gid]
    return d


def add_random_contacts(graph, pop, H, currently_infected=None, verbose=False):
    if currently_infected is None:
        currently_infected = range(graph.vcount())
    tot_probability = 0
    edges = []
    N = graph.vcount()
    for i in currently_infected:
        # compute edge probabilities 
        # Get the probability array for the node n
        single_node_bp_array = H[pop['block'][i]][pop['block']]
        # Add all things up
        ep = pop['fitness'][i] * pop['fitness'] * single_node_bp_array
        ep[i] = 0
        # extract and store edges
        rng = default_rng()
        to_connect = (ep > rng.uniform(0, 1, ep.shape))
        edges.extend([(i,c) for c in np.where(to_connect)[0].tolist()])
        tot_probability += ep.sum()
    try:
        layer = graph.es['layer']
    except:
        layer = []
    graph.add_edges(edges)
    graph.es['layer'] = layer+['random']*len(edges)
    if verbose:
        print(f"add_random_contacts: Random contacts added to the graph")
    return graph


def add_random_contacts_old(graph, p, tile2tile_distances=None, random_mode=False, currently_infected=None, save_memory=False, verbose=False):
    if currently_infected is None:
        currently_infected = range(graph.vcount())
    if random_mode == 'POLYMOD':
        edges = get_POLYMOD_random_contacts(graph, p, currently_infected, tile2tile_distances, save_memory=save_memory)
    else:
        edges = get_ER_random_contacts(graph, p, currently_infected, tile2tile_distances, save_memory=save_memory)
    gcopy = graph.copy()
    # distances = gcopy.es['distance']
    try:
        layer = gcopy.es['layer']
    except:
        layer = []
    gcopy.add_edges(edges.tolist())
    # gcopy.es['distance'] = distances
    gcopy.es['layer'] = layer+['random']*len(edges)
    if verbose:
        print(f"add_random_contacts: Random contacts added to the graph")
        # print(gcopy.summary())
    return gcopy


def get_POLYMOD_random_contacts(graph, p, currently_infected, tile2tile_distances=None, save_memory=False):
    ages    = graph.vs['type']
    ngroups = max(ages)+1
    vcount  = graph.vcount()
    g2g_array = preprocess_g2g_matrix(GROUP2GROUP, {'group_names':GROUP_NAMES}, {'type':ages}).reshape(-1)
    # Build the age-indexed array of probabilities: age_prob_matrix[g][u] is the probability of an edge linking any agent of group g and agent u, based on the contact matrix
    age_prob_matrix = np.empty(shape=(ngroups,vcount))
    for age in range(ngroups):
        age_prob_matrix[age] = p*g2g_array[np.asarray(ages)+ngroups*age]
    rng = default_rng(RNG)
    if tile2tile_distances is not None:
        tile_ids = np.asarray(graph.vs['tile_id'])
    if save_memory:
        edges = np.empty(shape=(0,2),dtype=int)
        for j,i in enumerate(currently_infected):
            if tile2tile_distances is not None:
                this_node_distances = tile2tile_distances[tile_ids[i]][tile_ids]
                q = age_prob_matrix[ages[i]]/this_node_distances
                q[i] = 0
                q = q/q.sum()*age_prob_matrix[ages[i]].sum()
            else:
                q = age_prob_matrix[ages[i]]
            to_connect = rng.uniform(0, 1, vcount)<q
            to_connect[currently_infected[:j+1]] = False
            edges = np.concatenate((edges, np.stack(np.meshgrid([i],to_connect.nonzero()[0]),-1).reshape(-1,2)))
    else:
        n_inf = len(currently_infected)
        currently_infected = np.asarray(currently_infected)
        if tile2tile_distances is not None:
            infected_nodes_distances = tile2tile_distances[tile_ids[currently_infected]][:,tile_ids]
            q = age_prob_matrix[np.asarray(ages)[currently_infected]]/infected_nodes_distances
            q[np.repeat(np.arange(n_inf),range(n_inf),axis=0),currently_infected[np.tril_indices(n_inf-1)[1]]] = 0
            q = q/q.sum(axis=1)[:,None]*age_prob_matrix[np.asarray(ages)[currently_infected]].sum(axis=1)[:,None]
        else:
            q = age_prob_matrix[np.asarray(ages)[currently_infected]]
        to_connect = rng.uniform(0, 1, (n_inf,vcount))<q
        to_connect[np.repeat(np.arange(n_inf),range(n_inf),axis=0),currently_infected[np.tril_indices(n_inf-1)[1]]] = False
        edges = to_connect.nonzero()
        edges = np.transpose((currently_infected[edges[0]],edges[1]))
    return edges
   

def get_ER_random_contacts(graph, p, currently_infected, tile2tile_distances=None, save_memory=False):
    vcount  = graph.vcount()
    rng = default_rng(RNG)
    if tile2tile_distances is not None:
        tile_ids = np.asarray(graph.vs['tile_id'])
    if save_memory:
        edges = np.empty(shape=(0,2),dtype=int)
        for j,i in enumerate(currently_infected):
            if tile2tile_distances is not None:
                this_node_distances = tile2tile_distances[tile_ids[i]][tile_ids]
                q = p/this_node_distances
                q[i] = 0
                q = q/q.sum()*p*(vcount-1)
            else:
                q = p
            to_connect = rng.uniform(0, 1, vcount)<q
            to_connect[currently_infected[:j+1]] = False
            edges = np.concatenate((edges, np.stack(np.meshgrid([i],to_connect.nonzero()[0]),-1).reshape(-1,2)))
    else:
        n_inf = len(currently_infected)
        currently_infected = np.asarray(currently_infected)
        if tile2tile_distances is not None:
            infected_nodes_distances = tile2tile_distances[tile_ids[currently_infected]][:,tile_ids]
            q = p/infected_nodes_distances
            q[np.repeat(np.arange(n_inf),range(n_inf),axis=0),currently_infected[np.tril_indices(n_inf-1)[1]]] = 0
            q = q/q.sum(axis=1)[:,None]*p*((vcount-1)*np.ones(n_inf)[:,None])
        else:
            q = p
        to_connect = rng.uniform(0, 1, (n_inf,vcount))<q
        to_connect[np.repeat(np.arange(n_inf),range(n_inf),axis=0),currently_infected[np.tril_indices(n_inf-1)[1]]] = False
        edges = to_connect.nonzero()
        edges = np.transpose((currently_infected[edges[0]],edges[1]))
    return edges
    

def add_contacts_distance(graph, reachable_tiles, p=1, verbose=False):
    gcopy = graph.copy()
    edges = []
    # distances = gcopy.es['distance']
    layer = gcopy.es['layer']
    tile_ids = gcopy.vs['tile_id']
    rng = default_rng(RNG)
    for i in range(gcopy.vcount()):
        # get preprocessed distances for node i
        reachable_ids = reachable_tiles[tile_ids[i]][tile_ids[i+1:]]
        # extract and store edges
        to_connect = reachable_ids*(rng.uniform(0, 1, gcopy.vcount()-1-i)<p)
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])
        # distances.extend(dist_array[to_connect].tolist())
    gcopy.add_edges(edges)
    # gcopy.es['distance'] = distances
    gcopy.es['layer'] = layer+['random']*len(edges)
    if verbose:
        print(f"add_contacts_distance: Random distance-based contacts added to the graph")
        # print(gcopy.summary())
    return gcopy


def add_contacts_age(graph, p=1, verbose=False):
    gcopy = graph.copy()
    edges = []
    # distances = graph.es['distance']
    layer = gcopy.es['layer']
    types = np.asarray(gcopy.vs['type'])
    rng = default_rng(RNG)
    for i in range(gcopy.vcount()):
        to_connect = (types[i+1:]==types[i])*(rng.uniform(0, 1, gcopy.vcount()-1-i)<p)
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])
        # distances.extend(dist_array[to_connect].tolist())
    gcopy.add_edges(edges)
    # graph.es['distance'] = distances
    gcopy.es['layer'] = layer+['random']*len(edges)
    if verbose:
        print(f"add_contacts_age: Random age-based contacts added to the graph")
        # print(gcopy.summary())
    return gcopy


def add_contacts_random(graph, p=1, verbose=False):
    gcopy = graph.copy()
    edges = []
    # distances = graph.es['distance']
    layer = gcopy.es['layer']
    rng = default_rng(RNG)
    for i in range(gcopy.vcount()):
        # extract and store edges
        to_connect = rng.uniform(0, 1, gcopy.vcount()-1-i)<p
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])
        # distances.extend(dist_array[to_connect].tolist())
    gcopy.add_edges(edges)
    # graph.es['distance'] = distances
    gcopy.es['layer'] = layer+['random']*len(edges)
    if verbose:
        print(f"add_contacts_random: Random contacts added to the graph")
        # print(gcopy.summary())
    return gcopy


def activate_edges(graph, pH=1, pF=1, verbose=False):
    edges = np.asarray([e.tuple for e in graph.es])
    # distances = np.asarray(graph.es['distance'])
    layer = np.asarray(graph.es['layer'])
    rng = default_rng(RNG)
    x = rng.uniform(0,1,layer.shape)
    y = np.where(layer=='friendship',x<pF,x<pH)  
    edges = edges[y]
    # distances = distances[y]
    layer = layer[y]
    h = ig.Graph(graph.vcount(), edges, vertex_attrs={k:graph.vs[k] for k in graph.vertex_attributes()}, edge_attrs={'layer':layer}) #, 'distance':distances})
    if verbose:
        print(f"activate_edges: Edges activated for the given graph")
    return h


def configure_activation(graph, config_file, act_prob=None, city=None):
    avg_degree, activation_probability, random_mode, use_distance, fitness_func = read_graph_params(config_file)
    ####
    if avg_degree == 'graph':
        avg_degree = np.mean(graph.degree())
    if act_prob:
        activation_probability['household']  = act_prob
        activation_probability['friendship'] = act_prob
    ####
    x,y = activation_probability.get('household',0),activation_probability.get('friendship',0)
    N = graph.vcount()
    H = graph.subgraph_edges([e.tuple for e in graph.es if 'household' in e['layer']], delete_vertices=False)
    household_edges = H.ecount()
    F = graph.subgraph_edges([e.tuple for e in graph.es if e['layer']=='friendship'], delete_vertices=False)
    friendship_edges = F.ecount()
    mu = avg_degree -2*x*household_edges/N -2*y*friendship_edges/N
    if mu<=0:
        return {'act_prob':activation_probability, 'mode':None}
    ####
    if not use_distance:
        use_distance = 'one'
    territory, preprocessed_grid_distance_array_0 = get_territory_and_distances(city, use_distance)
    ntx, nty = territory['ntiles']
    ntiles = ntx*nty
    ####
    fitness_rng = default_rng()
    fitness = get_fitness(fitness_func, graph.vcount(), rng=fitness_rng)
    ####    
    ngroups = len(GROUP_NAMES)
    try:
        pop = {'type':np.asarray(graph.vs['type']), 'tile_id':np.asarray(graph.vs['tile_id']), 'fitness':fitness, 'block':np.asarray(graph.vs['block'])}
    except:
        types = np.asarray(graph.vs['type'])
        tile_id = np.asarray(graph.vs['tile_id'])
        block = ngroups*tile_id+types
        pop = {'type':types, 'tile_id':tile_id, 'fitness':fitness, 'block':block}
    nodes_per_block = count_tot_nodes_per_block(pop)
    block_size_array = np.array([nodes_per_block.get((t,a),0) for t in range(ntiles) for a in range(ngroups)])
    if random_mode in [1, 'POLYMOD', 'age']:
        random_mode = 'age'
        g2g_matrix = preprocess_g2g_matrix(GROUP2GROUP, {'group_names':GROUP_NAMES}, {'type':graph.vs['type']})
    else:
        if random_mode not in [0, 'ER', 'random']:
            print('configure_activation [warning]: the value specified for "random_mode" in the epidemic config file is invalid, "ER" will be assumed')
        random_mode = 'random'
        g2g_matrix = np.ones((ngroups,ngroups))
    ####
    # Normalize probability coefficients; normalization is computed -per block- so we use a (nblocks,nblocks) array H
    H = compute_block_normalization_matrix(mu, pop, block_size_array, g2g_matrix, preprocessed_grid_distance_array_0, territory, use_approximation=True) 
    ####        
    activation_params = {'act_prob':activation_probability, 'mode':random_mode, 'pop':pop, 'H':H}
    return activation_params


def create_interaction_graph(graph, activation_params, currently_infected=None, verbose=False):
    activation_probability = activation_params['act_prob']
    random_mode = activation_params['mode']
    if activation_probability['household'] or activation_probability['friendship']:
        gcopy = activate_edges(graph, activation_probability['household'], activation_probability['friendship'], verbose=verbose)
    else:
        gcopy = ig.Graph(graph.vcount(), vertex_attrs={k:graph.vs[k] for k in graph.vertex_attributes()})
    if random_mode:
        pop = activation_params['pop']
        H = activation_params['H']
        gcopy = add_random_contacts(gcopy, pop, H, currently_infected, verbose)
    ### DEBUG ###
    print(f'average degree of the infected: {np.mean([gcopy.vs[i].degree() for i in currently_infected])}')
    #############
    return gcopy


def compute_random_edge_probability(G, avg_degree, activation_probability=None):
    if activation_probability is None:
        x,y = 0,0
    else:
        x,y = activation_probability.get('household',0),activation_probability.get('friendship',0)
    counter = Counter(G.vs['type'])
    
    ##################### THIS SHOULD NOT BE NEEDED ANYMORE ####################
    # A[i,j] is the number of different pairs of vertices (u,v) with type(u)==i and type(v)==j -- is a triu matrix
    # A = pairs_counter_matrix([counter[i] for i in range(len(GROUP_NAMES))])
    # B[i,j] is the polymod-based probability that edge (u,v) exists for any pair (u,v) with type(u)==i and type(v)==j -- is a symmetric matrix
    # B = preprocess_g2g_matrix(GROUP2GROUP, {'group_names':GROUP_NAMES}, {'type':G.vs['type']})
    # POLYMOD_edges is the total expected number of edges based on POLYMOD with no in-house contacts
    # POLYMOD_edges = (A*B).sum()
    ############################################################################


    N = G.vcount()
    N_choose_2 = N*(N-1)/2
    # household subgraph
    H = G.subgraph_edges([e.tuple for e in G.es if 'household' in e['layer']], delete_vertices=False)
    # household_edges is the total number of household edges in G
    household_edges = H.ecount()
    # friendship subgraph
    F = G.subgraph_edges([e.tuple for e in G.es if e['layer']=='friendship'], delete_vertices=False)
    # friendship_edges is the total number of friendship edges in G
    friendship_edges = F.ecount()
    # tot_edges is the total expected number of edges based on the selected configuration
    tot_edges = N*avg_degree/2 -x*household_edges -y*friendship_edges
    # edge_prob is the existence probability of an edge that guarantees the expected number of edges
    if tot_edges<=0:
        edge_prob = 0
    else:
        edge_prob = tot_edges/(N_choose_2-x*household_edges-y*friendship_edges)

    #### DEBUG ####
    print(f'N*(N-1)/2: {N_choose_2}')
    print(f'\t number of edges in the social graph: {household_edges+friendship_edges}')
    print(f'\t expected number of activated edges in the social graph: {x*household_edges+y*friendship_edges}')
    print(f'\t expected activated degree in the social graph: {2*(x*household_edges+y*friendship_edges)/N}')
    print(f'\t expected number of inactivated edges in the social graph: {(1-x)*household_edges+(1-y)*friendship_edges}')
    print(f'\t expected activated edges outside the social graph: {edge_prob*(N_choose_2-x*household_edges-y*friendship_edges)}')
    ###############
    
    return edge_prob

    # if x==0:
    #     if y==0:
    #         return edge_prob
    #     else:
    #         print(f'compute_random_edge_probability: [error] the given combination of activation probability {activation_probability} is invalid')
    #         sys.exit()
    # else:
    #     N = G.vcount()
    #     nu = np.mean(H.degree())
    #     if y==0:
    #         return (edge_prob*(N-1)-x*nu)/(N-1-x*nu)
    #     else:
    #         # friendship subgraph
    #         F = G.subgraph_edges([e.tuple for e in G.es if e['layer']=='friendship'], delete_vertices=False)
    #         mu = np.mean(F.degree())
    #         return (edge_prob*(N-1)-x*nu-y*mu)/(N-1-x*nu-y*mu)


def save_temporal_edges(gcopy, activation_probability):
    edges = np.asarray([e.tuple for e in gcopy.es])
    layer = np.asarray(gcopy.es['layer'])
    to_consider = True
    if activation_probability['household']==1:
        to_consider = to_consider & np.char.find(layer,'household')==-1
    if activation_probability['friendship']==1:
        to_consider = to_consider & np.char.find(layer,'friendship')==-1
    return edges[to_consider]
            


#### DEFINE FUNCTION FOR BUILDING TRENDS AND PLOTTING ####

def plot_model(model, iterations, filename_prefix='', figs=None, legend_prefix='', **line_kwargs):
    trends = model.build_trends(iterations)

    # set colors for statuses
    status2color = {
        'Susceptible':'blue',
        'Exposed':'gold',
        'Infected':'red',
        'Recovered':'lime',
        'Dead':'black',
        'Other':'gray'
    }
    diff = min(model.available_statuses.values())
    id2status = {v-diff:k for k,v in model.available_statuses.items()}
    id2color = {k:status2color[v] for k,v in id2status.items()}
    # id2color = {i:status2color[v] for i,(k,v) in enumerate(id2status.items())}

    ## PLOT DIFFUSION TREND ##
    viz = DiffusionTrend(model, trends)
    try:
        fig = figs[0]
    except:
        fig = None
    p1 = viz.plot(width=500, height=500, cols=id2color, fig=fig, legend_prefix=legend_prefix, **line_kwargs)
    if os.path.isdir(filename_prefix):
        outf = os.path.join(filename_prefix,'diffusion_trend.html')
    else:
        outf = filename_prefix+'diffusion_trend.html'
    output_file(outf)
    save(p1)

    ## PLOT DIFFUSION PREVALENCE ##
    viz2 = DiffusionPrevalence(model, trends)
    try:
        fig = figs[1]
    except:
        fig = None
    p2 = viz2.plot(width=500, height=500, cols=id2color, fig=fig, legend_prefix=legend_prefix, **line_kwargs)
    if os.path.isdir(filename_prefix):
        outf = os.path.join(filename_prefix,'diffusion_prevalence.html')
    else:
        outf = filename_prefix+'diffusion_prevalence.html'
    output_file(outf)
    save(p2)
    return p1,p2


####################################################################
#################### EPIDEMICS ON A GIVEN GRAPH ####################
####################################################################
def build_si_model(epidemic_params, pop_size, infected=None, graph=None, verbose=False):

    if graph and pop_size!=graph.vcount():
        print(f'build_si_model: WARNING! specified pop_size is {pop_size} but the given graph has {graph.vcount()} vertices! pop_size is set to {graph.vcount()}')
        pop_size = graph.vcount()

    #############################################
    #### READ PARAMS AND DEFINE COMPARTMENTS ####
    #############################################

    #### INFECTION ####
    # infection probability for edges linking to an infected node
    try:
        beta = epidemic_params['beta']
    except:
        print('build_si_model: could not determine the infection probability/ies ("beta") based on the given epidemic_params; will use 0.5')
        beta = .5
    if isinstance(beta, dict):
        # putting sublayers at the top level. e.g., 'park':{'bench':0.2,'playground':0.3} becomes 'park_bench':0.2, 'park_playground':0.3
        temp = {}
        for k,v in beta.items():
            if isinstance(v,dict):
                for sublayer,b in v.items():
                    temp[f'{k}_{sublayer}'] = b
            else:
                temp[k] = v
        beta = temp
        if verbose:
            print('build_si_model: the infection probabilities are the following')
            for k,v in beta.items():
                print(f'\t{k}: {v}')
    else:
        if verbose:
            print(f'build_si_model: the infection probability is {beta}')
        beta = {'default':beta}
    
    try:
        existing_layers = sorted(set(graph.es['layer']))
    except:
        existing_layers = list(beta.keys())

    infection_rules = []
    for layer in existing_layers:
        if layer not in beta:
            b = beta.get('default')
            if b is None:
                print(f'build_si_model: WARNING! infection probability for layer {layer} not specified and no default value is available; layer {layer} will be ignored!')
                continue
            elif verbose:
                print(f'build_si_model: infection probability for layer {layer} not specified, will use the default value {b}')
        else:
            b = beta[layer]
        infection_rules.append(EdgeCategoricalAttribute("layer", layer, probability=b, triggering_status=["Infected"]))
    
    #### INITIAL INFECTED ####
    # number of initially infected individuals
    if infected is None or isinstance(infected, int):
        if infected is None:
            try:
                try:
                    infected = epidemic_params['pi']
                except:
                    infected = int(round(pop_size*epidemic_params['delta']))
            except:
                print('build_si_model: could not determine the number of initially infected individuals ("pi" or "delta") based on the given epidemic_params; will use 1')
                infected = 1
        initial_infected_nodes = np.random.choice(pop_size,infected,replace=False).tolist()
    else:
        initial_infected_nodes = [i for i in infected]
    if verbose:
        print('build_si_model: the number of initially infected individuals is', len(initial_infected_nodes))
    
    ######################
    #### CREATE MODEL ####
    ######################

    model = CompositeModel(graph) 
    
    # Model statuses
    model.add_status("Susceptible")
    model.add_status("Infected")
    
    # Set inital infected
    model.reset(infected_nodes=initial_infected_nodes)
    
    # Add disease attribute, this means that no minimum disease length is imposed
    model.attributes['disease'] = np.zeros(pop_size)
    # Add infection and recovery rules
    for infection_rule in infection_rules: 
        model.add_rule("Susceptible", "Infected", infection_rule)
    
    return model


def build_sir_model(epidemic_params, pop_size, infected=None, graph=None, layers=None, store_infections=False, verbose=False):

    if graph and pop_size!=graph.vcount():
        print(f'build_sir_model: WARNING! specified pop_size is {pop_size} but the given graph has {graph.vcount()} vertices! pop_size is set to {graph.vcount()}')
        pop_size = graph.vcount()

    #############################################
    #### READ PARAMS AND DEFINE COMPARTMENTS ####
    #############################################

    #### INFECTION ####
    # infection probability for edges linking to an infected node
    try:
        beta = epidemic_params['beta']
    except:
        print('build_sir_model: could not determine the infection probability/ies ("beta") based on the given epidemic_params; will use 0.5')
        beta = .5
    if isinstance(beta, dict):
        # putting sublayers at the top level. e.g., 'park':{'bench':0.2,'playground':0.3} becomes 'park_bench':0.2, 'park_playground':0.3
        for k,v in beta.items():
            if isinstance(v,dict):
                for sublayer,b in v.items():
                    beta[f'{k}_{sublayer}'] = b
                del(beta[k])
        if verbose:
            print('build_sir_model: the infection probabilities are the following')
            for k,v in beta.items():
                print(f'\t{k}: {v}')
    else:
        if verbose:
            print(f'build_sir_model: the infection probability is {beta}')
        beta = {'default':beta}
   
    if layers is None:
        try:
            existing_layers = sorted(set(graph.es['layer']))
        except:
            existing_layers = list(beta.keys())
    else:
        existing_layers = layers

    infection_rules = []
    for layer in existing_layers:
        if layer not in beta:
            b = beta.get('default')
            if b is None:
                print(f'build_sir_model: WARNING! infection probability for layer {layer} not specified and no default value is available; layer {layer} will be ignored!')
                continue
            elif verbose:
                print(f'build_sir_model: infection probability for layer {layer} not specified, will use the default value {b}')
        else:
            b = beta[layer]
        infection_rules.append(EdgeCategoricalAttribute("layer", layer, probability=b, triggering_status=["Infected"]))
    
    #### RECOVERY ####
    try:
        gamma = epidemic_params['gamma']
    except:
        print('build_sir_model: could not determine the recovery probability/ies ("gamma") based on the given epidemic_params; will use 0.1')
        gamma = 0.1
    
    try:
        cure_rate = np.vectorize(gamma.__getitem__)(graph.vs['type'])
        if verbose:
           print('build_sir_model: the recovery probabilities are the following')
           for k,v in gamma.items():
               print(f'\t{k}: {v}')
    except:
        try:
            gamma = np.mean(gamma.values())
        except:
            pass
        print(f'build_sir_model: the recovery probability is set to {gamma}, regardless of the vertex type')
        cure_rate = gamma
    recovery = NodeNumericalAttribute('disease', value=0, op='==', rate=cure_rate)
    
    #### INITIAL INFECTED ####
    # number of initially infected individuals
    if infected is None or isinstance(infected, int):
        if infected is None:
            try:
                try:
                    infected = epidemic_params['pi']
                except:
                    infected = int(round(pop_size*epidemic_params['delta']))
            except:
                print('build_sir_model: could not determine the number of initially infected individuals ("pi" or "delta") based on the given epidemic_params; will use 1')
                infected = 1
        initial_infected_nodes = np.random.choice(pop_size,infected,replace=False).tolist()
    else:
        initial_infected_nodes = [i for i in infected]
    if verbose:
        print('build_sir_model: the number of initially infected individuals is', len(initial_infected_nodes))

    ######################
    #### CREATE MODEL ####
    ######################

    #model = CompositeModel(graph) 
    model = CompositeModel(graph, epidemic_params['seed'], store_infections=store_infections) 
    
    # Model statuses
    model.add_status("Susceptible")
    model.add_status("Infected")
    model.add_status("Recovered")
    
    # Set inital infected
    model.reset(infected_nodes=initial_infected_nodes)
    
    # Add disease attribute, this means that no minimum disease length is imposed
    model.attributes['disease'] = np.zeros(pop_size)
    # Add infection and recovery rules
    for infection_rule in infection_rules: 
        model.add_rule("Susceptible", "Infected", infection_rule)
    model.add_rule("Infected", "Recovered", recovery) 
    
    return model


def run_sir(model, n_iter=None, graph=None, randomize=False, activation_params=None, city=None, r_0=False, verbose=False):

    #### save edgelist with timestamp to build aggregated temporal graph ####
    temporal_edges = [] 
    ########################################

    # Run the epidemics   
    if not n_iter:
        n_iter = np.inf
    initially_infected_nodes = [k for k,v in sorted(model.status.items()) if v==model.available_statuses['Infected']]
    index_contacts = [[] for _ in range(len(initially_infected_nodes))]
    index_infected = True
    if r_0:
        if len(initially_infected_nodes)>1:
            sys.exit('[error]: only 1 initially infected node is admitted when computing R0 index!')
    if graph and not randomize:
        model.add_graph(graph, reset_status=False)
        if verbose:
            print('run_sir: graph added!')
    elif randomize:
        infected_degrees = []
        gcopy = create_interaction_graph(graph, activation_params, initially_infected_nodes, verbose=verbose)
        ## save temporal edges ##
        temporal_edges.append(save_temporal_edges(gcopy, activation_params['act_prob']))
        ######################### 
        if activation_params['mode']:
            gcopy.simplify(combine_edges={'layer':lambda x:sorted(x)[0] if x else None}) #, 'distance':lambda x:x[0] if x else None
        infected_degrees.append([Counter([e['layer'] for e in gcopy.vs[i].incident()]) for i in initially_infected_nodes])
        if index_infected:
            for idx,index in enumerate(initially_infected_nodes):
                v = gcopy.vs[index]
                index_contacts[idx].append(len([e for e in v.incident() if e['layer']=='random']))
        model.add_graph(gcopy, reset_status=False)
        if verbose:
            print('run_sir: contact graph for step 1 created and added!')
            for k,v in Counter(gcopy.es['layer']).items():
                print(f'edges of type {k}: {v}')
    
    iterations = model.iteration_bunch(2) #, verbose=True)
    i = 1
    num_currently_infected = iterations[i]['node_count'][model.available_statuses['Infected']]
    index_infected = any([model.status[k]==model.available_statuses['Infected'] for k in initially_infected_nodes])
        
    while i<n_iter and num_currently_infected:
        if r_0 and model.status[index] != model.available_statuses['Infected']:
            break
        if verbose and i%10==0:
            print(f'run_sir: the epidemics is at step {i}, the number of infected nodes is {num_currently_infected}')
        if randomize:
            currently_infected = [k for k,v in sorted(model.status.items()) if v==model.available_statuses['Infected']]
            gcopy = create_interaction_graph(graph, activation_params, currently_infected, verbose=False)
            ## save temporal edges ##
            temporal_edges.append(save_temporal_edges(gcopy, activation_params['act_prob']))
            if activation_params['mode']:
                gcopy.simplify(combine_edges={'layer':lambda x:sorted(x)[0] if x else None}) # , 'distance':lambda x:x[0] if x else None
            infected_degrees.append([Counter([e['layer'] for e in gcopy.vs[j].incident()]) for j in currently_infected])
            if index_infected:
                for idx,index in enumerate(initially_infected_nodes):
                    v = gcopy.vs[index]
                    index_contacts[idx].append(len([e for e in v.incident() if e['layer']=='random']))
            
            model.add_graph(gcopy, reset_status=False)
        iters = model.iteration_bunch(1) #, verbose=True)
        it = {'iteration':i+1}
        it.update({k:v for k,v in iters[-1].items() if k!='iteration'})
        iterations.append(it)        ##
        i += 1
        num_currently_infected = iterations[i]['node_count'][model.available_statuses['Infected']]
        index_infected = any([model.status[k]==model.available_statuses['Infected'] for k in initially_infected_nodes])

    if not randomize:
        infected_degrees = None
    
    return initially_infected_nodes, index_contacts, model, iterations, infected_degrees, temporal_edges


def build_infection_graph(infections):
    g = ig.Graph(directed=True, vertex_attrs={'name':[]}, edge_attrs={'layer':[],'time':[]})
    name2index = {}
    for layer,time_infections in infections.items():
        for t,edges in enumerate(time_infections):
            if edges is not None:
                i = edges.max()
                if i>=g.vcount():
                    g.add_vertices(i+1-g.vcount())
                g.add_edges(edges)
                g.es[-len(edges):]['layer'] = [layer]*len(edges)
                g.es[-len(edges):]['time']  = [t]*len(edges)
    g.vs['name'] = [i for i in range(g.vcount())]
    if g.ecount():
        g = g.components('WEAK').giant()
    return g


def get_sources(infections, attr=None):
    g = build_infection_graph(infections)
    sources = set([v.index for v in g.vs if v.indegree()==0])
    if attr:
        try:
            sources = [attr[g.vs['name'][i]] for i in sources]
        except:
            print(f'[error] getting attribute {attr} failed! returning vertex ids')
    return sources


def build_pathways(infections, tile_ids):
    g = build_infection_graph(infections)
    tile_ids = [tile_ids[i] for i in g.vs['name']]
    g.vs['tile_id'] = tile_ids 
    source_tiles = set([v['tile_id'] for v in g.vs if v.indegree()==0])
    tiles = ig.VertexClustering(g,tile_ids)
    tiles_infections = tiles.cluster_graph(combine_vertices={'tile_id':lambda x:(x[0] if x else None), 'name':lambda x:x}, combine_edges={'layer':'ignore', 'time':'min'})
    if tiles_infections.vcount():
        if not tiles_infections.ecount():
            comps = tiles_infections.components('WEAK').subgraphs()
            for comp in comps:
                if comp.vs['tile_id'] != [None]:
                    tiles_infections = comp
                    break
        else:
            tiles_infections = tiles_infections.components('WEAK').giant()
    to_keep_edges = []
    to_keep_times = []
    for v in tiles_infections.vs:
        if v['tile_id'] not in source_tiles:
            es = v.in_edges()
            if es:
                etimes = [e['time'] for e in es]
                mintime = min(etimes)
                to_keep = [e.tuple for t,e in zip(etimes,es) if t==mintime]
                to_keep_edges.extend(to_keep)
                to_keep_times.extend([mintime]*len(to_keep))
    tiles_infections.delete_edges()
    tiles_infections.add_edges(to_keep_edges)
    tiles_infections.es['time'] = to_keep_times
    return tiles_infections


def build_community_pathways(infections, social_graph):
    g = build_infection_graph(infections)
    clusters = social_graph.community_multilevel().membership
    clusters = [clusters[i] for i in g.vs['name']]
    g.vs['cluster_id'] = clusters
    source_clusters = set([v['cluster_id'] for v in g.vs if v.indegree()==0])
    clusters = ig.VertexClustering(g,clusters)
    clusters_infections = clusters.cluster_graph(combine_vertices={'cluster_id':lambda x:(x[0] if x else None), 'name':lambda x:x}, combine_edges={'layer':'ignore', 'time':'min'})
    if clusters_infections.vcount():
        if not clusters_infections.ecount():
            comps = clusters_infections.components('WEAK').subgraphs()
            for comp in comps:
                if comp.vs['cluster_id'] != [None]:
                    clusters_infections = comp
                    break
        else:
            clusters_infections = clusters_infections.components('WEAK').giant()
    to_keep_edges = []
    to_keep_times = []
    for v in clusters_infections.vs:
        if v['cluster_id'] not in source_clusters:
            es = v.in_edges()
            if es:
                etimes = [e['time'] for e in es]
                mintime = min(etimes)
                to_keep = [e.tuple for t,e in zip(etimes,es) if t==mintime]
                to_keep_edges.extend(to_keep)
                to_keep_times.extend([mintime]*len(to_keep))
    clusters_infections.delete_edges()
    clusters_infections.add_edges(to_keep_edges)
    clusters_infections.es['time'] = to_keep_times
    return clusters_infections

# ig.plot(h, layout=h.layout_grid(width=dimy), target='test.png', vertex_size=[1+c/2 for c in h.outdegree()], edge_width=[1/(1+t) for t in h.es['time']])


########### MAIN ###########
DEFAULT_OUT_PATH = 'epi_output'
DEFAULT_IN_PATH = '.'
DEFAULT_CONFIG_FILE = 'config/epidemics.ini'

def main():
    parser=argparse.ArgumentParser(description="network_epidemics: run a SIR epidemics on a given graph")
    parser.add_argument('-c', '--config', dest='config', type=str, default=DEFAULT_CONFIG_FILE, help='configuration file in .ini format')
    parser.add_argument('-o', '--output-path', dest='out_path', type=str, default=DEFAULT_OUT_PATH, help='output path, used for all output files generated as a result of the simulation')
    parser.add_argument('-i', '--input-path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('-g', '--graph', dest='graph', type=str, required=True, help='the graph to be used for the epidemic simulation')
    parser.add_argument('-n', '--n-iter', dest='n_iter', type=int, default=0, help='number of steps of the epidemic simulation; if falsy, the simulation runs until the number of infected individuals becomes 0')
    parser.add_argument('-m', '--mode', dest='mode', type=str, default='random', help='the execution mode: can be "random" or the name of a vertex attribute; in the latter case, all initially infected vertices have the same (randomly chosen) value for that attribute')
    parser.add_argument('-u', '--mode-value', dest='mode_value', action='append', type=str, default=None, help='the specific value of the mode attribute, only the modes tile_id and node_id are supported')
    parser.add_argument('-r', '--n-runs', dest='n_runs', type=int, default=1, help='number of runs to execute (only runs with at least 25% infected are counted)')
    parser.add_argument('-b', '--beta', dest='beta', type=float, default=0, help='infection probability')
    parser.add_argument('--set_R0', dest='R0', type=float, default=0, help='desired R0 for the index case (alternative to --beta; beta will be set accordingly)')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='whether to print information about the simulation')
    parser.add_argument('--randomize', dest='randomize', action='store_true', help='whether to create randomized versions of the input graph')
    parser.add_argument('-p', '--act_prob', dest='act_prob', type=float, default=0, help='activation probability for the static network (household+friendship layers)')
    parser.add_argument('--R0', dest='r_index', action='store_true', default=False, help='whether to just run the simulation in order to compute the R0 for the index case; if used, the initial number of infected individuals is set to 1 and the option -n (--n-iter) is ignored')
    parser.add_argument('--city', dest='city', type=str, default='', help='the city that the graph represents, this is needed for distance-based random contacts')
    parser.add_argument('--num_infected', dest='num_infected', type=int, default=0, help='number of initially infected agents')
    args = parser.parse_args()

    ### READ PARAMS ###
    in_path = args.in_path
    config_file = os.path.join(in_path,args.config)
    out_path = args.out_path
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    graph_file = os.path.join(in_path, args.graph)
    n_iter = args.n_iter
    mode = args.mode
    mode_value = args.mode_value
    n_runs = args.n_runs
    beta = args.beta
    R0 = args.R0
    if R0 and beta:
        print('[warning]: options --set_R0 and --beta are mutually exclusive, --set_R0 will be ignored')
        R0 = 0
    num_infected = args.num_infected
    verbose = args.verbose
    randomize = args.randomize
    act_prob = args.act_prob
    r_index = args.r_index
    city = args.city
    
    graph = ig.read(graph_file)
    if verbose:
        print("\n*** Graph ***")
        print(graph.summary())
        print("*** Graph ***\n")
    ### clean graph layers ###
    graph.es['layer'] = ['household' if 'household' in l else l for l in graph.es['layer']]
    
    if 'central' in mode_value or 'peripheral' in mode_value:
        hc = compute_geographic_harmonic_centrality(city=city, graph=graph_file)
        ctile = np.argmax(hc)
        tpop = {int(k):v for k,v in Counter(graph.vs['tile_id']).items()}
        ptiles = np.argsort(hc)
        for ptile in ptiles:
            if tpop.get(int(ptile),0)>0:
                break
        t_dict = {'central':ctile, 'peripheral':ptile}
        mode_value = [t_dict[v] if v in t_dict else v for v in mode_value]

    epidemic_params = read_epidemics_params(config_file)
    if R0:
        beta = (R0/np.mean(graph.vs.degree()))*epidemic_params['gamma']
    if beta:
        epidemic_params['beta'] = beta
    if num_infected:
        epidemic_params['pi'] = num_infected

    if verbose:
        print("\n*** Parameters ***")
        print(f"Input path: {in_path}")
        print(f"Configuration file: {config_file}")
        print(f"Output path: {out_path}")
        print(f"Input graph: {graph_file}")
        print(f"Mode: {mode}")
        print(f"Mode Value: {mode_value}")
        print(f"Number of runs: {n_runs}")
        if beta:
            print(f"Infection probability: {beta}")
        if act_prob:
            print(f"Activation probability for the static social network: {act_prob}")
        if num_infected:
            print(f"Number of initially infected agents: {num_infected}")
        print(f"Create random dynamic contacts: {randomize}")
        if city:
            print(f"Use the city: {city}")
        if r_index:
            print("Each simulation will run only as long as needed to compute R0 for the index case")
        elif not n_iter:
            print("Each simulation will run as long as needed to have no more infected individuals")
        else: 
            print(f"Number of iterations: {n_iter}")
        print("*** Parameters ***\n")
    
    if r_index and epidemic_params['pi']!=1:
        print(f'[warning]: to compute R0 the initial number of infected individuals must be 1; the value indicated in the config file is {epidemic_params["pi"]} and will be ignored')
        epidemic_params['pi'] = 1
    if r_index and n_iter:
        print(f'[warning]: when only computing R0, the simulation is interrupted as soon as the index case reaches the "removed" state; the given value {n_iter} for the number of iterations will be ignored')
        n_iter = 0

    if randomize:
        activation_params = configure_activation(graph, config_file, act_prob, city)
    else:
        activation_params = None

    n_infected = None
    infected_vertices = None
    if mode in graph.vertex_attributes():
        try:
            n_infected = epidemic_params['pi']
        except:
            n_infected = int(round(graph.vcount()*epidemic_params['delta']))
        values = list(set(graph.vs[mode])) 
        np.random.shuffle(values) 
        backup_values = values.copy()
    elif mode == 'node_id':
        try:
            n_infected = epidemic_params['pi']
        except:
            n_infected = int(round(graph.vcount()*epidemic_params['delta']))
        if n_infected != len(mode_value):
            sys.exit('ERROR: configuration parameters specified in the configuration file do not match the number of --mode-value provided, see number of initial infected')
    elif mode != 'random':
        print('WARNING: Specified mode is not a vertex attribute, running in random mode')
        mode = 'random'
    mode_str = f'{mode}_mode'

    prefix = os.path.basename(args.graph).rsplit('.',1)[0]+f'_beta{epidemic_params["beta"]}_gamma{epidemic_params["gamma"]}_{mode_str}_{"contacts_" if randomize else ""}'
    header = '# run, id, type, fitness, degree, hh_size, tile'
    lines = []
    max_length = 0
    i = 0
    while i<n_runs:
        print(f'---- At run {i} ----')
        if mode != 'random':
            while True:
                if mode == 'tile_id' and mode_value is not None:
                    if isinstance(mode_value, list):
                        v=mode_value[0]
                    else:
                        v=mode_value
                    value = int(v)
                elif mode == 'node_id':
                    if mode_value is None:
                        sys.exit("ERROR: value is missing, --mode-value is required for --mode 'node_id' ")
                    value = [int(j) for j in mode_value]
                else:
                    if not values:
                        print(f'WARNING: all values for attribute {mode} have been used, already used values will be reused')
                        values = backup_values.copy()
                    value = values.pop()
                if mode == 'node_id':
                    selected = value
                    if verbose:
                        for idx in value:
                            print(f'Node: {idx} - Type: {graph.vs[idx]["type"]} - Tile: {graph.vs[idx]["tile_id"]} ' 
                                  f' - Household: {graph.vs[idx]["household"]} - Degree: {graph.vs[idx].degree()}')
                else:
                    selected = np.where(np.array(graph.vs[mode])==value)[0]
                if len(selected)<n_infected:
                    if mode_value is not None:
                        print(f"ERROR: the selected tile has not enough vertices. Quit!")
                        sys.exit(111)
                    print(f'WARNING: not enough vertices having {mode}=={value}; selecting a different value for attribute {mode}')
                else:
                    break
            infected_vertices = np.random.choice(selected,n_infected,replace=False)
            if verbose:
                print(f'{len(infected_vertices)} vertices selected, all having attribute {mode}=={value}')
            mode_str = f'{mode}_{value}'
                
        if infected_vertices is None:
            infected_vertices = n_infected

        if randomize:
            layers = list(set(graph.es['layer']))+['random']
        else:
            layers = None

        st = timer()
        # infected_vertices can be a LIST or an INT, in the second case the infected nodes are choosen at random by the function build_sir_model()
        model = build_sir_model(epidemic_params, graph.vcount(), infected=infected_vertices, graph=graph, layers=layers, store_infections=True, verbose=verbose)
        initially_infected_nodes, random_contacts, model, iterations, infected_degrees, temporal_edges = run_sir(model, n_iter=n_iter, graph=graph, randomize=randomize, activation_params=activation_params, city=city, r_0=r_index, verbose=verbose)
        attack_rate = iterations[-1]['node_count'][3]/graph.vcount()
        print(f'attack rate: {attack_rate}')
        if attack_rate>=0.25:
            subfolder = 'global_outbreak'
        else:
            subfolder = 'minor_outbreak'
        if not os.path.exists(os.path.join(out_path,subfolder)):
            os.makedirs(os.path.join(out_path,subfolder))
        
        if verbose:
            if r_index:
                print(f'Time to create SIR epidemic model and compute R0 ({len(iterations)-1} iterations) for the index case on the given graph: {timer()-st}')
            else:
                print(f'Time to create SIR epidemic model and run {len(iterations)-1} iterations on the given graph: {timer()-st}')
        if randomize:
            r_mode = ''
            for k in ['household','friendship','ER','POLYMOD']:
                v = activation_params['act_prob'].get(k)
                if v:
                    r_mode += f'{k}={v}_'
            fname = f'infected_degrees_{r_mode}run_{i}.csv'
            with open(os.path.join(out_path,subfolder,prefix+fname), 'w') as fout:
                print('run, household, friendship, random', file=fout)
                for j,degs in enumerate(infected_degrees):
                    for d in degs:
                        print(f'{j}, {d.get("household",0)}, {d.get("friendship",0)}, {d.get("random",0)}', file=fout)
        max_length = max(max_length, max(len(conts) for conts in random_contacts))
        d = {}
        
        infections = model.infections
        indexes = np.asarray(initially_infected_nodes).reshape((len(initially_infected_nodes),1))
        for layer,infs in infections.items():
            sel_infs = [v for v in infs if v is not None]
            if sel_infs:
                d[layer] = (np.vstack(sel_infs)[:,0]==indexes).sum(axis=1).tolist()
            else:
                d[layer] = [0 for index in initially_infected_nodes]
        if i == 0:
            header += ', '+', '.join([f'infected_{layer}' for layer in d.keys()])
        with open(os.path.join(out_path,subfolder,prefix+f'run_{i}_sir_iterations.pickle'), 'wb') as f:
            pickle.dump(iterations, f)
        with open(os.path.join(out_path,subfolder,prefix+f'run_{i}_sir_infections.pickle'), 'wb') as f:
            pickle.dump(infections, f)
        with open(os.path.join(out_path,subfolder,prefix+f'run_{i}_temporal_edges.pickle'), 'wb') as f:
            pickle.dump(temporal_edges, f)
        for idx in range(len(initially_infected_nodes)):
            nidx = initially_infected_nodes[idx]
            v = graph.vs[nidx]
            v_type = v["type"]
            v_tile = v["tile_id"]
            v_fit  = v["fitness"]
            v_deg  = v.degree()
            if v["household"] == -1:
                v_hhSize = 0
            else:
                # Find all vertices that have the same hh idx
                v_hhSize = len(np.where(np.array(model.graph.vs["household"])==v["household"])[0])
            line = f'{i}, {nidx}, {v_type}, {v_fit}, {v_deg}, {v_hhSize}, {v_tile}'
            line += ', '+', '.join([str(inf[idx]) for inf in d.values()])
            line += ', '+', '.join([str(j) for j in random_contacts[idx]])
            lines.append(line)
        # plot_model(model, iterations, filename_prefix=os.path.join(out_path,prefix))
        if attack_rate<0.25:
            n_runs += 1
        i += 1

    # Save information about initial node 
    header += ', '+', '.join([f'random_contacts_{j}' for j in range(max_length)])
    csv_filename = os.path.join(out_path,prefix+f'epi_init_nodes{"_R0_index" if r_index else ""}.csv')
    if os.path.exists(csv_filename):
        print(f'file {csv_filename} found, will update it')
    else:
        with open(csv_filename, 'w') as f:
            print(header, file=f)
    with open(csv_filename, 'a') as f:
        for line in lines:
            print(line, file=f)

if __name__ == "__main__":
    main()



