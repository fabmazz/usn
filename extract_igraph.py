import igraph
import pandas as pd
import numpy as np

import argparse
import time
import subprocess
from pathlib import Path

parser = argparse.ArgumentParser("")
parser.add_argument("graph_file")
parser.add_argument("-p", help="write output to pajek", action="store_true")

args = parser.parse_args()

graph_f = Path(args.graph_file)
FOLDER = graph_f.parent

g = igraph.load(graph_f)
print(f"Graph has {len(g.vs)} nodes")
comps = g.connected_components()
sizes=np.array(comps.sizes())
sper=np.sort(sizes/sizes.sum())[::-1]
sprint = ", ".join([f"{x:3.2e}" for x in sper[:5]])
print(f"{len(comps)} connected component, biggest have size:", sprint)

data=[]
for i in range(len(g.vs)):
    v = g.vs[i]
    attr = dict(v.attributes())
    attr["i"] = i
    data.append(attr)

data_df = pd.DataFrame(data)

for k in ["tile_id","type","household","block"]:
    data_df[k] = data_df[k].astype(int)


data_df.to_parquet(FOLDER / "nodes_info.pq", compression="zstd")
print("Saved nodes info in", FOLDER / "nodes_info.pq")

g.es[1].attributes()

edgl = g.get_edgelist()
edgedata=[]
for eid,edge in zip(g.get_eids(edgl),edgl):
    attrs = dict(g.es[eid].attributes())
    attrs["eid"] = eid
    attrs["i"] = edge[0]
    attrs["j"] = edge[1]
    edgedata.append(attrs)

edgedata=pd.DataFrame(edgedata)

edgedata

house_edges=edgedata[edgedata["layer"]=="household"]

edgedata.to_parquet(FOLDER/"edges_data.pq",compression="zstd")
print("Saved edges data in", FOLDER/"edges_data.pq")

if args.p:
    t = time.time()
    g.write(FOLDER/"sb_graph.net",format="net")
    t2 = time.time() - t
    print("Written graph to pajek format in {:5.2f} secs".format(t2))

    subprocess.run(["zstd", "-10", f"{FOLDER/'sb_graph.net'}"])