double yf(double x, void *params) {
       struct yf_params *p = (struct yf_params *) params;
       unsigned int n = p->n;
       unsigned int i;
     
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 
#pragma omp parallel for reduction(+:v)
       for(i=0; i<n; i++) { v+=(1./(1.+xi[i]*xj[i]*x)); } 
       return (v - c);
}
     
double yf_deriv (double x, void *params) {
       struct yf_params *p = (struct yf_params *) params;

       unsigned int n = p->n;
       unsigned int i;
     
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 
#pragma omp parallel for reduction(+:v)
       for(i=0; i<n; i++) { v+=(-(xi[i]*xj[i])/((1.+xi[i]*xj[i]*x)*(1.+xi[i]*xj[i]*x))); } 
       return (v);
     
}
     
void yf_fdf (double x, void *params, double *y, double *dy) {
       struct yf_params *p = (struct yf_params *) params;

       unsigned int n = p->n;
       unsigned int i;
     
       double *xi = p->xi;
       double *xj = p->xj;
       double c = p->c;
       double v=0.; 
#pragma omp parallel for reduction(+:v)
       for(i=0; i<n; i++) { v+=(1./(1.+xi[i]*xj[i]*x)); } 

       double dv=0.; 
#pragma omp parallel for reduction(+:dv)
       for(i=0; i<n; i++) { dv+=(-(xi[i]*xj[i])/((1.+xi[i]*xj[i]*x)*(1.+xi[i]*xj[i]*x))); } 
     
       *y = (v-c);
       *dy = dv;
}
