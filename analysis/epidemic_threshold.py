#!/usr/bin/env python
# coding: utf-8

# In[45]:


import os, sys
import pickle
import pandas as pd
import numpy as np
from scipy.sparse.linalg import eigsh
import igraph
from collections import Counter
import itertools

# PLOTTING
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

##########
from network_epidemics import *

##########
from edmonds import edmonds


# In[69]:

sns.set_style('whitegrid', rc={'figure.figsize':(17,6)})
sns.set_context("talk")

############## MACROS ###############
graph_fname = 'data/graphs/viterbo/mu20_EP1_t500_min10pop/sb_graph.pickle'
main_out_path = 'output/epidemics/viterbo/new_paper/threshold/'
epidemics_paths = {float(dirname.rsplit('_',1)[1]): main_out_path+f'act_prob_{dirname.rsplit("_",1)[1]}/' for dirname in os.listdir(main_out_path) if 'act_prob' in dirname}
city = 'viterbo'
delta = 1/3
nruns = 100

recap_data = {'threshold value':[], 'configuration':[], 'threshold type':[]} 
    
## define function to compute the variability measure for a given set of attack rate values
def compute_delta(r):
    return np.sqrt((r**2).mean()-(r.mean())**2)/r.mean()

for combo in sorted(epidemics_paths.keys()):
    epidemics_path = epidemics_paths[combo]
    print(f'at {combo}')
    plots_path = epidemics_path+'plots/'
    if not os.path.exists(plots_path):
        os.makedirs(plots_path)

    ###################### **Epidemic Threshold** #######################
    
    ## get degrees
    degrees = []
    for fname in [fname for fname in os.listdir(epidemics_path) if 'epi_init' in fname]:
        with open(os.path.join(epidemics_path,fname), 'r') as fdegs:
            next(fdegs)
            for line in fdegs:
                fields = line.split(', ')
                static_deg = int(fields[4])
                hh_deg = int(fields[5])-1
                degs = [int(c)+(np.random.uniform(0,1,static_deg)>(1-combo)).sum() for c in fields[10:]]
                degrees.extend(degs) 
    degrees = np.asarray(degrees)
    ## compute the three threshold approximations
    t1 = degrees.mean()/((degrees**2).mean()-degrees.mean())
    # t_index = degrees.mean()/((degrees**2).mean()-2*degrees.mean())
    # t3 = 1/eigsh(G.get_adjacency_sparse().astype('f'),1)[0][0]
    print(f'{combo}: the mean degree is {degrees.mean()}')
    print(f'{combo}: the threshold approximation is {t1*delta}') 

    ## select files storing the result of the epidemic simulation
    filenames = []
    for root, dirs, files in os.walk(epidemics_path):
        for file in files:
            if file.endswith("iterations.pickle") and 'beta' in file:
                filenames.append(os.path.join(root, file))

    ## get iteration data containing information about the epidemic outbreak and extract the attack rate 
    data = {'beta':[], 'r':[]}
    for filename in filenames:
        fname = filename.rsplit('/',1)[-1]
        i = fname.find('beta')
        beta = float(fname[i+4:i+4+fname[i+4:].find('_')])
        with open(filename, 'rb') as f:
            iters = pickle.load(f)
        r = iters[-1]['node_count'][3]
        data['beta'].append(beta)
        data['r'].append(r)

    ## build and preview the dataframe
    df = pd.DataFrame(data)
    # print(df.describe())
    # print(df.head())

    ## create attack rate plot with inset
    plt.clf()
    g = sns.lineplot(data=df, x='beta', y='r', color='r')
    g.set_ylabel('attack rate', c='r')
    g.axvline(t1*delta, 0, 1, c='b', label=r'$\beta_c^{\mathrm{HMF}}$')
    # g.axvline(t_index*delta, 0, 1, c='g', label=r'$\tau_{\mathrm{index}}$')
    # g.axvline(t3*delta, 0, 1, c='y', label=r'$\Lambda^{-1}$')
    axins = inset_axes(plt.gca(), "30%", "40%", loc='upper left', borderpad=1.7)
    g_inset = sns.lineplot(data=df.query('beta>@t1*@delta*9/10 and beta<@t1*@delta*11/10'), x='beta', y='r')
    g_inset.axvline(t1*delta, 0, 1, c='b')
    # g_inset.axvline(t_index*delta, 0, 1, c='g')
    # g_inset.axvline(t3*delta, 0, 1, c='y')
    g_inset.set(xlabel=None, ylabel=None)
    # plt.legend()
    plt.savefig(plots_path+'attack_rate.png', bbox_inches='tight')
    print(f'attack rate plotted at {plots_path+"attack_rate.png"}')

    ## compute the variability measure for each set of epidemic runs having the same beta (contagion rate)
    delta_df = df.groupby('beta')[['r']].apply(compute_delta)
    delta_df = delta_df.reset_index()
    # print(delta_df.describe())
    # print(delta_df.head())

    ## plot the attack rate and variability as a function of beta, with the thresholds
    plt.clf()
    g = sns.lineplot(data=df, x='beta', y='r', color='r')
    g.set_ylabel('attack rate', c='r')
    ax = g.twinx()
    ax = sns.lineplot(data=delta_df, x='beta', y='r', ax=ax, color='g')
    deltas = delta_df['r'].to_numpy()
    betas  = delta_df['beta'].to_numpy()
    bmax = betas[np.argmax(deltas)]
    ax.axvline(bmax, 0, 1, c='g', ls=':', label=r'$\beta_c^{\Delta}$')
    ax.axvline(t1*delta, 0, 1, c='b', label=r'$\beta_c^{\mathrm{HMF}}$')
    ax.set_ylabel('variability', c='g')
    plt.legend()
    plt.savefig(plots_path+'variability.png', bbox_inches='tight')
    print(f'variability plotted at {plots_path+"variability.png"}')

    # recap_data['threshold value'].extend([t1*delta,t_index*delta,bmax])
    # recap_data['threshold type'].extend([r'$\beta_c^*$',r'$\beta_c$',r'$\beta_c^{\Delta}$'])
    # recap_data['configuration'].extend([combo,combo,combo])
    recap_data['threshold value'].extend([t1*delta,bmax])
    recap_data['threshold type'].extend([r'$\beta_c^{\mathrm{HMF}}$',r'$\beta_c^{\Delta}$'])
    recap_data['configuration'].extend([combo,combo])

recap_data = pd.DataFrame(recap_data)

recap_data.to_pickle('epidemic_threshold_data.pickle')

plt.clf()
plots_path = plots_path.rsplit('/',3)[0]+'/plots/'
if not os.path.exists(plots_path):
    os.makedirs(plots_path)
d = {}
for r in recap_data.to_dict('records'):
    h = d.setdefault(r['configuration'],{})
    h[r['threshold type']] = r['threshold value']
dd = {}
for k,v in d.items():
    dd.setdefault('configuration',[]).append(k)
    for h,u in v.items():
        dd.setdefault(h,[]).append(u)
df = pd.DataFrame(dd)
my_range = range(1,len(df.index)+1)
b1 = df.columns[1]
b2 = df.columns[2]
colors = sns.color_palette('husl',n_colors=len(my_range))
# The horizontal plot is made using the hline function
plt.vlines(x=my_range, ymin=df[b1], ymax=df[b2], color='grey', alpha=0.4)
plt.scatter(my_range, df[b2], color=colors, alpha=1 , label=b2, marker='^')
plt.scatter(my_range, df[b1], color=colors, alpha=0.4, label=b1, marker='o')
plt.legend()
# Add title and axis names
plt.xticks(my_range, df['configuration'])
plt.ylabel('epidemic threshold')
plt.xlabel('configuration')
plt.gcf().set_size_inches(8,4)
plt.savefig(plots_path+'threshold_comparison.png', bbox_inches='tight')
print(f'threshold comparison plotted at {plots_path+"threshold_comparison.png"}')
